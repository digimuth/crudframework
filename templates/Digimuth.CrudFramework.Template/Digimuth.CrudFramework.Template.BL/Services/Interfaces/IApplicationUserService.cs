using Digimuth.CrudFramework.BL.Services.Interfaces;
using Digimuth.CrudFramework.Template.BL.Dtos.ApplicationUser;
using Digimuth.CrudFramework.Template.Storage.Models;

namespace Digimuth.CrudFramework.Template.BL.Services.Interfaces;

public interface IApplicationUserService
    : IApplicationUserServiceBase<ApplicationUser, ApplicationUserDto, ApplicationUserDetailsDto, ApplicationUserActivationDto, ApplicationUserRegisterDto>
{

}