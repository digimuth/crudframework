using System;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using AutoMapper;
using Digimuth.CrudFramework.BL.Services;
using Digimuth.CrudFramework.BL.Services.Interfaces;
using Digimuth.CrudFramework.Common.Configuration;
using Digimuth.CrudFramework.EmailEngine.Services.Interfaces;
using Digimuth.CrudFramework.Template.BL.Dtos.ApplicationUser;
using Digimuth.CrudFramework.Template.BL.Extensions;
using Digimuth.CrudFramework.Template.BL.Services.Interfaces;
using Digimuth.CrudFramework.Template.EmailEngine;
using Digimuth.CrudFramework.Template.EmailEngine.Models;
using Digimuth.CrudFramework.Template.Storage.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;

namespace Digimuth.CrudFramework.Template.BL.Services;

public class ApplicationUserService
    : ApplicationUserServiceBase<ApplicationUser, ApplicationUserDto, ApplicationUserDetailsDto,
            ApplicationUserActivationDto, ApplicationUserRegisterDto>,
        IApplicationUserService
{
    private readonly AuthenticationConfiguration _authenticationConfiguration;
    private readonly IEmailService<EmailType> _emailService;

    public ApplicationUserService(
        IMapper mapper,
        UserManager<ApplicationUser> userManager,
        AuthenticationConfiguration authenticationConfiguration,
        IApplicationUserIdentityService applicationUserIdentityService,
        IHttpContextAccessor httpContextAccessor,
        ILogger<ApplicationUserService> logger,
        IEmailService<EmailType> emailService)
        : base(mapper, userManager, authenticationConfiguration, applicationUserIdentityService, httpContextAccessor,
            logger)
    {
        _emailService = emailService;
        _authenticationConfiguration = authenticationConfiguration;
    }

    protected override IQueryable<ApplicationUser> AllUsers()
    {
        var query = base.AllUsers();
        var contextUser = this.GetContextUser();

        if (!contextUser.IsAdmin())
        {
            query = query.Where(u => u.Id == contextUser.Id);
        }

        return query;
    }

    public override Task<bool> IsAuthorizedForReadAllAsync()
    {
        return Task.FromResult(true);
    }

    public override Task<bool> IsAuthorizedForReadAsync(long id)
    {
        var contextUser = this.GetContextUser();

        return Task.FromResult(contextUser.IsAdmin() || contextUser.Id == id);
    }

    public override Task<bool> IsAuthorizedForCreateAsync(ApplicationUserRegisterDto entity)
    {
        return Task.FromResult(this.GetContextUser().IsAdmin());
    }

    public override Task<bool> IsAuthorizedForUpdateAsync(long id, ApplicationUserRegisterDto entity)
    {
        var contextUser = this.GetContextUser();

        return Task.FromResult(contextUser.IsAdmin() || contextUser.Id == id);
    }

    public override Task<bool> IsAuthorizedForDeleteAsync(long id)
    {
        return Task.FromResult(this.GetContextUser().IsAdmin());
    }

    public override async Task<ApplicationUserDetailsDto> CreateAsync(ApplicationUserRegisterDto dto)
    {
        dto.UserName = dto.Email;
        dto.WithInvitation = true;

        var createdUser = await base.CreateAsync(dto);
        var dbUser = await UserManager.FindByEmailAsync(dto.Email);
        await UpdateUserRolesAsync(dbUser.Id, new[] { (int) dto.Role });

        return createdUser;
    }

    protected override async Task SendInvitationNotificationAsync(ApplicationUser user, string token)
    {
        var model = new InvitationEmailModel
        {
            Email = user.Email,
            UrlTemplate = AuthenticationConfiguration.InvitationUrlTemplate,
            Token = token,
            FirstName = user.FirstName,
            LastName = user.LastName
        };

        var recipient = new MailAddress(user.Email, $"{model.FirstName} {model.LastName}");

        await _emailService.SendEmail(recipient, EmailType.Invitation, model);
    }

    protected override async Task SendForgotPasswordNotificationAsync(ApplicationUser user, string token)
    {
        var model = new ForgotPasswordEmailModel
        {
            Email = user.Email,
            Token = token,
            FirstName = user.UserName,
            UrlTemplate = _authenticationConfiguration.ResetPasswordUrlTemplate,
        };

        var recipient = new MailAddress(user.Email, user.UserName);

        await _emailService.SendEmail(recipient, EmailType.ForgotPassword, model);
    }
}
