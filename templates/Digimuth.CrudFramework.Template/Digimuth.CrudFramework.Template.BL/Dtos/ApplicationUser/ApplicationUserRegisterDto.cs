using Digimuth.CrudFramework.BL.Dtos.ApplicationUser;
using Digimuth.CrudFramework.Template.Common;

namespace Digimuth.CrudFramework.Template.BL.Dtos.ApplicationUser;

public class ApplicationUserRegisterDto : ApplicationUserRegisterBaseDto
{
    public ApplicationUserRole Role { get; set; }
}