using System.Collections.Generic;
using System.Linq;
using Digimuth.CrudFramework.BL.Dtos.ApplicationUser;
using Digimuth.CrudFramework.Template.Common;

namespace Digimuth.CrudFramework.Template.BL.Dtos.ApplicationUser;

public class ApplicationUserIdentityDto : ApplicationUserIdentityBaseDto
{
    public IEnumerable<ApplicationUserRole> Roles
    {
        get => RolesInternal.Cast<ApplicationUserRole>();

        set => RolesInternal = value.Cast<int>();
    }
}