using System.Collections.Generic;
using System.Linq;
using Digimuth.CrudFramework.BL.Dtos.ApplicationUser;
using Digimuth.CrudFramework.Template.Common;

namespace Digimuth.CrudFramework.Template.BL.Dtos.ApplicationUser;

public class ApplicationUserDetailsDto : ApplicationUserDetailsBaseDto
{
    public IEnumerable<ApplicationUserRole> Roles => RolesInternal.Cast<ApplicationUserRole>();
}