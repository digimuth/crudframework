﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Digimuth.CrudFramework.Common;
using Digimuth.CrudFramework.Common.Models;
using Digimuth.CrudFramework.Template.Common;
using Microsoft.AspNetCore.Identity;

namespace Digimuth.CrudFramework.Template.BL.Extensions;

public static class UserManagerExtensions
{
    public static async Task<bool> IsInRoleAsync<TIdentity>(
        this UserManager<TIdentity> userManager,
        TIdentity user,
        ApplicationUserRole role)
        where TIdentity : ApplicationUserBase
    {
        var userClaims = await userManager.GetClaimsAsync(user);
        return userClaims.Any(e => e.Type == CrudClaimTypes.Role && e.Value == role.ToString("D"));
    }

    public static async Task<IList<TIdentity>> GetUsersInRoleAsync<TIdentity>(
        this UserManager<TIdentity> userManager,
        ApplicationUserRole role)
        where TIdentity : ApplicationUserBase
    {
        return await userManager.GetUsersForClaimAsync(
            new Claim(CrudClaimTypes.Role, role.ToString("D")));
    }
}