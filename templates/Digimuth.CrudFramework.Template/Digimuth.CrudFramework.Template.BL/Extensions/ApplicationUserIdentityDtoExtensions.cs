using System.Linq;
using Digimuth.CrudFramework.Template.BL.Dtos.ApplicationUser;
using Digimuth.CrudFramework.Template.Common;

namespace Digimuth.CrudFramework.Template.BL.Extensions;

public static class ApplicationUserIdentityDtoExtensions
{
    public static bool IsAdmin(this ApplicationUserIdentityDto dto)
    {
        return dto.Roles.Contains(ApplicationUserRole.Admin);
    }
}