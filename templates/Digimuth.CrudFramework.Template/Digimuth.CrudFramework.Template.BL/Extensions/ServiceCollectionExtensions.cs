using System.Linq;
using System.Reflection;
using Digimuth.CrudFramework.BL.Extensions;
using Digimuth.CrudFramework.Common.Configuration;
using Digimuth.CrudFramework.Common.Extensions;
using Digimuth.CrudFramework.Template.BL.Dtos.ApplicationUser;
using Digimuth.CrudFramework.Template.EmailEngine.Extensions;
using Digimuth.CrudFramework.Template.Storage;
using Digimuth.CrudFramework.Template.Storage.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Digimuth.CrudFramework.Template.BL.Extensions;

public static class ServiceCollectionExtensions
{
    private const string SERVICES_NAMESPACE = "Digimuth.CrudFramework.Template.BL.Services";
    private const string SERVICE_INTERFACES_NAMESPACE = SERVICES_NAMESPACE + ".Interfaces";

    public static IServiceCollection AddBLDependencies(
        this IServiceCollection services,
        IConfiguration configuration,
        IWebHostEnvironment hostEnvironment,
        AuthenticationConfiguration authenticationConfiguration = null)
    {
        authenticationConfiguration ??= configuration.GetSection("Authentication").Get<AuthenticationConfiguration>();

        return services
            .AddCrudRepository()
            .AddEmailEngine(configuration, hostEnvironment)
            .AddSingleton(authenticationConfiguration)
            // use .AddDbContextFactory<CatalogDbContext, OrganisationDbContext> for multi-tenant
            .AddDbContextFactory<CatalogDbContext>()
            .AddDefaultApplicationUserIdentityService<ApplicationUserIdentityDto, ApplicationUser>()
            .AddAutoMapper(typeof(ServiceCollectionExtensions))
            .AddCommonOrganisationScopingServices()
            .RegisterAllServices();
    }

    private static IServiceCollection RegisterAllServices(this IServiceCollection services)
    {
        var serviceTypes = Assembly.GetAssembly(typeof(ServiceCollectionExtensions))!
            .ExportedTypes
            .Where(t => t.Namespace == SERVICES_NAMESPACE && t.IsClass && !t.IsAbstract)
            .ToList();

        foreach (var serviceType in serviceTypes)
        {
            var serviceInterface = serviceType.GetInterfaces()
                .FirstOrDefault(i => i.Namespace == SERVICE_INTERFACES_NAMESPACE
                                     && i.GenericTypeArguments.Length == serviceType.GenericTypeArguments.Length);

            if (serviceInterface != null)
            {
                services.AddTransient(serviceInterface, serviceType);
            }
        }

        return services;
    }
}