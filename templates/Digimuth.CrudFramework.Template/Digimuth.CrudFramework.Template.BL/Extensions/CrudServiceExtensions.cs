using Digimuth.CrudFramework.BL.Services.Interfaces;
using Digimuth.CrudFramework.Template.BL.Dtos.ApplicationUser;

namespace Digimuth.CrudFramework.Template.BL.Extensions;

public static class CrudServiceExtensions
{
    public static ApplicationUserIdentityDto GetContextUser<T1, T2, T3, T4>(this ICrudService<T1, T2, T3, T4> service)
    {
        return service.ContextUser as ApplicationUserIdentityDto;
    }
}