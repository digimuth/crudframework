using AutoMapper;
using Digimuth.CrudFramework.Template.BL.Dtos.ApplicationUser;
using Digimuth.CrudFramework.Template.Storage.Models;

namespace Digimuth.CrudFramework.Template.BL.MappingProfiles;

public class ApplicationUserProfile : Profile
{
    public ApplicationUserProfile()
    {
        CreateMap<ApplicationUser, ApplicationUserDto>().ReverseMap();
        CreateMap<ApplicationUser, ApplicationUserDetailsDto>().ReverseMap();
        CreateMap<ApplicationUser, ApplicationUserIdentityDto>().ReverseMap();
        CreateMap<ApplicationUserRegisterDto, ApplicationUser>()
            .ForSourceMember(d => d.Password, o => o.DoNotValidate())
            .ForSourceMember(d => d.Id, o => o.DoNotValidate())
            .ReverseMap();
    }
}