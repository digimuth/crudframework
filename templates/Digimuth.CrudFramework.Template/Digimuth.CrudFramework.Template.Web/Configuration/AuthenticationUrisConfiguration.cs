namespace Digimuth.CrudFramework.Template.Web.Configuration;

public class AuthenticationUrisConfiguration
{
    public string[] RedirectUris { get; set; }
}