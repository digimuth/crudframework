using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using AutoMapper;
using Digimuth.CrudFramework.Common;
using Digimuth.CrudFramework.Storage.Extensions;
using Digimuth.CrudFramework.Template.BL.Dtos.ApplicationUser;
using Digimuth.CrudFramework.Template.BL.Extensions;
using Digimuth.CrudFramework.Template.Common;
using Digimuth.CrudFramework.Template.Storage;
using Digimuth.CrudFramework.Template.Storage.Models;
using Digimuth.CrudFramework.Template.Web.Extensions;
using Digimuth.CrudFramework.Web.Extensions;
using LettuceEncrypt;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Digimuth.CrudFramework.Template.Web;

public class Startup
{
    public IConfiguration Configuration { get; }
    public IWebHostEnvironment Environment { get; }

    public Startup(IConfiguration configuration, IWebHostEnvironment environment)
    {
        Configuration = configuration;
        Environment = environment;
    }

    public void ConfigureServices(IServiceCollection services)
    {
        services.AddDbContext<CatalogDbContext>();
        services.AddDistributedMemoryCache(); // use SQL Server or Redis for multi-tenant

        services.AddSession(idleHoursTimeout: 4);

        services.AddControllers()
            .AddJsonMergePatch();

        services.AddIdentity(Configuration);

        services.AddSwagger();

        services.AddUrlHelper()
            .AddContextUserActionFilter()
            .AddBLDependencies(Configuration, Environment);

        services.AddSpaStaticFiles(configuration =>
        {
            configuration.RootPath = "Frontend/build";
        });

        services.AddLettuceEncrypt()
            .PersistDataToDirectory(new DirectoryInfo("certs"), null);
    }

    public void Configure(
        IApplicationBuilder app,
        IWebHostEnvironment env,
        UserManager<ApplicationUser> userManager,
        CatalogDbContext dbContext,
        IMapper mapper)
    {
        if (env.IsDevelopment() || env.EnvironmentName == "Container")
        {
            app.UseDeveloperExceptionPage();
            app.UseSwaggerGeneration("Digimuth.CrudFramework.Template.Web", "Digimuth.CrudFramework.Template.Web");

            dbContext.Database.Migrate();
        }

        var initialUsers = SeedUsers(userManager, mapper, Configuration);
        if (initialUsers.Count > 0)
        {
            dbContext.Seed(Configuration);
        }

        app.UseSession();
        app.UseAuthentication();
        app.UseIdentityServer();
        app.UseHttpsRedirection();
        app.UseStaticFiles();
        app.UseSpaStaticFiles();
        app.UseRouting();
        app.UseAuthorization();

        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllers();
        });

        app.UseSpa(spa =>
        {
            spa.Options.SourcePath = "Frontend/build";

            if (env.IsDevelopment())
            {
                spa.UseProxyToSpaDevelopmentServer("http://localhost:3000");
            }
        });
    }

    private static List<ApplicationUserRegisterDto> SeedUsers(UserManager<ApplicationUser> userManager, IMapper mapper, IConfiguration configuration)
    {
        var initialUsers = configuration.GetSection("InitialUsers").Get<List<ApplicationUserRegisterDto>>();

        foreach (var user in initialUsers)
        {
            if (!userManager.Users.Any(u => u.Email == user.Email))
            {
                var applicationUser = mapper.Map<ApplicationUser>(user);
                applicationUser.CreatedById = null;
                applicationUser.UpdatedById = null;
                applicationUser.EmailConfirmed = true;
                applicationUser.CreatedAt = DateTime.Now;

                userManager.CreateAsync(applicationUser, user.Password)
                    .GetAwaiter()
                    .GetResult();

                userManager.AddClaimAsync(applicationUser,
                        new Claim(CrudClaimTypes.Role, ApplicationUserRole.Admin.ToString("D")))
                    .GetAwaiter()
                    .GetResult();
            }
        }

        return initialUsers;
    }
}
