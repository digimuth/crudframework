using System;
using System.Collections.Generic;
using Digimuth.CrudFramework.IdentityServer.Extensions;
using Digimuth.CrudFramework.Template.Common;
using Digimuth.CrudFramework.Template.Storage;
using Digimuth.CrudFramework.Template.Storage.Models;
using Digimuth.CrudFramework.Template.Web.Configuration;
using Digimuth.CrudFramework.Web.Extensions;
using Digimuth.CrudFramework.Web.Services;
using IdentityServer4.Models;
using Microsoft.AspNetCore.ApiAuthorization.IdentityServer;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;

namespace Digimuth.CrudFramework.Template.Web.Extensions;

public static class ServiceCollectionExtensions
{
    // customise swagger ui security here
    public static IServiceCollection AddSwagger(this IServiceCollection services)
    {
        var scheme = new OpenApiSecurityScheme
        {
            Name = "oauth2",
            Type = SecuritySchemeType.OAuth2,
            Flows = new OpenApiOAuthFlows
            {
                AuthorizationCode = new OpenApiOAuthFlow
                {
                    TokenUrl = new Uri("/connect/token", UriKind.Relative),
                    AuthorizationUrl = new Uri("/connect/authorize", UriKind.Relative),
                    Scopes = new Dictionary<string, string>
                    {
                        {"offline_access", "Get refresh token"},
                        {"profile", "User profile"},
                        {"openid", "Open ID"},
                        {"Digimuth.CrudFramework.Template.WebAPI", "API access"},
                    }
                }
            }
        };

        return services.AddSwaggerGeneration(scheme);
    }

    // customise identity server options here
    public static void AddIdentity(this IServiceCollection services, IConfiguration configuration)
    {
        var redirectUris = configuration.GetSection("Authentication")
            .Get<AuthenticationUrisConfiguration>().RedirectUris;
        var builder = ClientBuilder.SPA("Digimuth.CrudFramework.Template.Web")
            .WithLogoutRedirectUri("/authentication/logout-callback");

        foreach (var redirectUri in redirectUris)
        {
            builder = builder.WithRedirectUri(redirectUri);
            builder = builder.WithLogoutRedirectUri(redirectUri);
        }

        var client = builder.Build();
        client.AllowedGrantTypes.Clear();
        client.AllowedGrantTypes.Add(GrantType.AuthorizationCode);
        client.RequirePkce = true;
        client.AllowOfflineAccess = true;
        client.AlwaysIncludeUserClaimsInIdToken = true;
        client.IdentityProviderRestrictions = Array.Empty<string>();
        client.AccessTokenLifetime = 15 * 60; // 15 minutes
        client.AbsoluteRefreshTokenLifetime = 15 * 24 * 60; // 15 days
        client.SlidingRefreshTokenLifetime = 5 * 24 * 60; // 5 days
        client.UpdateAccessTokenClaimsOnRefresh = true;
        client.AccessTokenType = AccessTokenType.Jwt;

        services.AddIdentityServer<ApplicationUser, CatalogDbContext>(options =>
        {
            options.UserInteraction.LoginUrl = "/login";
            options.UserInteraction.LogoutUrl = "/api/authentication/logout";
        }, client).AddProfileService<ProfileWithRolesService<ApplicationUser, ApplicationUserRole>>();
        services.AddAuthentication().AddIdentityServerJwt();

        services.AddIdentityServerCertificateCreationServices(configuration);

        //Uncomment for polish error communicates
        //services.AddScoped<IdentityErrorDescriber, PolishIdentityErrorDescriber>();
    }
}