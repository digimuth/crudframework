using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Digimuth.CrudFramework.BL.Dtos.ApplicationUser;
using Digimuth.CrudFramework.Template.BL.Dtos.ApplicationUser;
using Digimuth.CrudFramework.Template.BL.Services.Interfaces;
using Digimuth.CrudFramework.Template.Common;
using Digimuth.CrudFramework.Web.Controllers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Digimuth.CrudFramework.Template.Web.Controllers;

[ApiController]
[Route("api/[controller]")]
public class ApplicationUserController
    : CrudControllerBase<
        ApplicationUserDto,
        ApplicationUserDetailsDto,
        ApplicationUserRegisterDto,
        ApplicationUserRegisterDto>
{
    private readonly IApplicationUserService _applicationUserService;
    private readonly ILogger<ApplicationUserController> _logger;

    public ApplicationUserController(
        IApplicationUserService applicationUserService,
        ILogger<ApplicationUserController> logger)
        : base(applicationUserService, logger)
    {
        _applicationUserService = applicationUserService;
        _logger = logger;
    }


    [HttpPost("{id}/roles")]
    public async Task<ActionResult> PutRoles(long id, [FromBody] IEnumerable<ApplicationUserRole> roles)
    {
        if (!await _applicationUserService.IsAuthorizedForCreateAsync(new ApplicationUserRegisterDto()))
        {
            return Forbid();
        }

        await _applicationUserService.UpdateUserRolesAsync(id, roles.Cast<int>());

        return Ok();
    }

    [AllowAnonymous]
    [HttpPost("activate")]
    public async Task<IActionResult> Activate(ApplicationUserActivationDto dto)
    {
        if (!ModelState.IsValid)
        {
            return BadRequest(ModelState);
        }

        try
        {
            await _applicationUserService.ActivateUserAsync(dto);
        }
        catch (ValidationException ex)
        {
            _logger.LogError(ex, "Activate failed for email ({0}).", dto.Email);

            return BadRequest(ex.Message);
        }

        return Ok();
    }

    [AllowAnonymous]
    [HttpPost("beginForgotPassword")]
    public async Task<IActionResult> ForgotPasswordFlow(StartForgotPasswordDto dto)
    {
        if (!ModelState.IsValid)
        {
            return BadRequest(ModelState);
        }

        await _applicationUserService.StartForgotPasswordFlowAsync(dto.Email);

        return Ok();
    }

    [AllowAnonymous]
    [HttpPost("endForgotPassword")]
    public async Task<IActionResult> FinishForgotPasswordFlow(EndForgotPasswordDto dto)
    {
        if (!ModelState.IsValid)
        {
            return BadRequest(ModelState);
        }

        try
        {
            await _applicationUserService.EndForgotPasswordFlowAsync(dto);
        }
        catch (ValidationException ex)
        {
            _logger.LogError(ex, $"FinishForgotPasswordFlow failed for email ({dto.Email})");

            return BadRequest(ex.Message);
        }

        return Ok();
    }
}
