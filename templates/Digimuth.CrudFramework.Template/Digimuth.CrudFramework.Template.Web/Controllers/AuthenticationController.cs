﻿using Digimuth.CrudFramework.Common.Configuration;
using Digimuth.CrudFramework.IdentityServer.Controllers;
using Digimuth.CrudFramework.Template.Storage.Models;
using Digimuth.CrudFramework.Web.Controllers;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Digimuth.CrudFramework.Template.Web.Controllers;

[AllowAnonymous]
[Route("api/[controller]")]
public class AuthenticationController : AuthenticationControllerBase<ApplicationUser>
{
    public AuthenticationController(
        SignInManager<ApplicationUser> signInManager,
        UserManager<ApplicationUser> userManager,
        ILogger<AuthenticationControllerBase<ApplicationUser>> logger,
        IIdentityServerInteractionService identityServerInteractionService,
        AuthenticationConfiguration configuration) : base(signInManager,
        userManager,
        logger,
        identityServerInteractionService, configuration)
    {
    }

    protected override string GetExternalResponseUrl(string returnUrl)
    {
        return Url.Action(nameof(Callback), "Authentication", new { returnUrl }, Request.Scheme);
    }
}