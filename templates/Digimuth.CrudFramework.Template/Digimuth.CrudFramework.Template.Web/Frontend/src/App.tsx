import './App.less';
import {Provider as InversifyProvider} from 'inversify-react';
import {Provider, useDispatch} from 'react-redux'
import {store} from './redux/store';
import RoutingContainer, {RoutingPaths} from "./pages/routing/routingContainer";
import React, {useEffect} from "react";
import {unstable_HistoryRouter as HistoryRouter} from "react-router-dom";
import ApiService from "./services/apiService";
import SiderMenu from './pages/components/siderMenu/siderMenu';
import HeaderContainer from './pages/components/header/headerContainer';
import FooterContainer from './pages/components/footer/footerContainer';
import {CrudBreadcrumb, CrudLayout} from 'digimuth-components/dist';
import {Container} from 'inversify';
import {AuthProvider, AuthProviderProps, initUserManager, useAuth, UserManager} from 'oidc-react';
import {createBrowserHistory} from "history";
import {ConfigProvider} from "antd";

const authSettings: AuthProviderProps = {
    authority: process.env.REACT_APP_URL,
    clientId: "Digimuth.CrudFramework.Template.Web",
    redirectUri: process.env.REACT_APP_URL,
    autoSignIn: false,
    automaticSilentRenew: true,
    scope: "openid Digimuth.CrudFramework.Template.WebAPI profile offline_access",
    loadUserInfo: true
};

export const userManager: UserManager | undefined = initUserManager(authSettings);

function App() {

    const container = new Container();
    const history = createBrowserHistory();
    container.bind(ApiService).toSelf().inSingletonScope();

    return (
        /* // @ts-ignore */
        <AuthProvider
            userManager={userManager}
            onSignIn={u => {
                const anyState = u?.state as any;
                const fromState = anyState?.targetPath;
                const targetUrl = fromState ? fromState : "/";
                history.replace(targetUrl);
            }}
            autoSignIn={false}>
            <Provider store={store}>
                <InversifyProvider container={container}>
                    <HistoryRouter history={history}>
                        <Layout/>
                    </HistoryRouter>
                </InversifyProvider>
            </Provider>
        </AuthProvider>
    )
}

const CrudDefaultTheme = {
    components: {
        Button: {
            colorBgContainerDisabled: "#E5EDFB",
            controlHeight: 34,
        },
        Input: {
            controlHeight: 34
        },
        Layout: {
            colorBgHeader: "#2A2A2A",
            colorBgTrigger: "#3A3A3A",
            colorBgBody: "#fefefe",
        }
    },
    token: {
        borderRadius: 24,
        colorPrimary: "#3662DB",
        colorWarning: "#DE6826",

    },
}

function Layout() {
    const auth = useAuth();

    return <ConfigProvider theme={CrudDefaultTheme}>
        <CrudLayout
            breadcrumb={<CrudBreadcrumb routingDefinitions={RoutingPaths}/>}
            siderMenu={<SiderMenu/>}
            header={<HeaderContainer/>}
            footerContent={<FooterContainer/>}
            showSider={!!auth.userData}
        >
            <RoutingContainer/>
        </CrudLayout>
    </ConfigProvider>;
}

export default App;
