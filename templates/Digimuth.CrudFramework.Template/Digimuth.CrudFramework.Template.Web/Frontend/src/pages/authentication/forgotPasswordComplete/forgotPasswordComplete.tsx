import {CrudForm, CrudFormItemProps, CrudFormWrapperProps, nameof} from "digimuth-components/dist";
import {EndForgotPasswordDto} from "../../../api";
import React, {useState} from "react";
import {Form} from "antd";
import CheckPasswordComponent from "../subcomponents/checkPasswordComponent";
import CheckConfirmPasswordComponent from "../subcomponents/checkConfirmPasswordComponent";
import styles from "./forgotPasswordCompleteContainer.module.less";

const layout = {
    labelCol: {span: 24},
    wrapperCol: {span: 24},
    layout: 'vertical',
    requiredMark: false
};

const tailLayout = {
    wrapperCol: {span: 24},
};

export default function ForgotPasswordComplete(props: CrudFormWrapperProps<EndForgotPasswordDto>) {
    const [passwordIsValid, setPasswordIsValid] = useState(false);
    const [passwordsAreEqual, setPasswordsAreEqual] = useState(false);
    const [form] = Form.useForm();

    const formItems: CrudFormItemProps[] = [{
        label: "New password",
        name: "password",
        children: <CheckPasswordComponent onPasswordCheck={setPasswordIsValid}/>
    }, {
        label: "Confirm password",
        name: nameof<EndForgotPasswordDto>("newPassword"),
        dependencies: ["password"],
        children: <CheckConfirmPasswordComponent form={form} onPasswordCheck={setPasswordsAreEqual}/>
    }];

    return <CrudForm
        isSubmitDisabled={!passwordIsValid || !passwordsAreEqual}
        form={form}
        onSubmit={props.onSubmit}
        formItems={formItems}
        saveButtonTitle="Set new password"
        formProps={layout}
        tailProps={tailLayout}
        submitButtonProps={{className: styles.submitButton}}
    />;
}
