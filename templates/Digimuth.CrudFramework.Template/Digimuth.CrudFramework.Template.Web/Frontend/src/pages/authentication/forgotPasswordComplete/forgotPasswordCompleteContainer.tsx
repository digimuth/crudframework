import {useInjection} from "inversify-react";
import ApiService from "../../../services/apiService";
import {Navigate, useLocation} from "react-router-dom";
import {RoutingPaths} from "../../routing/routingContainer";
import React from "react";
import {ApplicationUserApi, EndForgotPasswordDto} from "../../../api";
import ForgotPasswordComplete from "./forgotPasswordComplete";

export default function ForgotPasswordCompleteContainer(props: { onAfterSubmit: () => void }) {
    const apiService = useInjection(ApiService);
    const location = useLocation();

    const searchParams = new URLSearchParams(location.search);
    const token = searchParams.get("token");
    const email = searchParams.get("email");

    if (!token) {
        return <Navigate to={RoutingPaths.login.route} replace={true}/>;
    }

    const onSubmit = async (value: EndForgotPasswordDto): Promise<any> => {
        try {
            value.token = token;
            value.email = email;

            await apiService.getApi(ApplicationUserApi).apiApplicationUserEndForgotPasswordPost(value);

            props.onAfterSubmit();
        } catch (e: any) {
            console.log(e);

            return e.response?.data;
        }
    }

    return <ForgotPasswordComplete onSubmit={onSubmit}/>;
}
