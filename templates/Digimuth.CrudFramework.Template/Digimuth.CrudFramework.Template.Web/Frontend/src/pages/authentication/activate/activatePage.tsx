import { Result } from "antd";
import { RoutingPaths } from "../../routing/routingContainer";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import ActivateContainer from "./activateContainer";
import { AsyncButton } from "digimuth-components/dist";
import AuthenticationLayout from "../authenticationLayout";

export default function ActivatePage() {
    const [submitted, setSubmitted] = useState<boolean>(false);

    return <AuthenticationLayout title={submitted ? undefined : "Dokończ rejestrację"}>
        {submitted
            ? <ActivateSuccessResult />
            : <ActivateContainer onAfterSubmit={() => setSubmitted(true)} />
        }
    </AuthenticationLayout>;
}

function ActivateSuccessResult() {
    const navigate = useNavigate();

    const goToLogin = () => {
        navigate(RoutingPaths.login.route, { replace: true });
    }

    return <Result
        title="Pomyślnie ustawiono nowe hasło. Możesz teraz rozpocząć korzystanie z serwisu"
        status="success"
        extra={<AsyncButton type="default" onClick={goToLogin}>Zaloguj się</AsyncButton>}
    />;
}