import { Form } from "antd";
import { CrudForm, CrudFormItemProps, CrudFormWrapperProps, nameof } from "digimuth-components/dist";
import { ApplicationUserActivationDto } from "../../../api";
import styles from './activate.module.less';
import { useState } from "react";
import CheckPasswordComponent from "../subcomponents/checkPasswordComponent";
import CheckConfirmPasswordComponent from "../subcomponents/checkConfirmPasswordComponent";

const layout = {
    labelCol: { span: 24 },
    wrapperCol: { span: 24 },
    layout: 'vertical',
    requiredMark: false
};

const tailLayout = {
    wrapperCol: { span: 24 },
};

export default function Activate(props: CrudFormWrapperProps<ApplicationUserActivationDto>) {
    const [passwordIsValid, setPasswordIsValid] = useState(false);
    const [passwordsAreEqual, setPasswordsAreEqual] = useState(false);
    const [form] = Form.useForm();

    const formItems: CrudFormItemProps[] = [
        {
            label: "Hasło",
            name: nameof<ApplicationUserActivationDto>("password"),
            children: <CheckPasswordComponent onPasswordCheck={setPasswordIsValid} />
        },
        {
            label: "Potwierdź hasło",
            name: "newPassword",
            dependencies: ["password"],
            children: <CheckConfirmPasswordComponent form={form} onPasswordCheck={setPasswordsAreEqual} />
        }
    ];

    return <>
        <CrudForm
            isSubmitDisabled={!passwordIsValid || !passwordsAreEqual}
            form={form}
            onSubmit={props.onSubmit}
            formItems={formItems}
            saveButtonTitle="Zarejestruj"
            formProps={layout}
            tailProps={tailLayout}
            submitButtonProps={{ className: styles.submitButton }}
        />
    </>;
}
