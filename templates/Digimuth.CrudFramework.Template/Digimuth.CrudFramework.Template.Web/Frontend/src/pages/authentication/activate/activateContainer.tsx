import { useInjection } from "inversify-react";
import React from "react";
import { Navigate, useLocation } from "react-router-dom";
import { ApplicationUserActivationDto, ApplicationUserApi } from "../../../api";
import ApiService from "../../../services/apiService";
import { RoutingPaths } from "../../routing/routingContainer";
import Activate from "./activate";

export default function ActivateContainer(props: { onAfterSubmit: () => void }) {
    const apiService = useInjection(ApiService);
    const location = useLocation();

    const searchParams = new URLSearchParams(location.search);
    const token = searchParams.get("token");
    const email = searchParams.get("email");

    if (!token) {
        return <Navigate to={RoutingPaths.login.route} replace={true} />;
    }

    const onSubmit = async (value: ApplicationUserActivationDto): Promise<any> => {
        try {
            value.token = token;
            value.email = email;

            await apiService.getApi(ApplicationUserApi).apiApplicationUserActivatePost(value);

            props.onAfterSubmit();
        } catch (e: any) {
            console.log(e);

            return e.response?.data;
        }
    }

    return <Activate onSubmit={onSubmit} />;
}