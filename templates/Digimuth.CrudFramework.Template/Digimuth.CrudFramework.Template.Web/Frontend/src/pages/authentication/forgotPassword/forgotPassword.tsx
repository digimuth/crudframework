import Layout, {Content} from 'antd/lib/layout/layout';
import {CrudForm, CrudFormItemProps, CrudFormWrapperProps} from "digimuth-components/dist";
import styles from "./forgotPassword.module.less";

const layout = {
    labelCol: {span: 8},
    wrapperCol: {span: 12},
};

const tailLayout = {
    wrapperCol: {offset: 8, span: 4},
};

const formItems: CrudFormItemProps[] = [{
    name: "email",
    label: "Email",
    rules: [
        {required: true, message: 'This field is required'},
        {type: "email", message: "Email format is incorrect"}
    ]
}];

export default function ForgotPassword(props: CrudFormWrapperProps<any>) {
    return <Layout>
        <Content className={styles.content}>
            <CrudForm
                onSubmit={props.onSubmit}
                formItems={formItems}
                saveButtonTitle="Reset password"
                formProps={layout}
                tailProps={tailLayout}/>
        </Content>
    </Layout>;
}
