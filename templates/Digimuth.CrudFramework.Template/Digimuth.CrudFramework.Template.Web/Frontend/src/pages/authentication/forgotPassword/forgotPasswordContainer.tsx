import {message} from "antd";
import React from "react";
import {RoutingPaths} from "../../routing/routingContainer";
import ForgotPassword from "./forgotPassword";
import {useNavigate} from "react-router-dom";
import {useInjection} from "inversify-react";
import ApiService from "../../../services/apiService";
import {ApplicationUserApi} from "../../../api";

export default function ForgotPasswordContainer() {
    const navigate = useNavigate();
    const apiService = useInjection(ApiService);

    const onSubmit = async (value: any): Promise<any> => {
        message.info("If the email address you provided belongs to an account, you'll receive an email with instructions on how to reset your password.");
        try {
            await apiService.getApi(ApplicationUserApi).apiApplicationUserBeginForgotPasswordPost(value);
        } catch (e: any) {
            console.log(e);
            return e.response?.data;
        }
        navigate(RoutingPaths.login.route);
    }

    return <>
        <ForgotPassword onSubmit={onSubmit}/>
    </>;
}
