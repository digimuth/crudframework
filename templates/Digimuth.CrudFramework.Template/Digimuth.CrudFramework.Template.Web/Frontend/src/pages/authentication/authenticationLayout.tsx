import styles from "./authenticationLayout.module.less"
import Title from "antd/lib/typography/Title";
import { ReactElement } from "react";
import { useAuth } from "oidc-react";
import { Navigate } from "react-router-dom";
import { Layout } from "antd";

export default function AuthenticationLayout(props: AuthenticationLayoutProps) {
    const auth = useAuth();

    if (auth.userData) {
        return <Navigate to={"/"} replace={true} />;
    }

    return <Layout className={styles.layout}>
        {props.title && <Title>{props.title}</Title>}
        {props.children}
    </Layout>;
}

interface AuthenticationLayoutProps {
    title?: string,
    children: ReactElement
}