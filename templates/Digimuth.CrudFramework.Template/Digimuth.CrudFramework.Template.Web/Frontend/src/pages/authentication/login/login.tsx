import {Button, Input, Switch} from 'antd';
import {AsyncSwitch, CrudForm, CrudFormItemProps} from 'digimuth-components/dist';
import React from "react";

const layout = {
    labelCol: {span: 8},
    wrapperCol: {span: 12},
};

const tailLayout = {
    wrapperCol: {offset: 8, span: 4},
};

const formItems: CrudFormItemProps[] = [{
    label: "Username",
    name: "email",
    rules: [{required: true, message: 'This field is required'}]
}, {
    label: "Password",
    name: "password",
    rules: [{required: true, message: 'This field is required'}],
    children: <Input.Password/>
}, {
    label: "Remember me",
    name: "rememberMe",
    children: <AsyncSwitch/>
}];

export default function Login(props: LoginProps) {
    const initValues = {
        "Input.RememberMe": false
    };

    return <CrudForm
        onSubmit={props.onLogin}
        formItems={formItems}
        initialValues={initValues}
        saveButtonTitle="Login"
        formProps={layout}
        tailProps={tailLayout}
        extraActions={<Button type="link" onClick={props.onForgotPassword}>Forgot password?</Button>}/>
}

interface LoginProps {
    onLogin: (value: LoginViewModel) => Promise<boolean>,
    onForgotPassword: () => any
}

export interface LoginViewModel {
    username: string,
    password: string,
    rememberMe: boolean
}