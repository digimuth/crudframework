import Login from "./login";
import {useInjection} from "inversify-react";
import ApiService from "../../../services/apiService";
import {useAuth} from "oidc-react";
import {AuthenticationApi, LoginDto} from "../../../api";
import React from "react";
import { useNavigate, useSearchParams } from "react-router-dom";


export default function LoginContainer() {

    const navigate = useNavigate();
    const apiService = useInjection(ApiService);
    const [searchParams, _] = useSearchParams();

    const onLogin = async (value: any): Promise<any> => {
        try {
            const loginDto = {
                email: value.email,
                password: value.password,
                rememberMe: value.rememberMe
            } as LoginDto;
            // TODO: Figure out correct returnUrl

            const response = await apiService.getApi(AuthenticationApi)
                .apiAuthenticationLoginPost("", loginDto);

            const returnUrl = searchParams.get("ReturnUrl");

            if (returnUrl) {
                window.location.href = returnUrl;
                return;
            }

            navigate("/");
        } catch (e: any) {
            console.log(e);
            return e.response?.data;
        }
    }

    const onForgotPassword = () => {
        navigate('/forgotPassword');
    }

    return <Login onForgotPassword={onForgotPassword} onLogin={onLogin}/>
}
