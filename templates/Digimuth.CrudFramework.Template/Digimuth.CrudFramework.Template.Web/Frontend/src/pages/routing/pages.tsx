import React from "react";
import { loadingSpin } from "./routingContainer";
import { AuthInitialized, Protected } from "./routingWrappers";

const CrudTableExample = React.lazy(() => import("../components/crudTableExample/crudTableExample"));
const CrudFormExample = React.lazy(() => import("../components/crudFormExample"));

const Main = React.lazy(() => import("../components/mainPage"));
const Login = React.lazy(() => import( "../components/loginPage"));
const Users = React.lazy(() => import("../applicationUser/applicationUserListContainer"));

const ForgotPassword = React.lazy(() => import("../components/forgotPassword"));
const ForgotPasswordComplete = React.lazy(() => import("../components/forgotPasswordComplete"));
const Activate = React.lazy(() => import("../authentication/activate/activatePage"));

export const LoginPage = () => <AuthInitialized loadingIndicator={loadingSpin}>
    <Login />
</AuthInitialized>;

export const ForgotPasswordPage = () => <AuthInitialized loadingIndicator={loadingSpin}>
    <ForgotPassword />
</AuthInitialized>;

export const ForgotPasswordCompletePage = () => <AuthInitialized loadingIndicator={loadingSpin}>
    <ForgotPasswordComplete/>
</AuthInitialized>

export const ActivatePage = () => <AuthInitialized loadingIndicator={loadingSpin}>
    <Activate />
</AuthInitialized>;

export const CrudTableExamplePage = () => <Protected loadingIndicator={loadingSpin}>
    <CrudTableExample />
</Protected>

export const CrudFormExamplePage = () => <Protected loadingIndicator={loadingSpin}>
    <CrudFormExample />
</Protected>

export const UsersPage = () => <Protected loadingIndicator={loadingSpin}>
    <Users />
</Protected>

export const MainPage = () => <Protected loadingIndicator={loadingSpin}>
    <Main />
</Protected>
