import React from "react";
import { Route, Routes } from "react-router-dom";
import { RoutingDefinition } from "digimuth-components/dist/components/crudBreadcrumb/crudBreadcrumb";
import styles from "./routingContainer.module.less";
import { LoadingOutlined } from "@ant-design/icons";
import { Spin } from "antd";
import * as Pages from "./pages";

const mainPage = { route: "/", name: "Strona główna" };
const crudTable = { route: "/crudTable", name: "Crud table example" }
const crudForm = { route: "/crudForm", name: "Crud form example" }
const users = { route: "/users", name: "Users" }

export const RoutingPaths: { [key: string]: RoutingDefinition } = {
    login: { route: "/login" },
    activate: { route: "/activate" },
    forgotPassword: { route: "/forgotPassword" },
    forgotPasswordComplete: {route: "/forgotPasswordComplete"},

    crudTable: { route: "/crudTable", breadcrumbEntries: [ mainPage, crudTable ] },
    crudForm: { route: "/crudForm", breadcrumbEntries: [ mainPage, crudForm ] },
    users: { route: "/users", breadcrumbEntries: [ mainPage, users ] },
};

const loadingIcon = <LoadingOutlined className={styles.loadingIcon} spin />;
export const loadingSpin = <Spin className={styles.loadingContainer} indicator={loadingIcon} />;

export default function RoutingContainer() {
    return <Routes>
        <Route path={RoutingPaths.crudTable.route} element={<Pages.CrudTableExamplePage />} />
        <Route path={RoutingPaths.crudForm.route} element={<Pages.CrudFormExamplePage />} />
        <Route path={RoutingPaths.login.route} element={<Pages.LoginPage />} />
        <Route path={RoutingPaths.users.route} element={<Pages.UsersPage />} />

        <Route path={RoutingPaths.forgotPassword.route} element={<Pages.ForgotPasswordPage />} />
        <Route path={RoutingPaths.forgotPasswordComplete.route} element={<Pages.ForgotPasswordCompletePage/>}/>
        <Route path={RoutingPaths.activate.route} element={<Pages.ActivatePage />} />

        <Route path={"/"} element={<Pages.MainPage />} />
    </Routes>
}
