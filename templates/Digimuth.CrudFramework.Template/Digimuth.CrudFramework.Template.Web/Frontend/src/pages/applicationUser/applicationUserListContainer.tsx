import {useInjection} from "inversify-react";
import {useCallback, useEffect, useState} from "react";
import ApiService from "../../services/apiService";
import {ApplicationUserApi, ApplicationUserDto} from "../../api";
import ApplicationUsersList from "./applicationUserList";
import ApplicationUserModalFormAddContainer
    from "./applicationUserModalFormAddContainer/applicationUserModalFormAddContainer";
import {Layout} from "antd";
import {Content} from "antd/lib/layout/layout";
import styles from "./applicationUserListContainer.module.less";

export default function ApplicationUsersListContainer() {
    const apiService = useInjection(ApiService);
    const [applicationUsers, setApplicationUsers] = useState<ApplicationUserDto[] | null>();
    const [isAddFormVisible, setAddFormVisible] = useState(false);
    const [applicationUserModalFormContainerTitle, setApplicationUserModalFormContainerTitle] = useState<string>();
    const [applicationUserToEdit, setApplicationUserToEdit] = useState<ApplicationUserDto | null>(null);
    const [idApplicationUserToEdit, setIdApplicationUserToEdit] = useState<number | null>(null);

    const refreshApplicationUsersList = useCallback(async () => {
        try {
            const applicationUsers = await apiService.getApi(ApplicationUserApi).apiApplicationUserGet();
            setApplicationUsers(applicationUsers.data);
        } catch (e: any) {
            console.log(e.response?.data);
        }
    }, [apiService]);

    useEffect(() => {
        refreshApplicationUsersList();
    }, [refreshApplicationUsersList]);

    const onAddClicked = () => {
        setApplicationUserModalFormContainerTitle("Zaproś użytkownika");
        setApplicationUserToEdit(null);
        setIdApplicationUserToEdit(null);
        setAddFormVisible(true);
    }

    const onEditClicked = async (id: number) => {
        setApplicationUserModalFormContainerTitle(`Edit ApplicationUser with id: ${id}`);

        const response = await apiService.getApi(ApplicationUserApi).apiApplicationUserIdGet(id);

        setApplicationUserToEdit(response.data as ApplicationUserDto);
        setIdApplicationUserToEdit(id);
        setAddFormVisible(true);
    }

    const onDeleteClicked = async (id: number) => {
        const response = await apiService.getApi(ApplicationUserApi).apiApplicationUserIdDelete(id)

        if (response.request.status !== 200) {
            console.log("Error occured while deleting data"); //add some warning?
        } else {
            await refreshApplicationUsersList();
        }
    }

    const onSubmit = async (values: ApplicationUserDto) => {
        try {
            if (applicationUserToEdit === null) {
                await apiService.getApi(ApplicationUserApi).apiApplicationUserPost(values);
            } else if (idApplicationUserToEdit !== null) {
                await apiService.getApi(ApplicationUserApi).apiApplicationUserIdPut(idApplicationUserToEdit, values)
            }
        } catch (e: any) {
            return e.response.data;
        }

        setAddFormVisible(false)
        await refreshApplicationUsersList();
    }

    return <div className={styles.content}>
            <ApplicationUsersList
                data={applicationUsers || null}
                onAddClicked={onAddClicked}
                onEditClicked={onEditClicked}
                onDeleteClicked={onDeleteClicked}
            />
            <ApplicationUserModalFormAddContainer
                onFinish={() => setAddFormVisible(false)}
                onSubmit={onSubmit}
                addFormVisibility={isAddFormVisible}
                title={applicationUserModalFormContainerTitle || ""}
                applicationUserToEdit={applicationUserToEdit}
            />
    </div>
}
