import { ApplicationUserDto } from "../../../api";
import ApplicationUserModalForm from "../applicationUserModalForm/applicationUserModalForm";

export default function ApplicationUserModalFormAddContainer(props: ApplicationUserModalFormAddContainerProps) {
    return <ApplicationUserModalForm
        isModalVisible={props.addFormVisibility}
        setModalVisibility={props.onFinish}
        onSubmit={props.onSubmit}
        modalTitle={props.title}
        saveButtonTitle="Submit"
        initialValues={props.applicationUserToEdit!}
    />
}

interface ApplicationUserModalFormAddContainerProps {
    onFinish: (refreshList: boolean) => void,
    onSubmit: (values: ApplicationUserDto) => Promise<any>,
    addFormVisibility: boolean,
    title: string,
    applicationUserToEdit: ApplicationUserDto | null
}
