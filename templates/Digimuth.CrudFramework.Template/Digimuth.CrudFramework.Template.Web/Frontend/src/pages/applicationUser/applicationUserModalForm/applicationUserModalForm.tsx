import { ApplicationUserDto, ApplicationUserRegisterDto, ApplicationUserRole } from "../../../api";
import { CrudFormItemProps, CrudFormModal, nameof } from "digimuth-components/dist";
import { Select } from "antd";

const {Option} = Select;

const formItems: CrudFormItemProps[] = [
    {
        name: nameof<ApplicationUserRegisterDto>("email"),
        rules: [
            { required: true, message: 'To pole jest wymagane' }
        ],
    },
    {
        name: nameof<ApplicationUserRegisterDto>("role"),
        rules: [
            { required: true, message: 'To pole jest wymagane' }
        ],
        children: <Select>
            {Object.keys(ApplicationUserRole).map(role =>
                <Option value={role}>{role}</Option>
            )}
        </Select>
    },
];

export default function ApplicationUserModalForm(props: ApplicationUserModalProps) {
    return <CrudFormModal
        formItems={formItems}
        onSubmit={props.onSubmit}
        onModalCancelClicked={() => props.setModalVisibility(false)}
        initialValues={props.initialValues}
        modalOkText={props.saveButtonTitle}
        modalTitle={props.modalTitle}
        isModalVisible={props.isModalVisible}
    />;
}

interface ApplicationUserModalProps {
    initialValues?: ApplicationUserDto,
    isModalVisible: boolean;
    setModalVisibility: (isVisible: boolean) => any;
    onSubmit: (value: ApplicationUserDto) => Promise<any>,
    modalTitle: string,
    saveButtonTitle: string,
}
