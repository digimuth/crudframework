import { ColumnsType } from "antd/lib/table";
import { ApplicationUserDto } from "../../api";
import { CrudTableWrapperProps } from "digimuth-components/dist/components/crudTable/crudTable";
import { CrudTable, nameof } from "digimuth-components/dist";
import { CheckCircleTwoTone } from "@ant-design/icons";

export default function ApplicationUsersList(props: CrudTableWrapperProps<ApplicationUserDto>) {
    const columns: ColumnsType<ApplicationUserDto> = [
        {
            key: nameof<ApplicationUserDto>("email"),
            dataIndex: nameof<ApplicationUserDto>("email"),
            title: "Email"
        },
        {
            key: nameof<ApplicationUserDto>("emailConfirmed"),
            dataIndex: nameof<ApplicationUserDto>("emailConfirmed"),
            title: "Email Confirmed",
            render: isConfirmed => isConfirmed
                ? <CheckCircleTwoTone twoToneColor="#52c41a" style={{ fontSize: "20px" }} />
                : ""
        },
    ];

    return <CrudTable
        data={props.data}
        onAddClicked={props.onAddClicked}
        onEditClicked={props.onEditClicked}
        onDeleteClicked={props.onDeleteClicked}
        columns={columns}
    />
}
