import styles from "./login.module.less"
import {AsyncSwitch, ColorPicker, CrudForm, CrudFormItemProps} from "digimuth-components/dist";
import {message} from "antd";

const layout = {
    labelCol: { xl: { span: 4 }, lg: { span: 6 } },
    wrapperCol: { xl: { span: 12 }, lg: { span: 18 }, md: { span: 18 } },
};

const tailLayout = {
    wrapperCol: { xl: {offset: 4, span: 4}, lg: { offset: 6, span: 6 } },
};

const formItems: CrudFormItemProps[] = [
    {
        label: "First name",
        name: "firstName",
        rules:[
            {
                required: true,
                message: "First name is required"
            }
        ]
    }, {
        label: "Last name",
        name: "lastName",
        rules:[
            {
                required: true,
                message: "Last name is required"
            }
        ]
    }, {
        label: "Country",
        name: "country"
    }, {
        label: "Favorite color",
        name: "favoriteColor",
        children: <ColorPicker/>
    }, {
        label: "Send SMS notification",
        name: "smsNotification",
        children: <AsyncSwitch/>
    }
]

export default function CrudFormExample() {

    const onSubmit = async (value: any) => {
        message.info("Form submitted")
    }

    const initValues = {
        "country": "Poland"
    }

    return <>
        <h2>Crud form example usage</h2>
        <CrudForm
            formItems={formItems}
            onSubmit={onSubmit}
            formProps={layout}
            tailProps={tailLayout}
            initialValues={initValues}
            saveButtonTitle={"Submit form"}
        />
    </>
}
