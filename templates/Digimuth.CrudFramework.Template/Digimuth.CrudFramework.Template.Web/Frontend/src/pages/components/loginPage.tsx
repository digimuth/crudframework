import styles from "./login.module.less"
import LoginContainer from "../authentication/login/loginContainer";

export default function Login() {
    return <>
        <div className={styles.container}>
            <h2>Login</h2>
            <LoginContainer/>
        </div>
    </>
}
