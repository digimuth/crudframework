import React, {useState} from "react";
import AuthenticationLayout from "../authentication/authenticationLayout";
import ForgotPasswordCompleteContainer from "../authentication/forgotPasswordComplete/forgotPasswordCompleteContainer";
import {useNavigate} from "react-router-dom";
import {Button, Result} from "antd";
import {RoutingPaths} from "../routing/routingContainer";

export default function ForgotPasswordCompletePage() {

    const [submitted, setSubmitted] = useState<boolean>(false);

    return <AuthenticationLayout
        title={submitted ? undefined : "Set new password"}>
        {submitted
            ? <ForgotPasswordCompleteSuccessResult/>
            : <ForgotPasswordCompleteFormContainer onAfterSubmit={() => setSubmitted(true)}/>}
    </AuthenticationLayout>;
}

function ForgotPasswordCompleteFormContainer(props: { onAfterSubmit: () => void }) {
    return <>
        <ForgotPasswordCompleteContainer onAfterSubmit={props.onAfterSubmit}/>
    </>;
}

function ForgotPasswordCompleteSuccessResult() {
    const navigate = useNavigate();

    return <Result
        title="You have successfully set a new password. You can now log in"
        status="success"
        extra={
            <Button type="default" onClick={() => navigate(RoutingPaths.login.route, {replace: true})}>
                Login
            </Button>
        }
    />;
}
