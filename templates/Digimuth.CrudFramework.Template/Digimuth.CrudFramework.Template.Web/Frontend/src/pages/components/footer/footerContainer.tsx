export default function FooterContainer() {
  return <>
    Made by <a href="https://digimuth.com">Digimuth</a>
  </>;
}