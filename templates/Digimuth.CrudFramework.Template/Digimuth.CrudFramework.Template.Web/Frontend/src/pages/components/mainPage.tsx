import { FormOutlined, TableOutlined } from "@ant-design/icons";
import { Card, Menu } from "antd";
import { NavLink } from "react-router-dom";
import { RoutingPaths } from "../routing/routingContainer";

export default function MainPage() {
  return <>
    <h2>Main page</h2>
    <Card title="Examples" style={{ width: 250 }}>
      <Menu>
        <Menu.Item key={RoutingPaths.crudTable.route} icon={<TableOutlined />}>
          <NavLink to={RoutingPaths.crudTable.route}>
            Crud Table
          </NavLink>
        </Menu.Item>
        <Menu.Item key={RoutingPaths.crudForm.route} icon={<FormOutlined />}>
          <NavLink to={RoutingPaths.crudForm.route}>
            Crud Form
          </NavLink>
        </Menu.Item>
      </Menu>
    </Card>
  </>;
}