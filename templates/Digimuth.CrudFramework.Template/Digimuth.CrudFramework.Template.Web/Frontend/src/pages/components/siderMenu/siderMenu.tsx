import {FormOutlined, TableOutlined, UserOutlined} from "@ant-design/icons";
import {ConfigProvider, Menu} from "antd";
import {NavLink} from "react-router-dom";
import {RoutingPaths} from "../../routing/routingContainer";
import styles from "./siderMenu.module.less"
import {useCurrentRoutingPath} from "digimuth-components/dist";

export default function SiderMenu() {
    const route = useCurrentRoutingPath(RoutingPaths)?.route || "";
    const siderDefaultTheme = {
        token:{
            borderRadius:8
        },
        components: {
            Menu: {
                colorItemBg: "#2A2A2A",
            }
        }
    }
    return <ConfigProvider theme={siderDefaultTheme}>
        <NavLink to={"/"}>
            <h3 className={styles.logo}>Logo</h3>
        </NavLink>
        <Menu theme="dark" selectedKeys={[route]} mode="inline">
            <Menu.Item key={RoutingPaths.crudTable.route} icon={<TableOutlined/>}>
                <NavLink to={RoutingPaths.crudTable.route}>
                    Crud Table
                </NavLink>
            </Menu.Item>
            <Menu.Item key={RoutingPaths.crudForm.route} icon={<FormOutlined/>}>
                <NavLink to={RoutingPaths.crudForm.route}>
                    Crud Form
                </NavLink>
            </Menu.Item>
            <Menu.Item key={RoutingPaths.users.route} icon={<UserOutlined/>}>
                <NavLink to={RoutingPaths.users.route}>
                    Users
                </NavLink>
            </Menu.Item>
        </Menu>
    </ConfigProvider>;
}
