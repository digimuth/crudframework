import styles from "./login.module.less"
import ForgotPasswordContainer from "../authentication/forgotPassword/forgotPasswordContainer";

export default function ForgotPassword() {

    return <>
        <div className={styles.container}>
            <h2>Forgot password</h2>
            <ForgotPasswordContainer/>
        </div>
    </>
}
