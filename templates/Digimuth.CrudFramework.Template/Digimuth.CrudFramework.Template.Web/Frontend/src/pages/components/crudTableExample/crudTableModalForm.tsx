import {AsyncSwitch, ColorPicker, CrudFormItemProps, CrudFormModal} from "digimuth-components/dist";
import {Example} from "./crudTableExample";

export default function CrudTableModalForm(props: CrudTableModalFormProps) {
    const formItems: CrudFormItemProps[] = [
        {
            label: "First name",
            name: "firstName",
            rules:[
                {
                    required: true,
                    message: "First name is required"
                }
            ]
        }, {
            label: "Last name",
            name: "lastName",
            rules:[
                {
                    required: true,
                    message: "Last name is required"
                }
            ]
        }, {
            label: "Country",
            name: "country"
        }, {
            label: "Favorite color",
            name: "favoriteColor",
            children: <ColorPicker/>
        }, {
            label: "Send SMS notification",
            name: "smsNotification",
            children: <AsyncSwitch/>
        }
    ]

    return <CrudFormModal
        formItems={formItems}
        onSubmit={props.onSubmit}
        onModalCancelClicked={() => props.setModalVisibility(false)}
        initialValues={props.initialValues}
        modalOkText={props.saveButtonTitle}
        modalTitle={props.modalTitle}
        isModalVisible={props.isModalVisible}
        width={750}
    />;
}

interface CrudTableModalFormProps {
    initialValues?: Example,
    isModalVisible: boolean;
    setModalVisibility: (isVisible: boolean) => any;
    onSubmit: (value: Example) => Promise<any>,
    modalTitle: string,
    saveButtonTitle: string,
}