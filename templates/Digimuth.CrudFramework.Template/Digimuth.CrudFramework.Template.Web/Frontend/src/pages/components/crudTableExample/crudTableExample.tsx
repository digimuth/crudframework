import styles from "../login.module.less"
import {CrudTable, nameof} from "digimuth-components/dist";
import {message} from "antd";
import {useState} from "react";
import {ColumnsType} from "antd/lib/table";
import CrudTableModalForm from "./crudTableModalForm";

export interface Example {
    id?: number | undefined,
    firstName: string,
    lastName: string,
    country: string,
    favoriteColor: string,
    smsNotification: boolean
}

export default function CrudTableExample() {
    const [modalFormContainerTitle, setModalFormContainerTitle] = useState<string>();
    const [isAddFormVisible, setAddFormVisible] = useState(false);
    const [exampleToEdit, setExampleToEdit] = useState<Example | null>(null);


    const data = [
        {
            id: 1,
            firstName: 'Jan',
            lastName: 'Kowalski',
            country: 'Poland',
            favoriteColor: '#42f59b',
            smsNotification: true
        }, {
            id: 2,
            firstName: 'John',
            lastName: 'Travolta',
            country: 'US',
            favoriteColor: '#000000',
            smsNotification: false
        }
    ]

    const columns: ColumnsType<Example> = [
        {
            key: nameof<Example>("id"),
            dataIndex: nameof<Example>("id"),
            title: "Id"
        }, {
            key: nameof<Example>("firstName"),
            dataIndex: nameof<Example>("firstName"),
            title: "First name"
        }, {
            key: nameof<Example>("lastName"),
            dataIndex: nameof<Example>("lastName"),
            title: "Last name"
        }, {
            key: nameof<Example>("country"),
            dataIndex: nameof<Example>("country"),
            title: "Country"
        }, {
            key: nameof<Example>("favoriteColor"),
            dataIndex: nameof<Example>("favoriteColor"),
            title: "Favorite color"
        }, {
            key: nameof<Example>("smsNotification"),
            dataIndex: nameof<Example>("smsNotification"),
            title: "Sms notification",
            render: (smsNotification: boolean) => {
                return <>
                    {smsNotification ? "True" : "False"}
                </>
            }
        }];

    const onAddClicked = () => {
        setModalFormContainerTitle("Add new entry");
        setAddFormVisible(true);
        setExampleToEdit(null);
    }

    const onEditClicked = async (id: number) => {
        setModalFormContainerTitle(`Edit entry with id: ${id}`);
        setExampleToEdit(data.filter(d => d.id === id)[0])
        setAddFormVisible(true);
    }

    const onDeleteClicked = () => {
        message.success("Deleting method triggered");
    }

    const onSubmit = async (values: Example) => {
        setAddFormVisible(false)
        message.success("onSubmit method triggerred");
    }

    return <>
        <h2>Crud table example usage</h2>
        <CrudTable
            data={data || null}
            columns={columns}
            onAddClicked={onAddClicked}
            onDeleteClicked={onDeleteClicked}
            onEditClicked={onEditClicked}
            addButtonTitle="Add new table entry"
        />
        <CrudTableModalForm
            isModalVisible={isAddFormVisible}
            setModalVisibility={() => setAddFormVisible(false)}
            onSubmit={onSubmit}
            modalTitle={modalFormContainerTitle || ""}
            saveButtonTitle="Submit"
            initialValues={exampleToEdit!}
        />
    </>
}
