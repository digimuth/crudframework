using Digimuth.CrudFramework.Storage;
using Digimuth.CrudFramework.Template.Storage.Models;
using Digimuth.CrudFramework.IdentityServer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Digimuth.CrudFramework.Template.Storage;

public class CatalogDbContext : CatalogDbContextBase<ApplicationUser>
{
    public CatalogDbContext(
        DbContextOptions<CatalogDbContext> options,
        IConfiguration configuration)
        : base(options, configuration)
    {
    }
}