using Digimuth.CrudFramework.Common.Models;

namespace Digimuth.CrudFramework.Template.Storage.Models;

public class ApplicationUser : ApplicationUserBase
{
    public string FirstName { get; set; }

    public string LastName { get; set; }
}