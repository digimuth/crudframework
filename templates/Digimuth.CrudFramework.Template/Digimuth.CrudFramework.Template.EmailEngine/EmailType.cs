namespace Digimuth.CrudFramework.Template.EmailEngine;

public enum EmailType
{
    Invitation,
    ForgotPassword
}