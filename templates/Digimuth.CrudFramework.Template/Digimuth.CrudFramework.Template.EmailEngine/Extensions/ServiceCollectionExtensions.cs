﻿using Digimuth.CrudFramework.EmailEngine.Extensions;
using Digimuth.CrudFramework.Template.EmailEngine.Models;
using Digimuth.CrudFramework.Template.EmailEngine.Templates;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Mjml.Net;

namespace Digimuth.CrudFramework.Template.EmailEngine.Extensions;

public static class ServiceCollectionExtensions
{
    private static readonly MjmlRenderer _renderer = new();

    public static IServiceCollection AddEmailEngine(
        this IServiceCollection services,
        IConfiguration configuration,
        IHostEnvironment hostEnvironment)
    {
        var emailEngine = services
        .RegisterEmailTemplate(
            EmailType.Invitation,
            "Zaproszenie do systemu",
            async model => _renderer.Render(await InvitationEmailTemplate.RenderAsync((InvitationEmailModel)model)).Html)
        .RegisterEmailTemplate(
            EmailType.ForgotPassword,
            "Resetowanie hasła",
            async model => _renderer.Render(await ForgotPasswordEmailTemplate.RenderAsync((ForgotPasswordEmailModel)model)).Html);

        if (hostEnvironment.IsStaging() || hostEnvironment.IsProduction())
        {
            return emailEngine
                .AddGridEmailService<EmailType>(configuration);
        }

        return emailEngine
            .AddEmailService<EmailType>(configuration);
    }
}
