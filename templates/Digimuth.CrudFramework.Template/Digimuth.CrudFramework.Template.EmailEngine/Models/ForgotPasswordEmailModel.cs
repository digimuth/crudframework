﻿using Digimuth.CrudFramework.EmailEngine.Models;

namespace Digimuth.CrudFramework.Template.EmailEngine.Models
{
    public class ForgotPasswordEmailModel : IEmailModel
    {
        public string Email { get; set; }

        public string Token { get; set; }

        public string UrlTemplate { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}
