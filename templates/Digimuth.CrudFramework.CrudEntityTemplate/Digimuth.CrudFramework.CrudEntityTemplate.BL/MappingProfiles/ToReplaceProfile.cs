using AutoMapper;
using Digimuth.CrudFramework.CrudEntityTemplate.BL.Dtos.ToReplace;
using Digimuth.CrudFramework.CrudEntityTemplate.Storage.Models;

namespace Digimuth.CrudFramework.CrudEntityTemplate.BL.MappingProfiles;

public class ToReplaceProfile : Profile
{
    public ToReplaceProfile()
    {
        CreateMap<ToReplace, ToReplaceDto>().ReverseMap();
        CreateMap<ToReplace, ToReplaceDetailsDto>().IncludeBase<ToReplace, ToReplaceDto>().ReverseMap();
        CreateMap<ToReplace, ToReplaceCreateDto>().IncludeBase<ToReplace, ToReplaceDto>().ReverseMap();
        CreateMap<ToReplace, ToReplaceUpdateDto>().IncludeBase<ToReplace, ToReplaceDto>().ReverseMap();
    }
}