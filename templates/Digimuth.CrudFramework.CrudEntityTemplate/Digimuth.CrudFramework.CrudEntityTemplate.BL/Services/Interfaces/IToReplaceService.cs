using Digimuth.CrudFramework.BL.Services.Interfaces;
using Digimuth.CrudFramework.CrudEntityTemplate.BL.Dtos.ToReplace;

namespace Digimuth.CrudFramework.CrudEntityTemplate.BL.Services.Interfaces;

public interface IToReplaceService
    : ICrudService<ToReplaceDto, ToReplaceDetailsDto, ToReplaceCreateDto, ToReplaceUpdateDto>
{
    
}