using System.Threading.Tasks;
using Digimuth.CrudFramework.BL.Services;
using Digimuth.CrudFramework.BL.Services.Interfaces;
using Digimuth.CrudFramework.CrudEntityTemplate.BL.Dtos.ToReplace;
using Digimuth.CrudFramework.CrudEntityTemplate.BL.Services.Interfaces;
using Digimuth.CrudFramework.CrudEntityTemplate.Storage.Models;
using Microsoft.AspNetCore.Http;

namespace Digimuth.CrudFramework.CrudEntityTemplate.BL.Services;

public class ToReplaceService
    : CrudService<ToReplace, ToReplaceDto, ToReplaceDetailsDto, ToReplaceCreateDto, ToReplaceUpdateDto>,
    IToReplaceService
{
    public ToReplaceService(IRepository<ToReplace> repository, IHttpContextAccessor httpContextAccessor)
        : base(repository, httpContextAccessor)
    {
        #warning implement access rules
        AuthenticationReadingPredicate = _ => true;
    }

    public override Task<bool> IsAuthorizedForReadAllAsync()
    {
        return base.IsAuthorizedForReadAllAsync();
    }

    public override Task<bool> IsAuthorizedForReadAsync(long id)
    {
        return base.IsAuthorizedForReadAsync(id);
    }

    public override Task<bool> IsAuthorizedForCreateAsync(ToReplaceCreateDto entity)
    {
        return base.IsAuthorizedForCreateAsync(entity);
    }

    public override Task<bool> IsAuthorizedForUpdateAsync(long id, ToReplaceUpdateDto entity)
    {
        return base.IsAuthorizedForUpdateAsync(id, entity);
    }

    public override Task<bool> IsAuthorizedForDeleteAsync(long id)
    {
        return base.IsAuthorizedForDeleteAsync(id);
    }
}