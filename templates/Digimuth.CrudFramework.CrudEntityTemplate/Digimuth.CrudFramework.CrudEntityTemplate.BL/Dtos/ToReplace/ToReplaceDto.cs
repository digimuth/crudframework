using Digimuth.CrudFramework.BL.Dtos;

namespace Digimuth.CrudFramework.CrudEntityTemplate.BL.Dtos.ToReplace;

#if (withSlug)
public class ToReplaceDto : BaseDtoWithSlug
#else
public class ToReplaceDto : BaseDto
#endif
{
#if (withSlug)
    public ToReplaceDto()
    {
        #error Update slug prefix
        SlugPrefix = "";
    }
#endif
}