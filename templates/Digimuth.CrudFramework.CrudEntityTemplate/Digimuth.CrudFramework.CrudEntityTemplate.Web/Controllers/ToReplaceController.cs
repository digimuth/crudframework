using Digimuth.CrudFramework.Web.Controllers;
using Digimuth.CrudFramework.CrudEntityTemplate.BL.Dtos.ToReplace;
using Digimuth.CrudFramework.CrudEntityTemplate.BL.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Digimuth.CrudFramework.CrudEntityTemplate.Web.Controllers;

[ApiController]
[Route("api/[controller]")]
public class ToReplaceController
    : CrudControllerBase<ToReplaceDto, ToReplaceDetailsDto, ToReplaceCreateDto, ToReplaceUpdateDto>
{
    public ToReplaceController(IToReplaceService crudService, ILogger<ToReplaceController> logger)
        : base(crudService, logger)
    {
    }
}