import { useInjection } from "inversify-react";
import { useState } from "react";
import ApiService from "../../services/apiService";
import { ToReplaceApi, ToReplaceCreateDto, ToReplaceDto, ToReplaceUpdateDto } from "../../api";
import ToReplacesList from "./LowercaseNameList";
import ToReplaceModalFormAddContainer from "./LowercaseNameModalFormAddContainer/LowercaseNameModalFormAddContainer";
import { Layout } from "antd";
import { Content } from "antd/lib/layout/layout";
import styles from "./LowercaseNameListContainer.module.scss";

export default function ToReplacesListContainer() {
    const apiService = useInjection(ApiService);
    const [LowercaseNames, setToReplaces] = useState<ToReplaceDto[] | null>();
    const [isAddFormVisible, setAddFormVisible] = useState(false);
    const [LowercaseNameModalFormContainerTitle, setToReplaceModalFormContainerTitle] = useState<string>();
    const [LowercaseNameToEdit, setToReplaceToEdit] = useState<ToReplaceUpdateDto | null>(null);
    const [idToReplaceToEdit, setIdToReplaceToEdit] = useState<number | null>(null);
    const [isLoading, setIsLoading] = useState<boolean>(false);

    const refreshToReplacesList = async () => {
        setIsLoading(true);

        try {
            const ToReplaces = await apiService.getApi(ToReplaceApi).apiToReplaceGet();
            setToReplaces(ToReplaces.data);
        } catch (e) {
            console.log(e.response.data);
        }

        setIsLoading(false);
    }

    const onAddClicked = () => {
        setToReplaceModalFormContainerTitle("Add new ToReplace");
        setToReplaceToEdit(null);
        setIdToReplaceToEdit(null);
        setAddFormVisible(true);
    }

    const onEditClicked = async (id: number) => {
        setToReplaceModalFormContainerTitle(`Edit ToReplace with id: ${id}`);
        setIsLoading(true);

        const response = await apiService.getApi(ToReplaceApi).apiToReplaceIdGet(id);

        setToReplaceToEdit(response.data as ToReplaceUpdateDto);
        setIdToReplaceToEdit(id);
        setAddFormVisible(true);
        setIsLoading(false);
    }

    const onDeleteClicked = async (id: number) => {
        const response = await apiService.getApi(ToReplaceApi).apiToReplaceIdDelete(id)

        if (response.request.status !== 200) {
            console.log("Error occured while deleting data"); //add some warning?
        } else {
            refreshToReplacesList();
        }
    }

    const onSubmit = async (values: ToReplaceCreateDto | ToReplaceUpdateDto) => {
        try {
            if (LowercaseNameToEdit === null) {
                await apiService.getApi(ToReplaceApi).apiToReplacePost(values);
            } else if (idToReplaceToEdit !== null) {
                await apiService.getApi(ToReplaceApi).apiToReplaceIdPut(idToReplaceToEdit, values)
            }
        } catch (e) {
            return e.response.data;
        }

        setAddFormVisible(false)
        refreshToReplacesList();
    }

    return <>
        <Layout style={{ backgroundColor: "white" }}>
            <Content>
                <Layout>
                    <Content className={styles.content}>
                        <ToReplacesList
                            data={LowercaseNames || null}
                            onAddClicked={onAddClicked}
                            onEditClicked={onEditClicked}
                            onDeleteClicked={onDeleteClicked}
                        />
                        <ToReplaceModalFormAddContainer
                            onFinish={() => setAddFormVisible(false)}
                            onSubmit={onSubmit}
                            addFormVisibility={isAddFormVisible}
                            title={LowercaseNameModalFormContainerTitle || ""}
                            ToReplaceToEdit={LowercaseNameToEdit}
                        />
                    </Content>
                </Layout>
            </Content>
        </Layout>
    </>
}