import { ColumnsType } from "antd/lib/table";
import { ToReplaceDto } from "../../api";
import { CrudTableWrapperProps } from "digimuth-components/dist/components/crudTable/crudTable";
import { CrudTable, nameof } from "digimuth-components/dist";

export default function ToReplacesList(props: CrudTableWrapperProps<ToReplaceDto>) {
    const columns: ColumnsType<ToReplaceDto> = [
        // {
        //     key: nameof<ToReplaceDto>("key"),
        //     dataIndex: nameof<ToReplaceDto>("key"),
        //     title: "Key"
        // },
    ];

    return <>
        <CrudTable
            data={props.data}
            onAddClicked={props.onAddClicked}
            onEditClicked={props.onEditClicked}
            onDeleteClicked={props.onDeleteClicked}
            isLoading={props.isLoading}
            columns={columns}
            addButtonTitle={"+ Add new ToReplace"}
        />
    </>
}
