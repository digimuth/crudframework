import { ToReplaceCreateDto, ToReplaceUpdateDto } from "../../../api";
import { CrudFormItemProps, CrudFormModal, nameof } from "digimuth-components/dist";

const formItems: CrudFormItemProps[] = [
    // {
    //     name: nameof<ToReplaceCreateDto>("key"),
    //     rules: [
    //         {required: true, message: 'This field is required'}
    //     ],
    // },
];

export default function ToReplaceModalForm(props: ToReplaceModalProps) {
    return <CrudFormModal
        formItems={formItems}
        onSubmit={props.onSubmit}
        onModalCancelClicked={() => props.setModalVisibility(false)}
        initialValues={props.initialValues}
        modalOkText={props.saveButtonTitle}
        modalTitle={props.modalTitle}
        isModalVisible={props.isModalVisible}
    />;
}

interface ToReplaceModalProps {
    initialValues?: ToReplaceCreateDto | ToReplaceUpdateDto,
    isModalVisible: boolean;
    setModalVisibility: (isVisible: boolean) => any;
    onSubmit: (value: ToReplaceCreateDto | ToReplaceUpdateDto) => Promise<any>,
    modalTitle: string,
    saveButtonTitle: string,
}