import { ToReplaceCreateDto, ToReplaceUpdateDto } from "../../../api";
import ToReplaceModalForm from "../LowercaseNameModalForm/LowercaseNameModalForm";

export default function ToReplaceModalFormAddContainer(props: ToReplaceModalFormAddContainerProps) {
    return <>
        <ToReplaceModalForm
            isModalVisible={props.addFormVisibility}
            setModalVisibility={props.onFinish}
            onSubmit={props.onSubmit}
            modalTitle={props.title}
            saveButtonTitle="Submit"
            initialValues={props.ToReplaceToEdit!}
        />
    </>
}

interface ToReplaceModalFormAddContainerProps {
    onFinish: (refreshList: boolean) => void,
    onSubmit: (values: ToReplaceCreateDto | ToReplaceUpdateDto) => Promise<any>,
    addFormVisibility: boolean,
    title: string,
    ToReplaceToEdit: ToReplaceUpdateDto | null
}
