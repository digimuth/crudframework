using Digimuth.CrudFramework.Storage.Models;

namespace Digimuth.CrudFramework.CrudEntityTemplate.Storage.Models;

#if (withSlug)
public class ToReplace : EntityWithSlugBase
#else
public class ToReplace : EntityBase
#endif
{
    
}