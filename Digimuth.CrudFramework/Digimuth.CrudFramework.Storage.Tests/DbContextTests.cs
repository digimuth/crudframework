using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Digimuth.CrudFramework.Common.Models;
using Digimuth.CrudFramework.Storage.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using NUnit.Framework;

namespace Digimuth.CrudFramework.Storage.Tests;

public class DbContextTests
{
    private InMemoryDbContext _dbContext;
    private Book _book1;
    private Book _book2;
    private Book _book3;

    [SetUp]
    public void Setup()
    {
        var options = new DbContextOptions<DbContext>();
        _dbContext = new InMemoryDbContext(options,
            null);
        _book1 = new Book { Id = 1, Title = "Bible" };
        _book2 = new Book { Id = 2, Title = "The Witcher" };
        _book3 = new Book { Id = 3, Title = "Sherlock Holmes" };
    }

    [Test]
    public async Task ShouldMapPrimaryKeyCollectionToOneToManyNavigationProperty()
    {
        await _dbContext.Books.AddRangeAsync(new[]
        {
            _book1,
            _book2,
            _book3
        });

        await _dbContext.SaveChangesAsync();
        _dbContext.ChangeTracker.Clear();

        var library = new Library
        {
            BooksIds = new[] { 1L, 3 }
        };

        _dbContext.Libraries.Add(library);

        Assert.IsNull(library.Books);

        await _dbContext.SaveChangesAsync();

        await _dbContext.Entry(library).ReloadAsync();

        Assert.IsTrue(library.Books.Any(b => b.Id == _book1.Id));
        Assert.IsTrue(library.Books.Any(b => b.Id == _book3.Id));
        Assert.AreEqual(2, library.Books.Count);
        // TODO: switch InMemoryDbContext to SQLite based in memory for proper FK update test
        // Assert.IsNotNull(library.Books.First().Title);
    }

    [Test]
    public async Task ShouldMapPrimaryKeyCollectionToOneToManyNavigationPropertyWithDirtyContext()
    {
        await _dbContext.Books.AddRangeAsync(new[]
        {
            _book1,
            _book2,
            _book3
        });

        await _dbContext.SaveChangesAsync();
        _dbContext.ChangeTracker.Clear();

        var book = _dbContext.Books.First();

        var library = new Library
        {
            BooksIds = new[] { 1L, 3 }
        };

        _dbContext.Libraries.Add(library);

        Assert.IsNull(library.Books);

        await _dbContext.SaveChangesAsync();

        await _dbContext.Entry(library).ReloadAsync();

        Assert.IsTrue(library.Books.Any(b => b.Id == _book1.Id));
        Assert.IsTrue(library.Books.Any(b => b.Id == _book3.Id));
        Assert.AreEqual(2, library.Books.Count);
        // TODO: switch InMemoryDbContext to SQLite based in memory for proper FK update test
        // Assert.IsNotNull(library.Books.First().Title);
    }

    [Test]
    public async Task ShouldNotChangeRelationWhenNullProvidedInsteadOfIdCollection()
    {
        var library = new Library
        {
            Books = new[] { _book1, _book3 },
            BooksIds = null
        };

        _dbContext.Libraries.Add(library);

        await _dbContext.SaveChangesAsync();

        await _dbContext.Entry(library).ReloadAsync();

        Assert.IsTrue(library.Books.Any(b => b.Id == _book1.Id));
        Assert.IsTrue(library.Books.Any(b => b.Id == _book3.Id));
        Assert.AreEqual(2, library.Books.Count);
        // Assert.IsNotNull(library.Books.First().Title);
    }

    private class Library : EntityBase
    {
        public ICollection<Book> Books { get; set; }

        [NotMapped]
        public ICollection<long> BooksIds { get; set; }
    }

    private class Book : EntityBase
    {
        public string Title { get; set; }

        public Library Library { get; set; }
    }

    private class InMemoryDbContext : CatalogDbContextBase
    {
        public DbSet<Book> Books { get; set; }

        public DbSet<Library> Libraries { get; set; }

        public InMemoryDbContext(
            DbContextOptions options,
            IConfiguration configuration)
            : base(options, configuration)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseInMemoryDatabase(Guid.NewGuid().ToString()).EnableSensitiveDataLogging();
        }

        public override async Task<int> SaveChangesAsync(
            bool acceptAllChangesOnSuccess,
            CancellationToken cancellationToken = new())
        {
            await MapIdNavigationCollectionsToForeignKeysAsync();

            return await base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }
    }
}