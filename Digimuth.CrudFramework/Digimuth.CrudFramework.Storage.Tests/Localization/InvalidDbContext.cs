﻿using Digimuth.CrudFramework.Storage.Tests.Localization.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Digimuth.CrudFramework.Storage.Tests.Localization
{
    public class InvalidDbContext : SqliteInMemoryDbContext
    {
        public DbSet<InvalidMovieModel> InvalidMovies { get; set; }

        public InvalidDbContext(DbContextOptions options, IConfiguration configuration)
        : base(options, configuration)
        {
        }

    }
}
