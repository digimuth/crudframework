using AutoMapper;
using Digimuth.CrudFramework.BL.Extensions;
using Digimuth.CrudFramework.Common.Services;
using Digimuth.CrudFramework.Common.Services.Interfaces;
using Digimuth.CrudFramework.Storage.Models;
using Digimuth.CrudFramework.Storage.Tests.Localization.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Digimuth.CrudFramework.Storage.Tests.Localization;

[TestFixture]
public class LocalizationTests
{
    private ServiceProvider _provider;
    private SqliteInMemoryDbContext _dbContext;

    private class TestCurrentLanguageService : CurrentLanguageService
    {
        public TestCurrentLanguageService(IHttpContextAccessor httpContextAccessor) : base(httpContextAccessor)
        {
        }

        public override string GetFallbackLanguage() => "PL";
    }

    [SetUp]
    public void SetUp()
    {
        var services = new ServiceCollection();
        var configuration = new ConfigurationBuilder().Build();

        services.AddSingleton<IConfiguration>(configuration);
        services.AddSingleton<ICurrentLanguageService, CurrentLanguageService>();

        var httpContext = new DefaultHttpContext();
        httpContext.Request.Headers["Language"] = "EN";

        var mockHttpContextAccessor = new Mock<IHttpContextAccessor>();
        mockHttpContextAccessor.Setup(_ => _.HttpContext).Returns(httpContext);

        services.AddSingleton(mockHttpContextAccessor.Object);

        services.InitAutomapper(typeof(LocalizationTests));

        services.AddDbContext<SqliteInMemoryDbContext>();

        _provider = services.BuildServiceProvider();

        _dbContext = _provider.GetService<SqliteInMemoryDbContext>();
        _dbContext.Database.EnsureDeleted();
        _dbContext.Database.EnsureCreated();
    }

    [Test]
    [TestCase("PL")]
    [TestCase("EN")]
    public void ShouldGetLanguageFromHeader(string languageInHeader)
    {
        var httpContext = new DefaultHttpContext();
        httpContext.Request.Headers["Language"] = languageInHeader;

        var mockHttpContextAccessor = new Mock<IHttpContextAccessor>();
        mockHttpContextAccessor.Setup(_ => _.HttpContext).Returns(httpContext);

        var currentLanguageService = new CurrentLanguageService(mockHttpContextAccessor.Object);

        var language = currentLanguageService.GetCurrentLanguage();

        Assert.AreEqual(languageInHeader, language);
    }

    // test here just for reference - compiled query is not supported by SQLite
    // [Test]
    // [TestCase(true, false)]
    // [TestCase(false, false)]
    public async Task ShouldMapFilteredLocalizableDtos(bool isLanguageGiven, bool expectsResult)
    {
        var moviesToAdd = new[]
        {
            new Movie
            {
                ProductionCompany = "Warner bros",
                Rating = 99,
                Title = new[]
                {
                    new LocalizableString
                    {
                        Locale = "PL",
                        Value = "Harry Potter i Komnata Tajemnic"
                    },
                    new LocalizableString
                    {
                        Locale = "EN",
                        Value = "Harry Potter And The Chamber of Secrets"
                    }
                },
                Description = new[]
                {
                    new LocalizableString
                    {
                        Locale = "PL",
                        Value = "Fajny film"
                    },
                    new LocalizableString
                    {
                        Locale = "EN",
                        Value = "Cool movie"
                    }
                },
                LocalizableNumber = new[]
                {
                    new LocalizableEntity<long>
                    {
                        Locale = "PL",
                        Value = 2
                    },
                    new LocalizableEntity<long>
                    {
                        Locale = "EN",
                        Value = 5
                    }
                }
            }
        };

        await _dbContext.AddRangeAsync(moviesToAdd);

        await _dbContext.SaveChangesAsync();

        _dbContext.ChangeTracker.Clear();

        var mapper = _provider.GetRequiredService<IMapper>();

        var languageService = _provider.GetRequiredService<ICurrentLanguageService>();

        bool testResult;

        if (isLanguageGiven) //only english
        {
            var result = await mapper.ProjectTo<MovieDto>(_dbContext.Movies.AsNoTracking(), new { language = languageService.GetCurrentLanguage() }).ToListAsync();
            testResult = result.Any(m => m.Description.Any(d => d.Locale != "EN") || m.Title.Any(t => t.Locale != "EN") || m.LocalizableNumber.Any(t => t.Locale != "EN"));
        }
        else
        {
            var result = await mapper.ProjectTo<MovieDto>(_dbContext.Movies.AsNoTracking()).ToListAsync();
            testResult = result.Any(t => t.Title.Count() != 2 || t.Description.Count() != 2 || t.LocalizableNumber.Count() != 2);
        }

        Assert.AreEqual(expectsResult, testResult);
    }

    // test here just for reference - compiled query is not supported by SQLite
    // [Test]
    public async Task ShouldFallbackToSelectedLanguageIfNoTranslationExists()
    {
        var moviesToAdd = new[]
        {
            new Movie
            {
                ProductionCompany = "Warner bros",
                Rating = 99,
                Title = new[]
                {
                    new LocalizableString
                    {
                        Locale = "PL",
                        Value = "Harry Potter i Komnata Tajemnic"
                    },
                    new LocalizableString
                    {
                        Locale = "EN",
                        Value = "Harry Potter And The Chamber of Secrets"
                    }
                },
                Description = new[]
                {
                    new LocalizableString
                    {
                        Locale = "PL",
                        Value = "Fajny film"
                    }
                },
                LocalizableNumber = new[]
                {
                    new LocalizableEntity<long>
                    {
                        Locale = "PL",
                        Value = 2
                    },
                    new LocalizableEntity<long>
                    {
                        Locale = "EN",
                        Value = 5
                    }
                }
            }
        };

        await _dbContext.AddRangeAsync(moviesToAdd);

        await _dbContext.SaveChangesAsync();

        _dbContext.ChangeTracker.Clear();

        var mapper = _provider.GetRequiredService<IMapper>();

        var languageService = _provider.GetRequiredService<ICurrentLanguageService>();

        var result = await mapper.ProjectTo<MovieDto>(_dbContext.Movies.AsNoTracking(), new { language = languageService.GetCurrentLanguage() }).ToListAsync();
        var testResult = result.Any(m => m.Description.Any(d => d.Locale != "PL") || m.Title.Any(t => t.Locale != "EN") || m.LocalizableNumber.Any(t => t.Locale != "EN"));

        Assert.True(testResult);
    }

    [Test]
    public async Task ShouldAddAndReturnLocalizedStrings()
    {
        var moviesToAdd = new[]
        {
            new Movie
            {
                ProductionCompany = "Warner bros",
                Rating = 99,
                Title = new[]
                {
                    new LocalizableString
                    {
                        Locale = "PL",
                        Value = "Harry Potter i Komnata Tajemnic"
                    },
                    new LocalizableString
                    {
                        Locale = "EN",
                        Value = "Harry Potter And The Chamber of Secrets"
                    }
                },
                Description = new[]
                {
                    new LocalizableString
                    {
                        Locale = "PL",
                        Value = "Fajny film"
                    },
                    new LocalizableString
                    {
                        Locale = "EN",
                        Value = "Cool movie"
                    }
                }
            }
        };

        await _dbContext.AddRangeAsync(moviesToAdd);

        await _dbContext.SaveChangesAsync();

        await _dbContext.Movies.LoadAsync();

        var movies = _dbContext.Movies.Local;

        Assert.IsNotEmpty(movies);
        CollectionAssert.AreEquivalent(movies.First().Title, new[]
        {
            new LocalizableString
            {
                Locale = "PL",
                Value = "Harry Potter i Komnata Tajemnic"
            },
            new LocalizableString
            {
                Locale = "EN",
                Value = "Harry Potter And The Chamber of Secrets"
            }
        });

        CollectionAssert.AreEquivalent(movies.First().Description, new[]
        {
            new LocalizableString
            {
                Locale = "PL",
                Value = "Fajny film"
            },
            new LocalizableString
            {
                Locale = "EN",
                Value = "Cool movie"
            }
        });
    }

    [Test]
    public async Task ShouldReplaceLocalizedStrings()
    {
        var moviesToAdd = new[]
        {
            new Movie
            {
                ProductionCompany = "Warner bros",
                Rating = 99,
                Title = new[]
                {
                    new LocalizableString
                    {
                        Locale = "PL",
                        Value = "Harry Potter i Komnata Tajemnic"
                    },
                    new LocalizableString
                    {
                        Locale = "EN",
                        Value = "Harry Potter And The Chamber of Secrets"
                    }
                }
            }
        };

        await _dbContext.AddRangeAsync(moviesToAdd);

        await _dbContext.SaveChangesAsync();

        _dbContext.ChangeTracker.Clear();

        var movie = await _dbContext.Movies.FirstAsync();

        movie.Title = new List<LocalizableString>
        {
            new()
            {
                Locale = "PL",
                Value = "Harry Potter i Komnata Tajemnic"
            },
            new()
            {
                Locale = "HU",
                Value = "Harry Potter és a titkok kamrája"
            }
        };

        await _dbContext.SaveChangesAsync();
        _dbContext.ChangeTracker.Clear();

        movie = await _dbContext.Movies.FirstAsync();

        CollectionAssert.AreEquivalent(movie.Title, new List<LocalizableString>
        {
            new()
            {
                Locale = "PL",
                Value = "Harry Potter i Komnata Tajemnic"
            },
            new()
            {
                Locale = "HU",
                Value = "Harry Potter és a titkok kamrája"
            }
        });
    }

    [Test]
    public async Task ShouldThrowExceptionOnDuplicatedLocales()
    {
        var moviesToAdd = new[]
        {
            new Movie
            {
                ProductionCompany = "Warner bros",
                Rating = 99,
                Title = new[]
                {
                    new LocalizableString
                    {
                        Locale = "EN",
                        Value = "Harry Potter And The Chamber"
                    },
                    new LocalizableString
                    {
                        Locale = "EN",
                        Value = "Harry Potter And The Chamber of Secrets"
                    }
                }
            }
        };

        var action = new Func<Task<int>>(async () =>
        {
            await _dbContext.AddRangeAsync(moviesToAdd);

            return await _dbContext.SaveChangesAsync();
        });

        Assert.ThrowsAsync<InvalidOperationException>(async () => await action());
    }

    [Test]
    public void DbContextInitializationShouldFailOnInvalidModel()
    {
        var services = new ServiceCollection();
        var configuration = new ConfigurationBuilder().Build();

        services.AddSingleton<IConfiguration>(configuration);
      
        var connection = new SqliteConnection("DataSource=:memory:");
        connection.Open(); 

        services.AddDbContext<InvalidDbContext>(options =>
            options.UseSqlite(connection)); 

        var provider = services.BuildServiceProvider();

        var dbContext = provider.GetService<InvalidDbContext>();

    
        Assert.Throws<InvalidOperationException>(() =>
        {
            dbContext.Database.EnsureDeleted();
            dbContext.Database.EnsureCreated();
        });

        connection.Close();
        connection.Dispose();
    }

    [TearDown]
    public async Task TearDown()
    {
        await _provider.DisposeAsync();
    }
}