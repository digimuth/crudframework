using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;
using Digimuth.CrudFramework.Storage.Tests.Localization.Models;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using NUnit.Framework;

namespace Digimuth.CrudFramework.Storage.Tests.Localization;

public class SqliteInMemoryDbContext : CatalogDbContextBase
{
    private readonly DbConnection _connection = new SqliteConnection("Filename=:memory:");

    public DbSet<Movie> Movies { get; set; }

    public SqliteInMemoryDbContext(DbContextOptions options, IConfiguration configuration)
        : base(options, configuration)
    {
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        _connection.Open();
        optionsBuilder.UseSqlite(_connection)
            .LogTo(TestContext.WriteLine);
    }

    public override async Task<int> SaveChangesAsync(
        bool acceptAllChangesOnSuccess,
        CancellationToken cancellationToken = new())
    {
        await MapIdNavigationCollectionsToForeignKeysAsync();

        return await base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
    }
}