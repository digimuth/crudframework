﻿using Digimuth.CrudFramework.BL.Dtos;
using System.Collections.Generic;

namespace Digimuth.CrudFramework.Storage.Tests.Localization.Models;

public class MovieDto : BaseDto
{
    public IEnumerable<LocalizableStringDto> Title { get; set; }

    public IEnumerable<LocalizableStringDto> Description { get; set; }

    public IEnumerable<LocalizableEntityDto<long>> LocalizableNumber { get; set; }

    public int Rating { get; set; }

    public string ProductionCompany { get; set; }
}