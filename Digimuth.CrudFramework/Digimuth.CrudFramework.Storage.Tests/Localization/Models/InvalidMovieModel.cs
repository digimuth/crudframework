﻿
using Digimuth.CrudFramework.Storage.Models;
using System.Collections.Generic;

namespace Digimuth.CrudFramework.Storage.Tests.Localization.Models
{

    public class InvalidMovieModel : EntityBase
    {
        public ICollection<LocalizableString> Title { get; set; }

        public ICollection<LocalizableString> Description { get; set; }

        public LocalizableEntity<long> InvalidLocalizableLong { get; set; }

        public LocalizableString InvalidLocalizableString { get; set; }

        public int Rating { get; set; }

        public string ProductionCompany { get; set; }
    }
}
