using System.Collections.Generic;
using Digimuth.CrudFramework.Storage.Models;

namespace Digimuth.CrudFramework.Storage.Tests.Localization.Models;

public class Movie : EntityBase
{
    public ICollection<LocalizableString> Title { get; set; }

    public ICollection<LocalizableString> Description { get; set; }

    public ICollection<LocalizableEntity<long>> LocalizableNumber { get; set; }

    public int Rating { get; set; }

    public string ProductionCompany { get; set; }
}