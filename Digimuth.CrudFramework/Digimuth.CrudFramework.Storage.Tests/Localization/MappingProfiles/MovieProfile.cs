﻿using AutoMapper;
using Digimuth.CrudFramework.Storage.Tests.Localization.Models;

namespace Digimuth.CrudFramework.Storage.Tests.Localization.MappingProfiles;

public class MovieProfile : Profile
{
    public MovieProfile()
    {
        CreateMap<Movie, MovieDto>().ReverseMap();
    }
}