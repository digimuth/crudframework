using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using ClosedXML.Excel;
using Digimuth.CrudFramework.Documents.Models;
using Digimuth.CrudFramework.Documents.Services.Interfaces;
using Digimuth.CrudFramework.Documents.Extensions;
using DocumentFormat.OpenXml.Packaging;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;

namespace Digimuth.CrudFramework.Documents.Tests.Services;

public class DocumentServiceTests
{
    private IDocumentService<DocumentType> _service;

    [OneTimeSetUp]
    public void OneTimeSetup()
    {
        var services = new ServiceCollection();

        services.RegisterDocumentTemplate<DocumentType, LetterModel>(
            DocumentType.Letter,
            "Digimuth.CrudFramework.Documents.Tests.Templates.LetterTestTemplate.docx",
            Assembly.GetAssembly(typeof(DocumentServiceTests)));

        services.RegisterDocumentTemplate<DocumentType, SpreadsheetModel>(
            DocumentType.Spreadsheet,
            "Digimuth.CrudFramework.Documents.Tests.Templates.SpreadsheetTestTemplate.xlsx",
            Assembly.GetAssembly(typeof(DocumentServiceTests)));

        services.AddDocumentService<DocumentType>();

        using var serviceProvider = services.BuildServiceProvider();

        _service = serviceProvider.GetRequiredService<IDocumentService<DocumentType>>();
    }

    [Test]
    public async Task ShouldReplaceTokensInWordDocument()
    {
        var model = new LetterModel
        {
            FirstName = "Ben",
            LastName = "Dover",
            PhoneNumber = "123234345",
            CompanyName = "Company Co."
        };

        await using var resultStream = await _service.GenerateDocumentAsync(DocumentType.Letter, model);

        using var file = WordprocessingDocument.Open(resultStream, true);
        var document = file.MainDocumentPart.Document;
        var resultText = document.InnerText;

        Assert.AreEqual(
            "Hello Ben,This is a template document for Ben Dover. Please provide your 123234345 so we can proceed.Greetings,Company Co.",
            resultText);
    }

    [Test]
    public async Task ShouldReplaceTokensInExcelDocument()
    {
        var model = new SpreadsheetModel
        {
            Date = "2021-03-22",
            Name = "Sal Hand",
            Title = "The best spreadsheet ever",
            Orders = new List<SpreadsheetOrder>
            {
                new SpreadsheetOrder
                {
                    Number = "001",
                    To = "FlatApp Barek Lancuch",
                    Value = 123.22m
                },
                new SpreadsheetOrder
                {
                    Number = "002",
                    To = "Filip Witold Borkowski",
                    Value = 997.1m
                }
            }
        };

        await using var resultStream = await _service.GenerateDocumentAsync(DocumentType.Spreadsheet, model);

        using var file = new XLWorkbook(resultStream);
        var worksheet = file.Worksheets.First();

        Assert.AreEqual(
            "2021-03-22",
            worksheet.Cell("B1").GetValue<string>());

        Assert.AreEqual(
            "Sal Hand",
            worksheet.Cell("B2").GetValue<string>());

        Assert.AreEqual(
            "The best spreadsheet ever",
            worksheet.Cell("A4").GetValue<string>());

        Assert.AreEqual(
            "001",
            worksheet.Cell("A7").GetValue<string>());

        Assert.AreEqual(
            "FlatApp Barek Lancuch",
            worksheet.Cell("B7").GetValue<string>());

        Assert.AreEqual(
            123.22,
            worksheet.Cell("C7").GetValue<decimal>());

        Assert.AreEqual(
            "002",
            worksheet.Cell("A8").GetValue<string>());

        Assert.AreEqual(
            "Filip Witold Borkowski",
            worksheet.Cell("B8").GetValue<string>());

        Assert.AreEqual(
            997.1,
            worksheet.Cell("C8").GetValue<decimal>());

        Assert.AreEqual(
            123.22 + 997.1,
            worksheet.Cell("C9").GetValue<decimal>());
    }

    [Test]
    public void ShouldThrowExceptionOnNotRegisteredDocumentTemplate()
    {
        Assert.ThrowsAsync<ArgumentException>(() =>
            _service.GenerateDocumentAsync(DocumentType.KpiReport, new SpreadsheetModel()));
    }

    [Test]
    public void ShouldThrowExceptionOnImproperModelForTemplate()
    {
        Assert.ThrowsAsync<ArgumentException>(() =>
            _service.GenerateDocumentAsync(DocumentType.Letter, new SpreadsheetModel()));
    }
}

internal enum DocumentType
{
    Letter,
    Spreadsheet,
    KpiReport
}

internal class LetterModel : IDocumentModel
{
    public string FirstName { get; set; }

    public string LastName { get; set; }

    public string PhoneNumber { get; set; }

    public string CompanyName { get; set; }
}

internal class SpreadsheetModel : IDocumentModel
{
    public string Date { get; set; }

    public string Name { get; set; }

    public string Title { get; set; }

    // for tables in Excel reports refer to https://closedxml.github.io/ClosedXML.Report/docs/en/Flat-tables
    // make sure to set the table name to property name (here: "Orders")
    // by using Formulas -> Define Name in MS Excel
    public IEnumerable<SpreadsheetOrder> Orders { get; set; }
}

internal class SpreadsheetOrder
{
    public string Number { get; set; }

    public string To { get; set; }

    public decimal Value { get; set; }
}