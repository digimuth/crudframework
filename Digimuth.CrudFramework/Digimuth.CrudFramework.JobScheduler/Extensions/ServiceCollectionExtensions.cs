using Digimuth.CrudFramework.JobScheduler.Models;
using Digimuth.CrudFramework.JobScheduler.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Digimuth.CrudFramework.JobScheduler.Extensions;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddJobScheduler(this IServiceCollection services)
    {
        return services
            .AddHostedService<JobSchedulerService>();
    }

    public static IServiceCollection RegisterJob<TJob>(this IServiceCollection services)
        where TJob : class, IJob
    {
        return services
            .AddTransient<TJob>()
            .AddTransient<IJob, TJob>();
    }

    public static IServiceCollection AddDynamicJobScheduler(this IServiceCollection services)
    {
        services.AddSingleton<DynamicJobSchedulerService>();
        services.AddHostedService(s => s.GetService<DynamicJobSchedulerService>());

        return services;
    }

    public static IServiceCollection RegisterDynamicJob<TJob>(this IServiceCollection services)
        where TJob : class, IDynamicJob
    {
        return services.AddSingleton<IDynamicJob, TJob>();
    }
}