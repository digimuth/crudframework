using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Digimuth.CrudFramework.JobScheduler.Models;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Digimuth.CrudFramework.JobScheduler.Services;

public class JobSchedulerService : IHostedService
{
    private readonly IServiceScopeFactory _serviceScopeFactory;
    private readonly ILogger<JobSchedulerService> _logger;

    public JobSchedulerService(IServiceScopeFactory serviceScopeFactory, ILogger<JobSchedulerService> logger)
    {
        _serviceScopeFactory = serviceScopeFactory;
        _logger = logger;
    }

    public Task StartAsync(CancellationToken cancellationToken)
    {
        using var scope = _serviceScopeFactory.CreateScope();
        var jobs = scope.ServiceProvider.GetService<IEnumerable<IJob>>();

        foreach (var job in jobs)
        {
            ScheduleJob(job);
        }

        return Task.CompletedTask;
    }

    private void ScheduleJob(IJob job)
    {
        var now = job.UseLocalTime ? DateTimeOffset.Now : DateTimeOffset.UtcNow;
        var nextOccurrence = job.CronExpression.GetNextOccurrence(now, job.UseLocalTime ? TimeZoneInfo.Local : TimeZoneInfo.Utc);

        if (!nextOccurrence.HasValue)
        {
            return;
        }

        var jobType = job.GetType();

        _logger.LogInformation("Scheduling job: {FullName} for: {NextOccurrence}", jobType.FullName, nextOccurrence.Value);

        var delay = nextOccurrence.Value - now;

        Timer timer = null;

        timer = new Timer(s =>
        {
            ExecuteJob(s);
            timer.Dispose();
        }, jobType, delay, TimeSpan.FromMilliseconds(-1));
    }

    private async void ExecuteJob(object state)
    {
        var jobType = (Type)state;

        using var scope = _serviceScopeFactory.CreateScope();
        var job = scope.ServiceProvider.GetService(jobType) as IJob;

        try
        {
            _logger.LogInformation("Executing scheduled job: {FullName}", jobType.FullName);

            await job.ExecuteAsync();
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error during execution of scheduled job: {FullName}", jobType.FullName);
        }

        ScheduleJob(job);
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        return Task.CompletedTask;
    }
}