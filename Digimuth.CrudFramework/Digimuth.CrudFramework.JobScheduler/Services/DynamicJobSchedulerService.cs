﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Digimuth.CrudFramework.JobScheduler.Models;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Digimuth.CrudFramework.JobScheduler.Services;

public class DynamicJobSchedulerService : BackgroundService
{
    private readonly IServiceScopeFactory _serviceScopeFactory;
    private readonly ILogger<DynamicJobSchedulerService> _logger;
    private IEnumerable<IDynamicJob> _jobTypes;

    public DynamicJobSchedulerService(
        IServiceScopeFactory serviceScopeFactory,
        ILogger<DynamicJobSchedulerService> logger)
    {
        _serviceScopeFactory = serviceScopeFactory;
        _logger = logger;
    }

    protected override async Task ExecuteAsync(CancellationToken cancellationToken)
    {
        await Task.Delay(10000, cancellationToken);

        _logger.LogInformation("Starting DynamicJobSchedulerService");
        using var serviceScope = _serviceScopeFactory.CreateScope();
        var serviceProvider = serviceScope.ServiceProvider;
        _jobTypes = serviceProvider.GetServices<IDynamicJob>();

        foreach (var jobType in _jobTypes)
        {
            await jobType.RescheduleJobsAsync(await jobType.GetAllJobsAsync());
        }
    }

    public T GetDynamicJobByImplementedType<T>()
    {
        return (T) _jobTypes.First(j => j.GetType().IsAssignableTo(typeof(T)));
    }

    public override Task StopAsync(CancellationToken cancellationToken)
    {
        _logger.LogInformation("Stopping DynamicJobSchedulerService");

        return base.StopAsync(cancellationToken);
    }
}