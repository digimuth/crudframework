﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Cronos;
using Digimuth.CrudFramework.JobScheduler.Context;
using Microsoft.Extensions.Logging;

namespace Digimuth.CrudFramework.JobScheduler.Models;

public abstract class DynamicJobBase<T> : IDynamicJob where T : DynamicJobInstance
{
    private readonly ILogger<DynamicJobBase<T>> _logger;
    private readonly IDictionary<long, Timer> _timers;

    protected DynamicJobBase(ILogger<DynamicJobBase<T>> logger)
    {
        _logger = logger;
        _timers = new Dictionary<long, Timer>();
    }

    protected abstract Task ExecuteAsync(T data);
    public abstract Task<IEnumerable<DynamicJobInstance>> GetAllJobsAsync();

    /// <summary>
    /// Reschedules all jobs of type T defined in IDynamicJobDbContext DynamicJobs set.
    /// It will dispose provided scope after it's done
    /// </summary>
    public virtual async Task RescheduleJobsAsync(IEnumerable<DynamicJobInstance> dynamicJobs)
    {
        foreach (var id in _timers.Keys)
        {
            await _timers[id].DisposeAsync();
        }
        _timers.Clear();

        foreach (var dynamicJob in dynamicJobs)
        {
            ScheduleJob((T) dynamicJob);
        }
    }

    /// <summary>
    /// Reschedules a single job of type T defined in IDynamicJobDbContext DynamicJobs set.
    /// This job will keep rescheduling itself after update.
    /// It will dispose provided scope after it's done
    /// </summary>
    public virtual async Task RescheduleSingleJob(DynamicJobInstance dynamicJob)
    {
        if (_timers.TryGetValue(dynamicJob.Id, out var timer))
        {
            await timer.DisposeAsync();
            _timers.Remove(dynamicJob.Id);
            ScheduleJob((T) dynamicJob);
        }
    }

    /// <summary>
    /// Deletes a single job of type T defined in IDynamicJobDbContext DynamicJobs set.
    /// This job will be canceled and will not reschedule itself.
    /// It will dispose provided scope after it's done
    /// </summary>
    public virtual async Task DeleteSingleJob(DynamicJobInstance dynamicJob)
    {
        if (_timers.TryGetValue(dynamicJob.Id, out var timer))
        {
            await timer.DisposeAsync();
        }
    }

    private void ScheduleJob(T job)
    {
        var utcNow = DateTime.UtcNow;
        var nextOccurrence = CronExpression.Parse(job.CronExpression).GetNextOccurrence(utcNow);

        if (!nextOccurrence.HasValue)
        {
            return;
        }

        _logger.LogInformation("Scheduling job: {FullName} for: {NextOccurrence} (UTC)", typeof(T).FullName, nextOccurrence.Value);

        var delay = nextOccurrence.Value - utcNow;

        Timer timer = null;
        timer = new Timer(_ =>
        {
            ExecuteJob(job);

            if (_timers.ContainsKey(job.Id))
            {
                _timers.Remove(job.Id);
                ScheduleJob(job);
            }

            timer.Dispose();
        }, state: null, delay, TimeSpan.FromMilliseconds(-1));

        _timers.Add(job.Id, timer);
    }

    private async void ExecuteJob(T data)
    {
        try
        {
            _logger.LogInformation("Executing scheduled job: {FullName}", typeof(T).FullName);
            await ExecuteAsync(data);
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error during execution of scheduled job: {FullName}", typeof(T).FullName);
        }
    }
}