﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Digimuth.CrudFramework.JobScheduler.Context;

namespace Digimuth.CrudFramework.JobScheduler.Models;

public interface IDynamicJob
{
    Task RescheduleJobsAsync(IEnumerable<DynamicJobInstance> dynamicJobs);

    Task<IEnumerable<DynamicJobInstance>> GetAllJobsAsync();

    /// <summary>
    /// Reschedules a single job of type T defined in IDynamicJobDbContext DynamicJobs set.
    /// This job will keep rescheduling itself after update.
    /// It will dispose provided scope after it's done
    /// </summary>
    Task RescheduleSingleJob(DynamicJobInstance dynamicJob);

    /// <summary>
    /// Deletes a single job of type T defined in IDynamicJobDbContext DynamicJobs set.
    /// This job will be canceled and will not reschedule itself.
    /// It will dispose provided scope after it's done
    /// </summary>
    Task DeleteSingleJob(DynamicJobInstance dynamicJob);
}