﻿using System.Threading.Tasks;
using Cronos;

namespace Digimuth.CrudFramework.JobScheduler.Models;

public interface IJob
{
    CronExpression CronExpression { get; }
    bool UseLocalTime { get; }
    Task ExecuteAsync();
}