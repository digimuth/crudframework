using Digimuth.CrudFramework.Storage.Models;

namespace Digimuth.CrudFramework.JobScheduler.Context;

public class DynamicJobInstance : EntityBase
{
    public string CronExpression { get; set; }
}