﻿using Microsoft.EntityFrameworkCore;

namespace Digimuth.CrudFramework.JobScheduler.Context;

public interface IDynamicJobDbContext
{
    DbSet<DynamicJobInstance> DynamicJobs { get; set; }
}