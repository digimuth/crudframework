using Digimuth.CrudFramework.BlobStorage.Configuration;
using Digimuth.CrudFramework.BlobStorage.Services;
using Digimuth.CrudFramework.Common.Services.Interfaces;
using Moq;
using NUnit.Framework;
using System.IO;
using System.Threading.Tasks;

namespace Digimuth.CrudFramework.BlobStorage.IntegrationTests.Services;

public class Tests
{
    private FileSystemBlobStorageService _service;
    private FileSystemBlobStorageService _serviceWihtoutOrganisation;

    [SetUp]
    public void Setup()
    {
        var currentOrganisationServiceMock = new Mock<ICurrentOrganisationService>();

        var configuration = new FileSystemBlobStorageConfiguration
        {
            Directory = "./"
        };

        _service = new FileSystemBlobStorageService(configuration, currentOrganisationServiceMock.Object);
        _serviceWihtoutOrganisation = new FileSystemBlobStorageService(configuration);
    }

    [Test]
    public async Task ShouldBlobStorageWithoutOrganisationCreateReadAndDeleteFiles()
    {
        const string filename = "test.txt";
        const string content = "Hello Blob Storage without organisation World!";

        await using var inputFileStream = new MemoryStream();
        await using var streamWriter = new StreamWriter(inputFileStream);
        await streamWriter.WriteAsync(content);
        await streamWriter.FlushAsync();
        inputFileStream.Position = 0;

        var uploadedFileName = await _serviceWihtoutOrganisation.UploadBlobAsync(inputFileStream, filename);
        var downloadFileStream = await _serviceWihtoutOrganisation.DownloadBlobAsync(uploadedFileName);

        using var streamReader = new StreamReader(downloadFileStream);
        var output = await streamReader.ReadToEndAsync();
        Assert.AreEqual(content, output);

        await _serviceWihtoutOrganisation.DeleteBlobAsync(uploadedFileName);

        Assert.ThrowsAsync<FileNotFoundException>(() => _serviceWihtoutOrganisation.DownloadBlobAsync(uploadedFileName));
    }

    [Test]
    public async Task ShouldBlobStorageCreateReadAndDeleteFiles()
    {
        const string filename = "test.txt";
        const string content = "Hello Blob Storage World!";

        await using var inputFileStream = new MemoryStream();
        await using var streamWriter = new StreamWriter(inputFileStream);
        await streamWriter.WriteAsync(content);
        await streamWriter.FlushAsync();
        inputFileStream.Position = 0;

        var uploadedFileName = await _service.UploadBlobAsync(inputFileStream, filename);
        var downloadFileStream = await _service.DownloadBlobAsync(uploadedFileName);

        using var streamReader = new StreamReader(downloadFileStream);
        var output = await streamReader.ReadToEndAsync();
        Assert.AreEqual(content, output);

        await _service.DeleteBlobAsync(uploadedFileName);

        Assert.ThrowsAsync<FileNotFoundException>(() => _service.DownloadBlobAsync(uploadedFileName));
    }
}