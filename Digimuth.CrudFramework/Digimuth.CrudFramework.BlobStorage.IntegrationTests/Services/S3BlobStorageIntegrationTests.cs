using System.IO;
using System.Threading.Tasks;
using Amazon.S3;
using Digimuth.CrudFramework.BlobStorage.Configuration;
using Digimuth.CrudFramework.BlobStorage.Services;
using Digimuth.CrudFramework.Common.Services.Interfaces;
using Moq;
using NUnit.Framework;

namespace Digimuth.CrudFramework.BlobStorage.IntegrationTests.Services;

public class S3BlobStorageIntegrationTests
{
    private S3BlobStorageService _service;

    [SetUp]
    public void Setup()
    {
        var currentOrganisationServiceMock = new Mock<ICurrentOrganisationService>();

        var configuration = new S3BlobStorageConfiguration()
        {
            ServiceUrl = "https://fra1.digitaloceanspaces.com",
            BucketName = "digimuth",
            AccessKey = "DO00HDVUPJRZLLJEKDT7",
            SecretKey = "YYUusQRsTIDxEUAF410CKNTCisMBklcormJ0GF7SZ6E"
        };

        _service = new S3BlobStorageService(configuration, currentOrganisationServiceMock.Object);
    }

    [Test]
    public async Task ShouldS3BlobStorageCreateReadAndDeleteFiles()
    {
        const string filename = "test.txt";
        const string content = "Hello Blob Storage World!";

        await using var inputFileStream = new MemoryStream();
        await using var streamWriter = new StreamWriter(inputFileStream);
        await streamWriter.WriteAsync(content);
        await streamWriter.FlushAsync();
        inputFileStream.Position = 0;

        var uploadedFileName = await _service.UploadBlobAsync(inputFileStream, filename);
        var downloadFileStream = await _service.DownloadBlobAsync(uploadedFileName);

        using var streamReader = new StreamReader(downloadFileStream);
        var output = await streamReader.ReadToEndAsync();
        Assert.AreEqual(content, output);

        await _service.DeleteBlobAsync(uploadedFileName);

        Assert.ThrowsAsync<AmazonS3Exception>(() => _service.DownloadBlobAsync(uploadedFileName));
    }

}