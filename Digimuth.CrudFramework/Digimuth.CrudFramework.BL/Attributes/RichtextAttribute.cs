using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Digimuth.CrudFramework.BL.Dtos;
using Ganss.Xss;

namespace Digimuth.CrudFramework.BL.Attributes;

public class RichtextAttribute : DataTypeAttribute
{
    private readonly string _baseUrl;

    public RichtextAttribute(string errorMessage = null, string baseUrl = null) : base(DataType.Html)
    {
        _baseUrl = baseUrl;
        ErrorMessage = errorMessage ?? "Provided richtext value contains disallowed tags or attributes";
    }

    public override bool IsValid(object value)
    {
        if (value is string originalValue)
        {
            return CheckHtmlStringWithSanitizer(originalValue);
        }

        if (value is IEnumerable<LocalizableStringDto> originalLocalizableValue)
        {
            return originalLocalizableValue.All(v => CheckHtmlStringWithSanitizer(v.Value));
        }

        throw new Exception("Provided richtext type is not supported. Supported types are: string, IEnumerable<LocalizableStringDto>");
    }

    private bool CheckHtmlStringWithSanitizer(string originalValue)
    {
        var sanitizer = new HtmlSanitizer();

        var sanitizedValue = sanitizer.Sanitize(originalValue, _baseUrl);

        return sanitizedValue == originalValue;
    }
}