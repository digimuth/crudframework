using System.Collections.Generic;

namespace Digimuth.CrudFramework.BL.Dtos.ApplicationUser;

public class ApplicationUserDetailsBaseDto : ApplicationUserBaseDto
{
    protected internal IEnumerable<int> RolesInternal { get; set; } = new List<int>();
}