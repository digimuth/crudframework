using System.ComponentModel.DataAnnotations;

namespace Digimuth.CrudFramework.BL.Dtos.ApplicationUser;

public class ApplicationUserBaseDto : BaseDto
{
    public string UserName { get; set; }

    [EmailAddress]
    public string Email { get; set; }

    public bool EmailConfirmed { get; set; }
}