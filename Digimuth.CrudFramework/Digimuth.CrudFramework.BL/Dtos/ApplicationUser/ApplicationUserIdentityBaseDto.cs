using System.Collections.Generic;

namespace Digimuth.CrudFramework.BL.Dtos.ApplicationUser;

public class ApplicationUserIdentityBaseDto
{
    public long Id { get; set; }

    protected internal IEnumerable<int> RolesInternal { get; set; } = new List<int>();
}