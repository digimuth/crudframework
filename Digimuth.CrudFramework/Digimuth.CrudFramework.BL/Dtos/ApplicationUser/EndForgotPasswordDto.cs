using System.ComponentModel.DataAnnotations;

namespace Digimuth.CrudFramework.BL.Dtos.ApplicationUser;

public class EndForgotPasswordDto
{
    [EmailAddress]
    public string Email { get; set; }

    public string Token { get; set; }

    public string NewPassword { get; set; }
}