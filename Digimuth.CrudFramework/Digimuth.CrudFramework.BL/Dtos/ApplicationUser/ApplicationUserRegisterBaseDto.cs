namespace Digimuth.CrudFramework.BL.Dtos.ApplicationUser;

public class ApplicationUserRegisterBaseDto : ApplicationUserBaseDto
{
    public string Password { get; set; }

    public bool WithInvitation { get; set; }
}