using System.ComponentModel.DataAnnotations;

namespace Digimuth.CrudFramework.BL.Dtos.ApplicationUser;

public class ApplicationUserActivationBaseDto
{
    [EmailAddress]
    public string Email { get; set; }

    public string Password { get; set; }

    public string Token { get; set; }
}