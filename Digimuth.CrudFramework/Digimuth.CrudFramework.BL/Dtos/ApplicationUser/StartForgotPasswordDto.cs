using System.ComponentModel.DataAnnotations;

namespace Digimuth.CrudFramework.BL.Dtos.ApplicationUser;

public class StartForgotPasswordDto
{
    [EmailAddress]
    public string Email { get; set; }
}