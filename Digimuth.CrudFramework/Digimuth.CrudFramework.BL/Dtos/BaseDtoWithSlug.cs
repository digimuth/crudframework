using System;
using System.Text;

namespace Digimuth.CrudFramework.BL.Dtos;

public class BaseDtoWithSlug: BaseDto
{
    private const byte SLUG_PART_LENGTH = 4;
    private const string SLUG_ALLOWED_CHARACTERS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public string Slug { get; internal set; }

    protected string SlugPrefix { get; set; }

    internal void GenerateSlug()
    {
        var random = new Random();
        var builder = new StringBuilder(SlugPrefix);
        // builder.Append('-'); SF - removed, propose to force the dev to decide if they want a dash in the prefix themselves

        for (byte i = 0; i < SLUG_PART_LENGTH; ++i)
        {
            builder.Append(SLUG_ALLOWED_CHARACTERS[random.Next(SLUG_ALLOWED_CHARACTERS.Length)]);
        }

        Slug = builder.ToString();
    }
}