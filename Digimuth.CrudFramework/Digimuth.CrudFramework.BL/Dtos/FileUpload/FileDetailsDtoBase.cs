﻿namespace Digimuth.CrudFramework.BL.Dtos.FileUpload;

public class FileDetailsDtoBase : FileDtoBase
{
    public string FileName { get; set; }
    public string ContentType { get; set; }
    internal string BlobName { get; set; }
}