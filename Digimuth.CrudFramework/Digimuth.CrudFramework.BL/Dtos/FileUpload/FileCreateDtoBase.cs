﻿using Microsoft.AspNetCore.Http;

namespace Digimuth.CrudFramework.BL.Dtos.FileUpload;

public class FileCreateDtoBase : FileDtoBase
{
    public IFormFile File { get; set; }
    public string FileName { get; set; }
    public string ContentType { get; set; }
    public string BlobName { get; set; }
}