namespace Digimuth.CrudFramework.BL.Dtos;

public class LocalizableEntityDto<T> : LocalizableEntityBaseDto
{
    public T Value { get; set; }
}

public class LocalizableEntityBaseDto
{
    public string Locale { get; set; }
}