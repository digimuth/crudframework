using System;

namespace Digimuth.CrudFramework.BL.Dtos;

public class BaseDto
{
    public long Id { get; internal set; }

    public DateTimeOffset CreatedAt { get; internal set; }
    public long CreatedById { get; internal set; }

    public DateTimeOffset? UpdatedAt { get; internal set; }
    public long? UpdatedById { get; internal set; }
}