using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Digimuth.CrudFramework.BL.Dtos;
using Microsoft.EntityFrameworkCore.Storage;

namespace Digimuth.CrudFramework.BL.Services.Interfaces;

public interface IRepository<TSource>
{
    Task<List<TDest>> GetAllAsync<TDest>(Expression<Func<TSource, bool>> whereExpression, Func<IQueryable<TSource>, IOrderedQueryable<TSource>> orderBy = null);
    Task<TDestDetails> GetDetailsOrDefaultAsync<TDestDetails>(long id) where TDestDetails : BaseDto;

    Task<TDestDetails> CreateAsync<TInCreate, TDestDetails>(TInCreate dto)
        where TInCreate : BaseDto
        where TDestDetails : BaseDto;

    Task<TDestDetails> UpdateAsync<TInUpdate, TDestDetails>(TInUpdate dto, long id, Func<IQueryable<TSource>, IQueryable<TSource>> transformation = null)
        where TInUpdate : BaseDto
        where TDestDetails : BaseDto;

    Task DeleteAsync(long id);
    Task DeleteAsync(TSource entity);
    Task<bool> Exists(long id);
    Task<TSource> GetEntityAsync(long id);
    Task<TSource> GetEntityAsync(long id, Func<IQueryable<TSource>, IQueryable<TSource>> transformation);
    Task UpdateAsync(TSource entity);
    public IQueryable<TSource> GetQueryable(Expression<Func<TSource, bool>> whereExpression = null, Func<IQueryable<TSource>, IOrderedQueryable<TSource>> orderBy = null);
    IQueryable<TSource> GetTrackingQueryable(Expression<Func<TSource, bool>> whereExpression = null);
    Task<IDbContextTransaction> UseSerializableIsolatedTransaction();
    void DetachAddedEntities();
    Task SaveChangesAsync();
    void FillUpdateMetadataForDto(BaseDto dto);
    void FillCreationMetadataForDto(BaseDto dto);
    bool SplitQuery { get; set; }
}