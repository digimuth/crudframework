using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Digimuth.CrudFramework.BL.Dtos.ApplicationUser;

namespace Digimuth.CrudFramework.BL.Services.Interfaces;

public interface ICrudService<TDest> : ICrudService<TDest, TDest>
{

}

public interface ICrudService<TDest, TDestDetails> : ICrudService<TDest, TDestDetails, TDestDetails>
{

}

public interface ICrudService<TDest, TDestDetails, TInCreate> : ICrudService<TDest, TDestDetails, TInCreate, TInCreate>
{

}

public interface ICrudService<TDest, TDestDetails, in TInCreate, TInUpdate>
{
    Task<List<TDest>> GetAllAsync();
    Task<TDestDetails> GetDetailsOrDefaultAsync(long id);
    Task<TDestDetails> CreateAsync(TInCreate dto);
    Task<TDestDetails> UpdateAsync(TInUpdate dto, long id);
    Task<TDestDetails> UpdateAsync<TSource>(TInUpdate dto, long id, Func<IQueryable<TSource>, IQueryable<TSource>> transformation);
    Task DeleteAsync(long id);
    Task<bool> Exists(long id);
    ApplicationUserIdentityBaseDto ContextUser { get; }
    Task<bool> IsAuthorizedForReadAllAsync();
    Task<bool> IsAuthorizedForReadAsync(long id);
    Task<bool> IsAuthorizedForCreateAsync(TInCreate entity);
    Task<bool> IsAuthorizedForUpdateAsync(long id, TInUpdate entity);
    Task<bool> IsAuthorizedForDeleteAsync(long id);
    Task<TInUpdate> GetUpdateDtoAsync(long id);
}