using System.Threading.Tasks;
using Digimuth.CrudFramework.BL.Dtos.ApplicationUser;

namespace Digimuth.CrudFramework.BL.Services.Interfaces;

public interface IApplicationUserIdentityService
{
    void ClearApplicationUserIdentityCache(long userId);
    Task<ApplicationUserIdentityBaseDto> GetApplicationUserIdentityWithCacheAsync(long userId);
}