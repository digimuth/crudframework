﻿using Digimuth.CrudFramework.BL.Dtos;
using Digimuth.CrudFramework.BL.Dtos.FileUpload;
using System.IO;
using System.Threading.Tasks;

namespace Digimuth.CrudFramework.BL.Services.Interfaces;

public interface IFileService<TDest, TDestDetails, TInCreate, TInUpdate>
    : ICrudService<TDest, TDestDetails, TInCreate, TInUpdate>
    where TDest : FileDtoBase
    where TDestDetails : FileDetailsDtoBase
    where TInCreate : FileCreateDtoBase
    where TInUpdate : BaseDto
{
    Task<Stream> GetFileStream(long id);
}
