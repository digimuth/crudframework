using System.Collections.Generic;
using System.Threading.Tasks;
using Digimuth.CrudFramework.BL.Dtos.ApplicationUser;
using Digimuth.CrudFramework.Common.Models;

namespace Digimuth.CrudFramework.BL.Services.Interfaces;

public interface IApplicationUserServiceBase
<TApplicationUser,
    TApplicationUserDto,
    TApplicationUserDetailsDto,
    in TApplicationUserActivationDto,
    TApplicationUserRegisterDto> : IApplicationUserServiceBase
<TApplicationUser,
    TApplicationUserDto,
    TApplicationUserDetailsDto,
    TApplicationUserActivationDto,
    TApplicationUserRegisterDto,
    TApplicationUserRegisterDto>
    where TApplicationUserRegisterDto : ApplicationUserRegisterBaseDto
    where TApplicationUserDetailsDto : ApplicationUserDetailsBaseDto
    where TApplicationUserDto : ApplicationUserBaseDto
    where TApplicationUser : ApplicationUserBase
    where TApplicationUserActivationDto : ApplicationUserActivationBaseDto
{

}

public interface IApplicationUserServiceBase
    <TApplicationUser,
        TApplicationUserDto,
        TApplicationUserDetailsDto,
        in TApplicationUserActivationDto,
        TApplicationUserRegisterDto,
        TApplicationUserUpdateDto>
    : ICrudService<TApplicationUserDto, TApplicationUserDetailsDto, TApplicationUserRegisterDto, TApplicationUserUpdateDto>
    where TApplicationUser : ApplicationUserBase
    where TApplicationUserDto : ApplicationUserBaseDto
    where TApplicationUserDetailsDto : ApplicationUserDetailsBaseDto
    where TApplicationUserActivationDto : ApplicationUserActivationBaseDto
    where TApplicationUserRegisterDto : ApplicationUserRegisterBaseDto
    where TApplicationUserUpdateDto : ApplicationUserRegisterBaseDto
{
    Task UpdateUserRolesAsync(long id, IEnumerable<int> roles);
    Task ActivateUserAsync(TApplicationUserActivationDto applicationUserActivationDto);
    Task<List<TApplicationUser>> GetNotConfirmedUsersWithInvitationsSentMoreThanDaysAgo(int days);
    Task SendInvitationAsync(TApplicationUser user);
    Task<TApplicationUserDetailsDto> GetByEmailAsync(string email);
    Task StartForgotPasswordFlowAsync(string email);
    Task EndForgotPasswordFlowAsync(EndForgotPasswordDto dto);
    Task ResendInvitation(string email);
}