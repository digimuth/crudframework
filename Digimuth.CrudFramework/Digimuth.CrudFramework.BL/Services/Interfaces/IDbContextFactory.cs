using System.Threading.Tasks;
using Digimuth.CrudFramework.Storage;
using Microsoft.EntityFrameworkCore;

namespace Digimuth.CrudFramework.BL.Services.Interfaces;

public interface IDbContextFactory
{
    T GetDbContext<T>() where T : DbContext;
    DbContext GetDbContextForEntity<TEntity>();
    Task MigrateDbForOrganisationAsync(long organisationId, OrganisationDbContextBase dbContext = null);
}