using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Digimuth.CrudFramework.BL.Dtos;
using Digimuth.CrudFramework.BL.Dtos.ApplicationUser;
using Digimuth.CrudFramework.BL.Services.Interfaces;
using Digimuth.CrudFramework.Storage.Models;
using Microsoft.AspNetCore.Http;

namespace Digimuth.CrudFramework.BL.Services;

public class CrudService<TSource, TDest, TDestDetails, TInCreate, TInUpdate>
    : ICrudService<TDest, TDestDetails, TInCreate, TInUpdate>
    where TSource : EntityBase
    where TDest : BaseDto
    where TDestDetails : BaseDto
    where TInCreate : BaseDto
    where TInUpdate : BaseDto
{
    private readonly IRepository<TSource> _repository;
    private readonly IHttpContextAccessor _httpContextAccessor;

    public Expression<Func<TSource, bool>> AuthenticationReadingPredicate { get; protected set; }
    public Func<IQueryable<TSource>, IOrderedQueryable<TSource>> OrderBy { get; protected set; }
    public ApplicationUserIdentityBaseDto ContextUser
        => _httpContextAccessor?.HttpContext?.Items[nameof(ApplicationUserIdentityBaseDto)] as ApplicationUserIdentityBaseDto;

    public CrudService(IRepository<TSource> repository, IHttpContextAccessor httpContextAccessor)
    {
        _repository = repository;
        _httpContextAccessor = httpContextAccessor;
    }

    public virtual Task<List<TDest>> GetAllAsync()
    {
        return _repository.GetAllAsync<TDest>(AuthenticationReadingPredicate, OrderBy);
    }

    public virtual Task<TDestDetails> GetDetailsOrDefaultAsync(long id)
    {
        return _repository.GetDetailsOrDefaultAsync<TDestDetails>(id);
    }

    public virtual Task<TInUpdate> GetUpdateDtoAsync(long id)
    {
        return _repository.GetDetailsOrDefaultAsync<TInUpdate>(id);
    }

    public virtual Task<TDestDetails> UpdateAsync(TInUpdate dto, long id)
    {
        return _repository.UpdateAsync<TInUpdate, TDestDetails>(dto, id);
    }

    public Task<TDestDetails> UpdateAsync<TSource1>(TInUpdate dto, long id, Func<IQueryable<TSource1>, IQueryable<TSource1>> transformation)
    {
        return _repository.UpdateAsync<TInUpdate, TDestDetails>(dto, id, transformation as Func<IQueryable<TSource>, IQueryable<TSource>>);
    }

    public virtual Task<TDestDetails> CreateAsync(TInCreate dto)
    {
        return _repository.CreateAsync<TInCreate, TDestDetails>(dto);
    }

    public virtual Task DeleteAsync(long id)
    {
        return _repository.DeleteAsync(id);
    }

    public virtual Task<bool> Exists(long id)
    {
        return _repository.Exists(id);
    }

    internal Task UpdateAsync(TSource dto)
    {
        return _repository.UpdateAsync(dto);
    }

    internal Task<TSource> GetEntityAsync(
        long id,
        Func<IQueryable<TSource>, IQueryable<TSource>> transformation = null)
    {
        return _repository.GetEntityAsync(id, transformation);
    }

    public virtual Task<bool> IsAuthorizedForReadAllAsync()
    {
        return Task.FromResult(true);
    }

    public virtual Task<bool> IsAuthorizedForReadAsync(long id)
    {
        return Task.FromResult(true);
    }

    public virtual Task<bool> IsAuthorizedForCreateAsync(TInCreate entity)
    {
        return Task.FromResult(true);
    }

    public virtual Task<bool> IsAuthorizedForUpdateAsync(long id, TInUpdate entity)
    {
        return Task.FromResult(true);
    }

    public virtual Task<bool> IsAuthorizedForDeleteAsync(long id)
    {
        return Task.FromResult(true);
    }
}