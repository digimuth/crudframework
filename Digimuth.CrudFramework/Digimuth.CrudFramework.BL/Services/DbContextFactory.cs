using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Digimuth.CrudFramework.BL.Services.Interfaces;
using Digimuth.CrudFramework.Common.Services.Interfaces;
using Digimuth.CrudFramework.Storage;
using Digimuth.CrudFramework.Storage.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Digimuth.CrudFramework.BL.Services;

public class DbContextFactory<TCatalogContext> : IDbContextFactory
    where TCatalogContext : DbContext
{
    private readonly IServiceProvider _serviceProvider;

    public DbContextFactory(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }

    public T GetDbContext<T>() where T : DbContext
    {
        var dbContextType = typeof(T);

        if (dbContextType == typeof(TCatalogContext))
        {
            return GetCatalogDbContext() as T;
        }

        throw new ArgumentException($"DbContext type: {typeof(T).Name} not supported", "T");
    }

    public DbContext GetDbContextForEntity<TEntity>()
    {
        return GetCatalogDbContext();
    }

    public Task MigrateDbForOrganisationAsync(long organisationId, OrganisationDbContextBase dbContext = null)
    {
        throw new NotImplementedException();
    }

    private DbContext GetCatalogDbContext()
    {
        return _serviceProvider.GetRequiredService<TCatalogContext>();
    }
}

public class DbContextFactory<TCatalogContext, TOrganisationContext> : IDbContextFactory
    where TOrganisationContext : OrganisationDbContextBase
    where TCatalogContext : DbContext
{
    private readonly IServiceProviderScopingService _serviceProviderScopingService;
    private readonly IServiceProvider _serviceProvider;
    private readonly IConfiguration _configuration;
    private static readonly HashSet<long> _migrationsApplied = new HashSet<long>();
    private static readonly SemaphoreSlim _semaphore = new SemaphoreSlim(1, 1);
    private readonly ILogger<DbContextFactory<TCatalogContext, TOrganisationContext>> _logger;

    public DbContextFactory(
        IServiceProvider serviceProvider,
        IConfiguration configuration,
        IServiceProviderScopingService serviceProviderScopingService,
        ILogger<DbContextFactory<TCatalogContext, TOrganisationContext>> logger)
    {
        _serviceProvider = serviceProvider;
        _configuration = configuration;
        _serviceProviderScopingService = serviceProviderScopingService;
        _logger = logger;
    }

    public DbContext GetDbContextForEntity<TEntity>()
    {
        // TODO: figure out a more generic way to do that
        if (typeof(TEntity).Namespace.EndsWith("Catalog.Models"))
        {
            return GetCatalogDbContext();
        }

        return GetOrganisationDbContext();
    }

    public T GetDbContext<T>() where T : DbContext
    {
        var dbContextType = typeof(T);

        if (dbContextType == typeof(TOrganisationContext))
        {
            return GetOrganisationDbContext() as T;
        }

        if (dbContextType == typeof(TCatalogContext))
        {
            return GetCatalogDbContext() as T;
        }

        throw new ArgumentException($"DbContext type: {typeof(T).Name} not supported", "T");
    }

    private DbContext GetCatalogDbContext()
    {
        return _serviceProvider.GetRequiredService<TCatalogContext>();
    }

    private DbContext GetOrganisationDbContext()
    {
        var dbContext = _serviceProvider.GetRequiredService<TOrganisationContext>();
        var organisationId = dbContext.OrganisationId;

        MigrateDbForOrganisationAsync(organisationId, dbContext).GetAwaiter().GetResult();

        return dbContext;
    }

    public async Task MigrateDbForOrganisationAsync(long organisationId, OrganisationDbContextBase dbContext = null)
    {
        using var scope = _serviceProviderScopingService.GetServiceProviderScopeWithOrganisationId(organisationId);
        dbContext ??= scope.ServiceProvider.GetRequiredService<TOrganisationContext>();

        await MigrateDbForDbContextAsync(organisationId, dbContext);
    }

    private async Task MigrateDbForDbContextAsync(long organisationId, OrganisationDbContextBase dbContext)
    {
        await _semaphore.WaitAsync();

        try
        {
            if (!_migrationsApplied.Contains(organisationId))
            {
                _migrationsApplied.Add(organisationId);

                _logger.LogInformation($"Started migrating db for organisation {organisationId}");

                await dbContext.MigrateAsync();

                dbContext.Seed(_configuration);

                _logger.LogInformation($"Finished migrating db for organisation {organisationId}");
            }
        }
        finally
        {
            _semaphore.Release();
        }
    }
}