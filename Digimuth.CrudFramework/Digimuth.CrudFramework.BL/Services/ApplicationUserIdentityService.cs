using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using AutoMapper;
using Digimuth.CrudFramework.BL.Dtos.ApplicationUser;
using Digimuth.CrudFramework.BL.Services.Interfaces;
using Digimuth.CrudFramework.Common;
using Digimuth.CrudFramework.Common.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;

namespace Digimuth.CrudFramework.BL.Services;

public class ApplicationUserIdentityService<TApplicationUserIdentityDto, TApplicationUser>
    : IApplicationUserIdentityService
    where TApplicationUserIdentityDto : ApplicationUserIdentityBaseDto
    where TApplicationUser : ApplicationUserBase
{
    private readonly IDistributedCache _distributedCache;
    private readonly UserManager<TApplicationUser> _userManager;
    private readonly IMapper _mapper;

    public ApplicationUserIdentityService(
        UserManager<TApplicationUser> userManager,
        IMapper mapper,
        IDistributedCache distributedCache)
    {
        _userManager = userManager;
        _mapper = mapper;
        _distributedCache = distributedCache;
    }

    protected virtual bool SkipCache => false;

    public async Task<ApplicationUserIdentityBaseDto> GetApplicationUserIdentityWithCacheAsync(long userId)
    {
        if (SkipCache)
        {
            return await GetApplicationUserIdentityAsync(userId);
        }
        
        var cacheKey = $"User-{userId}";

        var existingIdentityJson = await _distributedCache.GetStringAsync(cacheKey);

        if (existingIdentityJson is null)
        {
            var userIdentity = await GetApplicationUserIdentityAsync(userId);
            var newIdentityJson = JsonSerializer.Serialize(userIdentity);
            await _distributedCache.SetStringAsync(cacheKey, newIdentityJson);
            return userIdentity;
        }

        return JsonSerializer.Deserialize<TApplicationUserIdentityDto>(existingIdentityJson);
    }

    public void ClearApplicationUserIdentityCache(long userId)
    {
        _distributedCache.Remove($"User-{userId}");
    }

    public virtual async Task<TApplicationUserIdentityDto> GetApplicationUserIdentityAsync(long id)
    {
        var dto = await _mapper.ProjectTo<TApplicationUserIdentityDto>(_userManager.Users).FirstAsync(u => u.Id == id);

        var claims = await _userManager.GetClaimsAsync(await _userManager.FindByIdAsync(id.ToString()));
        var roles = claims.Where(c => c.Type == CrudClaimTypes.Role);

        dto.RolesInternal = roles.Select(r => int.Parse(r.Value));

        return dto;
    }
}