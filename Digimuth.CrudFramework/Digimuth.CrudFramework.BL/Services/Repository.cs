using AutoMapper;
using Digimuth.CrudFramework.BL.Dtos;
using Digimuth.CrudFramework.BL.Dtos.ApplicationUser;
using Digimuth.CrudFramework.BL.Services.Interfaces;
using Digimuth.CrudFramework.Common.Services.Interfaces;
using Digimuth.CrudFramework.Storage.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Logging;
using Polly;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace Digimuth.CrudFramework.BL.Services;

public class Repository<TSource> : IRepository<TSource>
    where TSource : class, IEntityBase, new()
{
    private readonly IMapper _mapper;
    private readonly DbContext _dbContext;
    private readonly ILogger<Repository<TSource>> _logger;
    private readonly IHttpContextAccessor _httpContextAccessor;
    private readonly ICurrentLanguageService _currentLanguageService;

    private ApplicationUserIdentityBaseDto ContextUser
        => _httpContextAccessor?.HttpContext?.Items[nameof(ApplicationUserIdentityBaseDto)] as ApplicationUserIdentityBaseDto;

    public bool SplitQuery { get; set; } = false;

    public Repository(
        IMapper mapper,
        IDbContextFactory dbContextFactory,
        ILogger<Repository<TSource>> logger,
        IHttpContextAccessor httpContextAccessor,
        ICurrentLanguageService currentLanguageService = null)
    {
        _mapper = mapper;
        _logger = logger;
        _httpContextAccessor = httpContextAccessor;
        _currentLanguageService = currentLanguageService;

        _dbContext = dbContextFactory.GetDbContextForEntity<TSource>();
    }

    public Task<List<TDest>> GetAllAsync<TDest>(
        Expression<Func<TSource, bool>> whereExpression,
        Func<IQueryable<TSource>, IOrderedQueryable<TSource>> orderBy = null)
    {
        return _mapper.ProjectTo<TDest>(GetQueryable(whereExpression, orderBy).IgnoreAutoIncludes(),
              new
              {
                  language = _currentLanguageService?.GetCurrentLanguage(),
                  fallbackLanguage = _currentLanguageService?.GetFallbackLanguage()
              })
          .ToListAsync();
    }

    public Task<TDestDetails> GetDetailsOrDefaultAsync<TDestDetails>(long id)
        where TDestDetails : BaseDto
    {
        var query = _dbContext.Set<TSource>().AsQueryable().AsNoTracking();
        
        if (SplitQuery)
        {
            query = query.AsSplitQuery();
        }
        
        return _mapper.ProjectTo<TDestDetails>(query.IgnoreAutoIncludes(),
                new
                {
                    language = _currentLanguageService?.GetCurrentLanguage(),
                    fallbackLanguage = _currentLanguageService?.GetFallbackLanguage()
                })
            .FirstOrDefaultAsync(d => d.Id == id);
    }

    public async Task<TDestDetails> UpdateAsync<TInUpdate, TDestDetails>(TInUpdate dto, long id, Func<IQueryable<TSource>, IQueryable<TSource>> transformation = null)
        where TInUpdate : BaseDto
        where TDestDetails : BaseDto
    {
        if (ContextUser != null)
        {
            dto.UpdatedAt = DateTime.Now;
            dto.UpdatedById = ContextUser.Id;

            FillUpdateMetadataForDto(dto);
        }

        var sourceEntity = await GetEntityAsync(id, transformation);
        dto.CreatedAt = sourceEntity.CreatedAt;
        dto.CreatedById = sourceEntity.CreatedById;

        var entity = _mapper.Map(dto, sourceEntity, opt =>
        {
            opt.Items["language"] = _currentLanguageService?.GetCurrentLanguage();
            opt.Items["fallbackLanguage"] = _currentLanguageService?.GetFallbackLanguage();
        });

        entity.Id = id;

        await UpdateAsync(entity);

        var query = _dbContext.Set<TSource>().AsQueryable();
        
        if (SplitQuery)
        {
            query = query.AsSplitQuery();
        }

        return await _mapper.ProjectTo<TDestDetails>(query,
                new { language = _currentLanguageService?.GetCurrentLanguage() })
            .FirstOrDefaultAsync(d => d.Id == entity.Id);
    }

    public async Task UpdateAsync(TSource entity)
    {
        _dbContext.Set<TSource>().Update(entity);
        await SaveChangesAsync();
    }

    public async Task<TDestDetails> CreateAsync<TInCreate, TDestDetails>(TInCreate dto)
       where TInCreate : BaseDto
       where TDestDetails : BaseDto
    {
        FillCreationMetadataForDto(dto);

        var entity = await Policy
            .Handle<ValidationException>()
            .RetryAsync(3, onRetry: (_, retryCount) =>
            {
                _logger.LogInformation("Entity creation failed. " +
                                       $"Added entities of type {typeof(TSource)} will be detached. " +
                                       $"Retry count: ({retryCount})");

                DetachAddedEntities();
            })
            .ExecuteAsync(async () =>
            {
                if (dto is BaseDtoWithSlug dtoWithSlug && string.IsNullOrEmpty(dtoWithSlug.Slug))
                {
                    dtoWithSlug.GenerateSlug();
                    _logger.LogInformation($"Generated slug: [{dtoWithSlug.Slug}].");
                }

                var entity = _mapper.Map<TSource>(dto, opt =>
                {
                    opt.Items["language"] = _currentLanguageService?.GetCurrentLanguage();
                    opt.Items["fallbackLanguage"] = _currentLanguageService?.GetFallbackLanguage();
                });

                await _dbContext.Set<TSource>().AddAsync(entity);
                await SaveChangesAsync();

                return entity;
            });

        var queryParameters = new
        {
            language = _currentLanguageService?.GetCurrentLanguage() ?? _currentLanguageService?.GetFallbackLanguage()
        };

        var query = _dbContext.Set<TSource>().AsQueryable();
        
        if (SplitQuery)
        {
            query = query.AsSplitQuery();
        }

        return await _mapper.ProjectTo<TDestDetails>(query, queryParameters)
            .FirstOrDefaultAsync(d => d.Id == entity.Id);
    }

    public void DetachAddedEntities()
    {
        foreach (var entity in _dbContext.ChangeTracker.Entries()
                     .Where(e => e.Entity.GetType() == typeof(TSource) && e.State == EntityState.Added))
        {
            entity.State = EntityState.Detached;
        }
    }

    public async Task DeleteAsync(long id)
    {
        var entity = _dbContext.Set<TSource>().Local.FirstOrDefault(e => e.Id == id) ?? new TSource { Id = id };

        _dbContext.Attach(entity);

        await DeleteAsync(entity);
    }

    public async Task DeleteAsync(TSource entity)
    {
        _dbContext.Remove(entity);
        await SaveChangesAsync();
    }

    public Task<bool> Exists(long id)
    {
        return _dbContext.Set<TSource>()
            .AsNoTracking()
            .AnyAsync(e => e.Id == id);
    }

    public Task<TSource> GetEntityAsync(long id, Func<IQueryable<TSource>, IQueryable<TSource>> transformation)
    {
        var dbSet = _dbContext.Set<TSource>() as IQueryable<TSource>;
        
        if (SplitQuery)
        {
            dbSet = dbSet.AsSplitQuery();
        }
        
        if (transformation != null)
        {
            dbSet = transformation(dbSet);
        }

        return dbSet
            .FirstOrDefaultAsync(e => e.Id == id);
    }

    public Task<TSource> GetEntityAsync(long id)
    {
        return GetEntityAsync(id, null);
    }

    public IQueryable<TSource> GetQueryable(Expression<Func<TSource, bool>> whereExpression = null, Func<IQueryable<TSource>, IOrderedQueryable<TSource>> orderBy = null)
    {
        var source = _dbContext.Set<TSource>()
            .AsNoTracking();

        if (SplitQuery)
        {
            source = source.AsSplitQuery();
        }
        
        if (whereExpression != null)
        {
            source = source.Where(whereExpression);
        }

        if (orderBy != null)
        {
            source = orderBy(source);
        }

        return source;
    }

    public IQueryable<TSource> GetTrackingQueryable(Expression<Func<TSource, bool>> whereExpression = null)
    {
        var source = _dbContext.Set<TSource>().AsQueryable();

        if (SplitQuery)
        {
            source = source.AsSplitQuery();
        }
        
        if (whereExpression != null)
        {
            source = source.Where(whereExpression);
        }

        return source;
    }

    public async Task SaveChangesAsync()
    {
        try
        {
            await _dbContext.SaveChangesAsync();
        }
        catch (DbUpdateException e)
            when (e.InnerException is SqlException sqlException
                  && (sqlException.Number == 547       // Foreign Key reference conflict
                      || sqlException.Number == 2601)) // Duplicate key in Index
        {
            // TODO: change to some less specific message?
            _logger.LogError(e, "Exception during saving to database.");
            throw new ValidationException(sqlException.Message);
        }
        catch (Exception e)
        {
            if (e.InnerException is ValidationException validationException)
            {
                _logger.LogError(e, validationException.Message);
                throw validationException;
            }

            // If anything else happened do NOT silently fail.
            // TODO: Maybe use new exception instead of re-throwing.
            _logger.LogError(e, e.Message);
            throw;
        }
    }

    public void FillUpdateMetadataForDto(BaseDto dto)
    {
        if (dto == null)
        {
            return;
        }

        if (dto.CreatedAt == default)
        {
            dto.CreatedAt = DateTime.Now;
            dto.CreatedById = ContextUser.Id;
        }

        foreach (var property in dto.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance))
        {
            if (property.PropertyType.IsSubclassOf(typeof(BaseDto)))
            {
                var dependantDto = property.GetValue(dto) as BaseDto;
                FillCreationMetadataForDto(dependantDto);
            }
            else if (typeof(IEnumerable).IsAssignableFrom(property.PropertyType)
                     && property.PropertyType.IsGenericType
                     && property.PropertyType.GenericTypeArguments[0].IsSubclassOf(typeof(BaseDto)))
            {
                foreach (BaseDto dependantDto in (property.GetValue(dto) as IEnumerable) ?? Enumerable.Empty<object>())
                {
                    FillCreationMetadataForDto(dependantDto);
                }
            }
        }
    }

    public void FillCreationMetadataForDto(BaseDto dto)
    {
        if (dto == null)
        {
            return;
        }

        dto.CreatedAt = DateTime.Now;
        dto.CreatedById = ContextUser?.Id ?? 1;

        foreach (var property in dto.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance))
        {
            if (property.PropertyType.IsSubclassOf(typeof(BaseDto)))
            {
                var dependantDto = property.GetValue(dto) as BaseDto;
                FillCreationMetadataForDto(dependantDto);
            }
            else if (typeof(IEnumerable).IsAssignableFrom(property.PropertyType)
                     && property.PropertyType.IsGenericType
                     && property.PropertyType.GenericTypeArguments[0].IsSubclassOf(typeof(BaseDto)))
            {
                foreach (BaseDto dependantDto in (property.GetValue(dto) as IEnumerable) ?? Enumerable.Empty<object>())
                {
                    FillCreationMetadataForDto(dependantDto);
                }
            }
        }
    }

    public Task<IDbContextTransaction> UseSerializableIsolatedTransaction()
    {
        return _dbContext.Database.BeginTransactionAsync(IsolationLevel.Serializable);
    }
}