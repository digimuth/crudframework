using AutoMapper;
using Digimuth.CrudFramework.BL.Dtos.ApplicationUser;
using Digimuth.CrudFramework.BL.Services.Interfaces;
using Digimuth.CrudFramework.Common;
using Digimuth.CrudFramework.Common.Configuration;
using Digimuth.CrudFramework.Common.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Digimuth.CrudFramework.BL.Services;

public abstract class ApplicationUserServiceBase
    <TApplicationUser,
    TApplicationUserDto,
    TApplicationUserDetailsDto,
    TApplicationUserActivationDto,
    TApplicationUserRegisterDto>
    : ApplicationUserServiceBase<TApplicationUser,
        TApplicationUserDto,
        TApplicationUserDetailsDto,
        TApplicationUserActivationDto,
        TApplicationUserRegisterDto,
        TApplicationUserRegisterDto>
    where TApplicationUser : ApplicationUserBase
    where TApplicationUserDto : ApplicationUserBaseDto
    where TApplicationUserDetailsDto : ApplicationUserDetailsBaseDto
    where TApplicationUserActivationDto : ApplicationUserActivationBaseDto
    where TApplicationUserRegisterDto : ApplicationUserRegisterBaseDto
{
    protected ApplicationUserServiceBase(
        IMapper mapper,
        UserManager<TApplicationUser> userManager,
        AuthenticationConfiguration authenticationConfiguration,
        IApplicationUserIdentityService applicationUserIdentityService,
        IHttpContextAccessor httpContextAccessor,
        ILogger<
            ApplicationUserServiceBase<
                TApplicationUser,
                TApplicationUserDto,
                TApplicationUserDetailsDto,
                TApplicationUserActivationDto,
                TApplicationUserRegisterDto>> logger)
        : base(mapper, userManager, authenticationConfiguration, applicationUserIdentityService, httpContextAccessor, logger)
    {
    }
}

public abstract class ApplicationUserServiceBase
    <TApplicationUser,
        TApplicationUserDto,
        TApplicationUserDetailsDto,
        TApplicationUserActivationDto,
        TApplicationUserRegisterDto,
        TApplicationUserUpdateDto>
    : IApplicationUserServiceBase
    <TApplicationUser,
        TApplicationUserDto,
        TApplicationUserDetailsDto,
        TApplicationUserActivationDto,
        TApplicationUserRegisterDto,
        TApplicationUserUpdateDto>
    where TApplicationUser : ApplicationUserBase
    where TApplicationUserDto : ApplicationUserBaseDto
    where TApplicationUserDetailsDto : ApplicationUserDetailsBaseDto
    where TApplicationUserActivationDto : ApplicationUserActivationBaseDto
    where TApplicationUserRegisterDto : ApplicationUserRegisterBaseDto
    where TApplicationUserUpdateDto : ApplicationUserRegisterBaseDto
{
    protected readonly UserManager<TApplicationUser> UserManager;
    protected readonly AuthenticationConfiguration AuthenticationConfiguration;

    private readonly IMapper _mapper;
    private readonly IApplicationUserIdentityService _applicationUserIdentityService;
    private readonly IHttpContextAccessor _httpContextAccessor;
    private readonly ILogger<ApplicationUserServiceBase<TApplicationUser,
        TApplicationUserDto,
        TApplicationUserDetailsDto,
        TApplicationUserActivationDto,
        TApplicationUserRegisterDto,
        TApplicationUserUpdateDto>> _logger;

    public ApplicationUserIdentityBaseDto ContextUser =>
        _httpContextAccessor.HttpContext.Items[nameof(ApplicationUserIdentityBaseDto)] as ApplicationUserIdentityBaseDto;

    protected virtual bool AllowToChangePasswordInUpdate => true;

    protected ApplicationUserServiceBase(
        IMapper mapper,
        UserManager<TApplicationUser> userManager,
        AuthenticationConfiguration authenticationConfiguration,
        IApplicationUserIdentityService applicationUserIdentityService,
        IHttpContextAccessor httpContextAccessor,
        ILogger<ApplicationUserServiceBase<
            TApplicationUser,
            TApplicationUserDto,
            TApplicationUserDetailsDto,
            TApplicationUserActivationDto,
            TApplicationUserRegisterDto,
            TApplicationUserUpdateDto>> logger)
    {
        UserManager = userManager;
        AuthenticationConfiguration = authenticationConfiguration;

        _mapper = mapper;
        _applicationUserIdentityService = applicationUserIdentityService;
        _httpContextAccessor = httpContextAccessor;
        _logger = logger;
    }

    public virtual Task<List<TApplicationUser>> GetNotConfirmedUsersWithInvitationsSentMoreThanDaysAgo(int days)
    {
        return UserManager.Users
            .Where(u => !u.EmailConfirmed && u.InvitationLastSentAt < DateTime.Now.AddDays(-days))
            .ToListAsync();
    }

    public virtual async Task<TApplicationUserDetailsDto> CreateAsync(TApplicationUserRegisterDto dto)
    {
        dto.CreatedAt = DateTime.Now;
        dto.CreatedById = ContextUser?.Id ?? 0;

        var user = _mapper.Map<TApplicationUser>(dto);

        if (!dto.WithInvitation && AuthenticationConfiguration.PasswordFlowEnabled)
        {
            user.EmailConfirmed = !string.IsNullOrEmpty(dto.Password);

            var result = user.EmailConfirmed
                ? await UserManager.CreateAsync(user, dto.Password)
                : await UserManager.CreateAsync(user);

            ThrowExceptionOnErrorResult(result);
        }
        else
        {
            var result = await UserManager.CreateAsync(user);
            ThrowExceptionOnErrorResult(result);

            if (AuthenticationConfiguration.PasswordFlowEnabled)
            {
                await SendInvitationAsync(user);
            }
        }

        return _mapper.Map<TApplicationUserDetailsDto>(user);
    }

    public virtual Task<TApplicationUserDetailsDto> GetByEmailAsync(string email)
    {
        return _mapper.ProjectTo<TApplicationUserDetailsDto>(UserManager.Users.Where(u => u.Email == email))
            .FirstOrDefaultAsync();
    }

    public virtual async Task DeleteAsync(long id)
    {
        var user = await GetUserById(id);

        foreach (var login in await UserManager.GetLoginsAsync(user))
        {
            await UserManager.RemoveLoginAsync(user, login.LoginProvider, login.ProviderKey);
        }

        await UserManager.DeleteAsync(user);

        _applicationUserIdentityService.ClearApplicationUserIdentityCache(id);
    }

    public virtual async Task StartForgotPasswordFlowAsync(string email)
    {
        if (!AuthenticationConfiguration.PasswordFlowEnabled)
        {
            throw new NotSupportedException("Password flow not supported");
        }

        var user = await UserManager.Users.FirstOrDefaultAsync(u => u.Email == email);

        // silent fail to not leak information about user with specific email being registered in the system
        if (user == null)
        {
            _logger.LogWarning("Forgot password flow start attempt for non existing user, email: {0}", email);
            await Task.Delay(new Random().Next(70, 150));

            return;
        }

        if (!user.EmailConfirmed)
        {
            _logger.LogWarning("Forgot password flow start attempt for not confirmed user, email: {0}", email);
            
            await ResendInvitation(email);

            return;
        }

        var token = await UserManager.GeneratePasswordResetTokenAsync(user);
        await SendForgotPasswordNotificationAsync(user, token);
    }

    public virtual async Task EndForgotPasswordFlowAsync(EndForgotPasswordDto dto)
    {
        if (!AuthenticationConfiguration.PasswordFlowEnabled)
        {
            throw new NotSupportedException("Password flow not supported");
        }

        var user = await UserManager.Users.FirstOrDefaultAsync(u => u.Email == dto.Email);

        if (user == null)
        {
            // generic message to not leak information about user with specific email being registered in the system
            throw new ValidationException("Invalid token.");
        }

        var resetPasswordResult = await UserManager.ResetPasswordAsync(user, dto.Token, dto.NewPassword);
        ThrowExceptionOnErrorResult(resetPasswordResult);
    }

    public virtual Task<bool> Exists(long id)
    {
        return UserManager.Users.AnyAsync(u => u.Id == id);
    }

    public virtual Task<List<TApplicationUserDto>> GetAllAsync()
    {
        return _mapper.ProjectTo<TApplicationUserDto>(AllUsers()).ToListAsync();
    }

    public virtual async Task<TApplicationUserDetailsDto> GetDetailsOrDefaultAsync(long id)
    {
        var dto = await _mapper.ProjectTo<TApplicationUserDetailsDto>(UserManager.Users)
            .FirstOrDefaultAsync(u => u.Id == id);

        if (dto == null)
        {
            return null;
        }

        var roleClaimValues = (await GetRoleClaimsForUser(
                await UserManager.Users.FirstAsync(u => u.Id == id)))
            .Select(c => int.Parse(c.Value));

        dto.RolesInternal = roleClaimValues;

        return dto;
    }

    public virtual Task<TApplicationUserUpdateDto> GetUpdateDtoAsync(long id)
    {
        return _mapper
            .ProjectTo<TApplicationUserUpdateDto>(UserManager.Users)
            .FirstOrDefaultAsync(u => u.Id == id);
    }

    public virtual async Task UpdateUserRolesAsync(long id, IEnumerable<int> roles)
    {
        var user = await UserManager.Users.FirstOrDefaultAsync(u => u.Id == id);
        user.UpdatedAt = DateTime.Now;
        user.UpdatedById = ContextUser?.Id;

        await UserManager.UpdateAsync(user);

        var userClaims = (await GetRoleClaimsForUser(user)).ToList();
        var claimsToRemove = userClaims.Where(claim => !roles.Contains(ClaimValueToApplicationUserRoleType(claim.Value))).ToList();

        if (claimsToRemove.Any())
        {
            await UserManager.RemoveClaimsAsync(user, claimsToRemove);
        }

        var claimsToAdd = roles.Where(r => userClaims.All(c => c.Value != r.ToString("d"))).Select(r =>
            new Claim(CrudClaimTypes.Role, r.ToString("D"))).ToList();

        if (claimsToAdd.Any())
        {
            await UserManager.AddClaimsAsync(user, claimsToAdd);
        }

        _applicationUserIdentityService.ClearApplicationUserIdentityCache(id);
    }

    public virtual async Task<TApplicationUserDetailsDto> UpdateAsync(TApplicationUserUpdateDto dto, long id)
    {
        var user = await GetUserById(id);

        dto.UpdatedAt = DateTime.Now;
        dto.UpdatedById = ContextUser.Id;
        dto.CreatedById = user.CreatedById ?? 0;
        dto.CreatedAt = user.CreatedAt;
        dto.EmailConfirmed = user.EmailConfirmed;
        dto.UserName = dto.UserName ?? user.UserName ?? dto.Email;

        if (AllowToChangePasswordInUpdate)
        {
            if (!user.EmailConfirmed)
            {
                throw new ValidationException("Account not activated");
            }

            if (!string.IsNullOrEmpty(dto.Password) && AuthenticationConfiguration.PasswordFlowEnabled)
            {
                await ChangePassword(user, dto.Password);
            }
        }

        user = _mapper.Map(dto, user);
        user.Id = id;
        user.CreatedById = user.CreatedById == 0 ? null : user.CreatedById;
        var updateResult = await UserManager.UpdateAsync(user);
        ThrowExceptionOnErrorResult(updateResult);

        _applicationUserIdentityService.ClearApplicationUserIdentityCache(id);

        var roleClaimValues = (await GetRoleClaimsForUser(
                await UserManager.Users.FirstAsync(u => u.Id == id)))
            .Select(c => int.Parse(c.Value));

        var mappedUser = _mapper.Map<TApplicationUserDetailsDto>(user);
        mappedUser.RolesInternal = roleClaimValues;

        return mappedUser;
    }

    public virtual async Task ResendInvitation(string email)
    {
        if (!AuthenticationConfiguration.PasswordFlowEnabled)
        {
            throw new NotSupportedException("Password flow not supported");
        }

        var user = await GetByEmail(email);

        if (user != null && user.EmailConfirmed == false)
        {
            await SendInvitationAsync(user);
        }
    }

    public virtual async Task ActivateUserAsync(TApplicationUserActivationDto applicationUserActivationDto)
    {
        if (!AuthenticationConfiguration.PasswordFlowEnabled)
        {
            throw new NotSupportedException("Password flow not supported");
        }

        var user = await UserManager.Users.FirstOrDefaultAsync(u => applicationUserActivationDto.Email == u.Email);

        if (user == null)
        {
            throw new ValidationException("User e-mail doesn't exist");
        }

        if (user.EmailConfirmed)
        {
            throw new ValidationException("Account already activated");
        }

        var results = await Task.WhenAll(UserManager.PasswordValidators.Select(v => v.ValidateAsync(UserManager, user, applicationUserActivationDto.Password)));
        ThrowExceptionOnErrorResult(results);

        var result = await UserManager.ConfirmEmailAsync(user, applicationUserActivationDto.Token);
        ThrowExceptionOnErrorResult(result);

        user.UpdatedAt = DateTime.Now;
        user.UpdatedById = user.Id;

        await ChangePassword(user, applicationUserActivationDto.Password);

        _applicationUserIdentityService.ClearApplicationUserIdentityCache(user.Id);
    }

    public virtual async Task SendInvitationAsync(TApplicationUser user)
    {
        if (!AuthenticationConfiguration.PasswordFlowEnabled)
        {
            throw new NotSupportedException("Password flow not supported");
        }

        var token = await UserManager.GenerateEmailConfirmationTokenAsync(user);

        await SendInvitationNotificationAsync(user, token);

        user.InvitationLastSentAt = DateTime.Now;

        await UserManager.UpdateAsync(user);
    }

    public abstract Task<bool> IsAuthorizedForReadAllAsync();

    public abstract Task<bool> IsAuthorizedForReadAsync(long id);

    public abstract Task<bool> IsAuthorizedForCreateAsync(TApplicationUserRegisterDto entity);

    public abstract Task<bool> IsAuthorizedForUpdateAsync(long id, TApplicationUserUpdateDto entity);

    public abstract Task<bool> IsAuthorizedForDeleteAsync(long id);

    protected abstract Task SendInvitationNotificationAsync(TApplicationUser user, string token);

    protected abstract Task SendForgotPasswordNotificationAsync(TApplicationUser user, string token);

    protected virtual IQueryable<TApplicationUser> AllUsers()
    {
        var query = UserManager.Users;

        return query;
    }

    protected async Task ChangePassword(TApplicationUser user, string newPassword)
    {
        if (!AuthenticationConfiguration.PasswordFlowEnabled)
        {
            throw new NotSupportedException("Password flow not supported");
        }

        var results = await Task.WhenAll(UserManager.PasswordValidators.Select(v => v.ValidateAsync(UserManager, user, newPassword)));
        ThrowExceptionOnErrorResult(results);

        var result = await UserManager.RemovePasswordAsync(user);
        ThrowExceptionOnErrorResult(result);

        result = await UserManager.AddPasswordAsync(user, newPassword);
        ThrowExceptionOnErrorResult(result);
    }

    protected Task<TApplicationUser> GetUserById(long id)
    {
        return UserManager.Users.FirstAsync(u => u.Id == id);
    }

    protected Task<TApplicationUser> GetByEmail(string email)
    {
        return UserManager.Users.FirstOrDefaultAsync(u => u.Email == email);
    }

    protected async Task<IEnumerable<Claim>> GetRoleClaimsForUser(TApplicationUser user)
    {
        var userClaims = await UserManager.GetClaimsAsync(user);

        return userClaims.Where(c => c.Type == CrudClaimTypes.Role);
    }

    protected int ClaimValueToApplicationUserRoleType(string value)
    {
        return int.Parse(value);
    }

    protected static void ThrowExceptionOnErrorResult(params IdentityResult[] results)
    {
        var errors = results.Where(r => !r.Succeeded).SelectMany(r => r.Errors).ToList();
        if (errors.Any())
        {
            throw new ValidationException(string.Join(", ", errors.Select(e => e.Description)));
        }
    }

    public Task<TApplicationUserDetailsDto> UpdateAsync<TSource>(TApplicationUserUpdateDto dto, long id, Func<IQueryable<TSource>, IQueryable<TSource>> transformation)
    {
        throw new NotImplementedException();
    }
}