using System;
using System.Collections;
using System.Linq;
using AutoMapper.Extensions.ExpressionMapping;
using AutoMapper.Internal;
using Digimuth.CrudFramework.BL.Dtos;
using Digimuth.CrudFramework.BL.Dtos.ApplicationUser;
using Digimuth.CrudFramework.BL.Services;
using Digimuth.CrudFramework.BL.Services.Interfaces;
using Digimuth.CrudFramework.Common.Models;
using Digimuth.CrudFramework.Storage;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Digimuth.CrudFramework.BL.Extensions;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddCrudRepository(this IServiceCollection services)
    {
        return services
            .AddTransient(typeof(IRepository<>), typeof(Repository<>));
    }

    public static IServiceCollection AddDbContextFactory<TCatalogDbContext>(this IServiceCollection services)
        where TCatalogDbContext : DbContext
    {
        return services
            .AddTransient<IDbContextFactory, DbContextFactory<TCatalogDbContext>>();
    }

    public static IServiceCollection AddDbContextFactory<TCatalogDbContext, TOrganisationDbContext>(this IServiceCollection services)
        where TCatalogDbContext : DbContext
        where TOrganisationDbContext : OrganisationDbContextBase
    {
        return services
            .AddTransient<IDbContextFactory, DbContextFactory<TCatalogDbContext, TOrganisationDbContext>>();
    }

    public static IServiceCollection AddDefaultApplicationUserIdentityService
        <TApplicationUserIdentityDto, TApplicationUser>(this IServiceCollection services)
        where TApplicationUserIdentityDto : ApplicationUserIdentityBaseDto
        where TApplicationUser : ApplicationUserBase
    {
        return services
            .AddTransient<IApplicationUserIdentityService,
                ApplicationUserIdentityService<TApplicationUserIdentityDto, TApplicationUser>>();
    }

    public static bool IsDestinationTypeLocalizableEntityEnumerable(this Type destinationType)
    {
        if (!destinationType.IsGenericType || !destinationType.IsAssignableTo(typeof(IEnumerable)))
        {
            return false;
        }

        var genericArgument = destinationType.GetGenericArguments().First();

        if (genericArgument
            .IsAssignableTo(typeof(LocalizableEntityBaseDto)))
        {
            return true;
        }

        var baseType = genericArgument.BaseType;

        return baseType.IsGenericType && baseType.GetGenericTypeDefinition().IsAssignableTo(typeof(LocalizableEntityBaseDto));
    }

    public static IServiceCollection InitAutomapper(
        this IServiceCollection services,
        bool isLanguageFilteringEnabled,
        params Type[] automapperParams)
    {
        var parameters = automapperParams.Append(typeof(ServiceCollectionExtensions)).ToArray();

        return services.AddAutoMapper(config =>
        {
            config.AddExpressionMapping();

            if (!isLanguageFilteringEnabled)
            {
                return;
            }

            config.Internal().ForAllPropertyMaps(p => p.DestinationType.IsDestinationTypeLocalizableEntityEnumerable(), (pm, opt) =>
            {
                pm.MapFrom(pm.GetLanguageFilteringExpression());
            });
        }, parameters);
    }

    public static IServiceCollection InitAutomapper(this IServiceCollection services, params Type[] automapperParams)
    {
        return services.InitAutomapper(true, automapperParams);
    }
}