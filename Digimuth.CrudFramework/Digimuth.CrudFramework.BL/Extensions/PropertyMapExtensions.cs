using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using AutoMapper;

namespace Digimuth.CrudFramework.BL.Extensions;

public static class PropertyMapExtensions
{
    private static readonly MethodInfo _whereMethodGroup = typeof(Enumerable).GetMethods().First(m => m.Name == "Where");
    private static readonly MethodInfo _anyMethodGroup = typeof(Enumerable).GetMethods().First(m => m.Name == "Any" && m.GetParameters().Length == 2);

    public static LambdaExpression GetLanguageFilteringExpression(this PropertyMap pm)
    {
        // this will be LocalizableEntity<T> or LocalizableString or similar
        var localizableEntityType = pm.SourceType.GenericTypeArguments.First();
        var whereMethod = _whereMethodGroup.MakeGenericMethod(localizableEntityType);
        var inputParameter = Expression.Parameter(pm.TypeMap.SourceType, "s");

        // s.Member - "Member" is a source property name containing ICollection<Localizable...>
        Expression propertyAccessorExpression = pm.SourceMember is null
            ? Expression.Invoke(pm.CustomMapExpression, inputParameter)
            : Expression.PropertyOrField(inputParameter, pm.SourceMember.Name);

        // builds the inside of Where - (t => language == null || t.Locale == s.Member.Any(t1 => t1.Locale == language) ? language : fallbackLanguage)
        var whereCondition = GetWhereConditionLambdaExpression(localizableEntityType, propertyAccessorExpression);

        // this will build an expression (s => s.Member.Where(whereCondition))
        var filteringCallExpression =
            Expression.Call(whereMethod, propertyAccessorExpression, whereCondition);

        var returnType = typeof(IEnumerable<>).MakeGenericType(localizableEntityType);
        var type = typeof(Func<,>).MakeGenericType(pm.TypeMap.SourceType, returnType);

        // s => s.Member.Where(t => language == null || t.Locale == language)
        return Expression.Lambda(type, filteringCallExpression, inputParameter);
    }

    private static LambdaExpression GetWhereConditionLambdaExpression(Type localizableEntityType, Expression propertyAccessorExpression)
    {
        string language = null;
        string fallbackLanguage = null;
        var languageParameter = Expression.Invoke(Expression.Quote((Expression<Func<string>>) (() => language)));
        var fallbackLanguageParameter =  Expression.Invoke(Expression.Quote((Expression<Func<string>>) (() => fallbackLanguage)));
        var entityParameter = Expression.Parameter(localizableEntityType, "t");

        // t.Locale
        var localePropertyAccessExpression = Expression.Property(entityParameter, "Locale");

        // s.Member.Any(t1 => t1.Locale == language)
        var anyExpression =
            GetAnyCallExpression(localizableEntityType, propertyAccessorExpression, languageParameter);

        // s.Member.Any(t1 => t1.Locale == language) ? language : fallbackLanguage
        var conditionalAny = Expression.Condition(anyExpression, languageParameter, fallbackLanguageParameter);

        var whereConditionExpression = Expression.Or(
            // language == null
            Expression.Equal(languageParameter, Expression.Constant(null)),
            // t.Locale == s.Member.Any(t1 => t1.Locale == language) ? language : fallbackLanguage
            Expression.Equal(localePropertyAccessExpression, conditionalAny)
        );

        return Expression.Lambda(typeof(Func<,>)
            .MakeGenericType(localizableEntityType, typeof(bool)), whereConditionExpression, entityParameter);
    }

    private static MethodCallExpression GetAnyCallExpression(
        Type localizableEntityType,
        Expression propertyAccessorExpression,
        InvocationExpression languageParameter)
    {
        var entityParameterInAny = Expression.Parameter(localizableEntityType, "t1");
        var anyMethod = _anyMethodGroup.MakeGenericMethod(localizableEntityType);

        // t1.Locale == language
        var anyConditionExpression = Expression.Equal(Expression.Property(entityParameterInAny, "Locale"), languageParameter);

        // s.Member.Any(t1 => t1.Locale == language)
        var anyCondition = Expression.Lambda(typeof(Func<,>)
            .MakeGenericType(localizableEntityType, typeof(bool)), anyConditionExpression, entityParameterInAny);

        return Expression.Call(anyMethod, propertyAccessorExpression, anyCondition);
    }
}