using AutoMapper;
using Digimuth.CrudFramework.BL.Dtos;
using Digimuth.CrudFramework.Storage.Models;

namespace Digimuth.CrudFramework.BL.MappingProfiles;

public class LocalizableEntityProfile : Profile
{
    public LocalizableEntityProfile()
    {
        CreateMap<LocalizableString, LocalizableStringDto>().ReverseMap();
        CreateMap(typeof(LocalizableEntity<>), typeof(LocalizableEntityDto<>)).ReverseMap();
    }
}