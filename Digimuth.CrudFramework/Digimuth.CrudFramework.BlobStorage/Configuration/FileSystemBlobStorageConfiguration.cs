namespace Digimuth.CrudFramework.BlobStorage.Configuration;

public class FileSystemBlobStorageConfiguration
{
    public string Directory { get; set; }
}