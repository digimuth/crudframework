namespace Digimuth.CrudFramework.BlobStorage.Configuration;

public class AzureBlobStorageConfiguration
{
    public string ConnectionString { get; set; }

    public string ContainerName { get; set; }
}