namespace Digimuth.CrudFramework.BlobStorage.Configuration;

public class S3BlobStorageConfiguration
{
    public string AccessKey { get; set; }
    
    public string SecretKey { get; set; }
    
    public string BucketName { get; set; }
    
    public string? ServiceUrl { get; set; }
}