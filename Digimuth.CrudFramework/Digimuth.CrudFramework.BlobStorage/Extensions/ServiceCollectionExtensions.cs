using Digimuth.CrudFramework.BlobStorage.Configuration;
using Digimuth.CrudFramework.BlobStorage.Services;
using Digimuth.CrudFramework.BlobStorage.Services.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Digimuth.CrudFramework.BlobStorage.Extensions;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddAzureBlobStorage(
        this IServiceCollection services,
        IConfiguration configuration,
        AzureBlobStorageConfiguration blobConfiguration = null)
    {
        blobConfiguration ??= configuration.GetSection("BlobStorage").Get<AzureBlobStorageConfiguration>();

        return services
            .AddSingleton(blobConfiguration)
            .AddTransient<IBlobStorageService, AzureBlobStorageService>()
            .AddTransient<IBlobStorageMigrationService, AzureBlobStorageMigrationService>();
    }
    
    public static IServiceCollection AddS3BlobStorage(
        this IServiceCollection services,
        IConfiguration configuration,
        S3BlobStorageConfiguration blobConfiguration = null)
    {
        blobConfiguration ??= configuration.GetSection("BlobStorage").Get<S3BlobStorageConfiguration>();

        return services
            .AddSingleton(blobConfiguration)
            .AddTransient<IBlobStorageService, S3BlobStorageService>();
    }

    public static IServiceCollection AddFileSystemBlobStorage(
        this IServiceCollection services,
        IConfiguration configuration,
        FileSystemBlobStorageConfiguration blobConfiguration = null)
    {
        blobConfiguration ??= configuration.GetSection("BlobStorage").Get<FileSystemBlobStorageConfiguration>();

        return services
            .AddSingleton(blobConfiguration)
            .AddTransient<IBlobStorageService, FileSystemBlobStorageService>();
    }
}