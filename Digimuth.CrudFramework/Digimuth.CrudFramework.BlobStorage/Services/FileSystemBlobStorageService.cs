using Digimuth.CrudFramework.BlobStorage.Configuration;
using Digimuth.CrudFramework.BlobStorage.Services.Interfaces;
using Digimuth.CrudFramework.Common.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;

namespace Digimuth.CrudFramework.BlobStorage.Services;

public class FileSystemBlobStorageService : IBlobStorageService
{
    private readonly long _organisationId;
    private readonly FileSystemBlobStorageConfiguration _configuration;

    public FileSystemBlobStorageService(
        FileSystemBlobStorageConfiguration configuration,
        ICurrentOrganisationService currentOrganisationService = null)
    {
        _configuration = configuration;
        _organisationId = currentOrganisationService?.GetCurrentOrganisationId() ?? 0;
    }

    public Task<Stream> DownloadBlobAsync(string blobFilename)
    {
        var filename = GetFullFilename(blobFilename);

        return Task.FromResult((Stream)new FileStream(filename, FileMode.Open, FileAccess.Read));
    }

    public async Task<Stream> DownloadMultipleBlobsAsZipAsync(IEnumerable<string> blobFilenames)
    {
        var outputStream = new MemoryStream();

        using (var zipArchive = new ZipArchive(outputStream, ZipArchiveMode.Create, true))
        {
            foreach (var blobFilename in blobFilenames)
            {
                var filename = GetFullFilename(blobFilename);

                await using var fileStream = new FileStream(filename, FileMode.Open, FileAccess.Read);
            
                var originalFileName = GetOriginalFilename(blobFilename);
            
                var entry = zipArchive.CreateEntry(originalFileName);
            
                await using var entryStream = entry.Open();
            
                await fileStream.CopyToAsync(entryStream);
                await fileStream.FlushAsync();
                await entryStream.FlushAsync();
            }
        }

        await outputStream.FlushAsync();
        outputStream.Seek(0, SeekOrigin.Begin);

        return outputStream;
    }

    public async Task<string> UploadBlobAsync(Stream fileContentStream, string blobFilename)
    {
        Directory.CreateDirectory(Path.Combine(_configuration.Directory, _organisationId.ToString()));

        blobFilename = $"{Guid.NewGuid()}-{blobFilename}";
        var filename = GetFullFilename(blobFilename);

        await using var stream = new FileStream(filename, FileMode.CreateNew, FileAccess.Write);

        await fileContentStream.CopyToAsync(stream);

        return blobFilename;
    }

    public Task<string[]> ListAllBlobsAsync()
    {
        var directory = Path.Combine(_configuration.Directory, _organisationId.ToString());

        var directoryInfo = new DirectoryInfo(directory);
        var fileNames = directoryInfo.GetFiles()
            .Where(f => !f.Attributes.HasFlag(FileAttributes.Hidden))
            .Select(f => f.Name)
            .ToArray();

        return Task.FromResult(fileNames);
    }

    public Task DeleteBlobAsync(string blobFilename)
    {
        var filename = GetFullFilename(blobFilename);

        return Task.Run(() => File.Delete(filename));
    }

    private string GetFullFilename(string blobFilename)
    {
        return Path.Combine(_configuration.Directory, _organisationId.ToString(), blobFilename);
    }

    private string GetOriginalFilename(string blobFilename)
    {
        return string.Join('-', blobFilename.Split('-').Skip(5));
    }
}