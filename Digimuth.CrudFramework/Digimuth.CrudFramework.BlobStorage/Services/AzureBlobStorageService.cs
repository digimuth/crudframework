using Azure.Storage.Blobs;
using Digimuth.CrudFramework.BlobStorage.Configuration;
using Digimuth.CrudFramework.BlobStorage.Services.Interfaces;
using Digimuth.CrudFramework.Common.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Digimuth.CrudFramework.BlobStorage.Services;

public class AzureBlobStorageService : IBlobStorageService
{
    private readonly BlobContainerClient _blobContainerClient;

    public AzureBlobStorageService(
        AzureBlobStorageConfiguration configuration,
        IBlobStorageMigrationService blobStorageMigrationService,
        ICurrentOrganisationService currentOrganisationService = null)
    {
        var organisationId = currentOrganisationService?.GetCurrentOrganisationId() ?? 0;
        var containerName = string.Format(configuration.ContainerName, organisationId);

        _blobContainerClient = new BlobContainerClient(configuration.ConnectionString, containerName);
        blobStorageMigrationService.CreateCatalogForOrganisationIfNotExistsAsync(organisationId).GetAwaiter()
            .GetResult();
    }

    public async Task<string> UploadBlobAsync(Stream fileContentStream, string filename)
    {
        var blobFilename = $"{Guid.NewGuid()}-{filename}";
        var blobClient = _blobContainerClient.GetBlobClient(blobFilename);

        await blobClient.UploadAsync(fileContentStream);

        return blobFilename;
    }

    public async Task<Stream> DownloadBlobAsync(string blobFilename)
    {
        var blobClient = _blobContainerClient.GetBlobClient(blobFilename);

        var response = await blobClient.DownloadAsync();

        return response.Value.Content;
    }

    public async Task DeleteBlobAsync(string blobFilename)
    {
        var blobClient = _blobContainerClient.GetBlobClient(blobFilename);

        await blobClient.DeleteAsync();
    }

    public async Task<string[]> ListAllBlobsAsync()
    {
        var pages = await _blobContainerClient.GetBlobsAsync().AsPages().ToListAsync();

        return pages.SelectMany(p => p.Values).Select(b => b.Name).ToArray();
    }

    public async Task<Stream> DownloadMultipleBlobsAsZipAsync(IEnumerable<string> blobFilenames)
    {
        throw new NotImplementedException();
    }
}