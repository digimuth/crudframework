using System.Threading.Tasks;

namespace Digimuth.CrudFramework.BlobStorage.Services.Interfaces;

public interface IBlobStorageMigrationService
{
    Task CreateCatalogForOrganisationIfNotExistsAsync(long organisationId = 0);
}