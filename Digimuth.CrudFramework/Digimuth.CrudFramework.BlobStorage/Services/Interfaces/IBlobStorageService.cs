using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Digimuth.CrudFramework.BlobStorage.Services.Interfaces;

public interface IBlobStorageService
{
    Task<Stream> DownloadBlobAsync(string blobFilename);
    Task<string> UploadBlobAsync(Stream fileContentStream, string filename);
    Task DeleteBlobAsync(string blobFilename);
    Task<string[]> ListAllBlobsAsync();
    Task<Stream> DownloadMultipleBlobsAsZipAsync(IEnumerable<string> blobFilenames);
}