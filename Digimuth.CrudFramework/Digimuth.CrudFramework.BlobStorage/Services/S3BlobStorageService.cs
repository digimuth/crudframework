using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Amazon.S3;
using Amazon.S3.Transfer;
using Digimuth.CrudFramework.BlobStorage.Configuration;
using Digimuth.CrudFramework.BlobStorage.Services.Interfaces;
using Digimuth.CrudFramework.Common.Services.Interfaces;

namespace Digimuth.CrudFramework.BlobStorage.Services;

public class S3BlobStorageService : IBlobStorageService
{
    private readonly S3BlobStorageConfiguration _configuration;
    private readonly AmazonS3Client _s3Client;
    private readonly long _organisationId;

    public S3BlobStorageService(S3BlobStorageConfiguration configuration,
        ICurrentOrganisationService currentOrganisationService = null)
    {
        _configuration = configuration;
        var accessKey = configuration.AccessKey;
        var secretKey = configuration.SecretKey;
        
        var config = new AmazonS3Config();

        if (!string.IsNullOrEmpty(configuration.ServiceUrl))
        {
            config.ServiceURL = configuration.ServiceUrl;
        }
        
        _s3Client = new AmazonS3Client(
            accessKey,
            secretKey,
            config
        );
        
        _organisationId = currentOrganisationService?.GetCurrentOrganisationId() ?? 0;
    }

    public async Task<Stream> DownloadBlobAsync(string blobFilename)
    {
        var filename = Path.Combine(_organisationId.ToString(), blobFilename);

        var response = await _s3Client.GetObjectAsync(_configuration.BucketName, filename);

        return response.ResponseStream;
    }

    public async Task<string> UploadBlobAsync(Stream fileContentStream, string filename)
    {
        var blobFilename = $"{Guid.NewGuid()}-{filename}";
        var blobPath = Path.Combine(_organisationId.ToString(), blobFilename);
        
        var fileTransferUtility = new TransferUtility(_s3Client);
        var fileTransferUtilityRequest = new TransferUtilityUploadRequest
        {
            BucketName = _configuration.BucketName,
            InputStream = fileContentStream,
            Key = blobPath
        };
        
        await fileTransferUtility.UploadAsync(fileTransferUtilityRequest);

        return blobFilename;
    }

    public async Task DeleteBlobAsync(string blobFilename)
    {
        var filename = Path.Combine(_organisationId.ToString(), blobFilename);

        await _s3Client.DeleteObjectAsync(_configuration.BucketName, filename);
    }

    public async Task<string[]> ListAllBlobsAsync()
    {
        var response = await _s3Client.ListObjectsAsync(_configuration.BucketName);

        return response.S3Objects.Select(s => s.Key).ToArray();
    }

    public async Task<Stream> DownloadMultipleBlobsAsZipAsync(IEnumerable<string> blobFilenames)
    {
        throw new NotImplementedException();
    }
}