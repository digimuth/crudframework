using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Azure.Storage.Blobs;
using Digimuth.CrudFramework.BlobStorage.Configuration;
using Digimuth.CrudFramework.BlobStorage.Services.Interfaces;
using Microsoft.Extensions.Logging;

namespace Digimuth.CrudFramework.BlobStorage.Services;

public class AzureBlobStorageMigrationService : IBlobStorageMigrationService
{
    private static readonly HashSet<long> _migrationsApplied = new HashSet<long>();
    private static readonly SemaphoreSlim _semaphore = new SemaphoreSlim(1, 1);
    private readonly ILogger<AzureBlobStorageMigrationService> _logger;
    private readonly AzureBlobStorageConfiguration _configuration;

    public AzureBlobStorageMigrationService(
        AzureBlobStorageConfiguration configuration, ILogger<AzureBlobStorageMigrationService> logger)
    {
        _configuration = configuration;
        _logger = logger;
    }

    public async Task CreateCatalogForOrganisationIfNotExistsAsync(long organisationId = 0)
    {
        await _semaphore.WaitAsync();

        try
        {
            if (!_migrationsApplied.Contains(organisationId))
            {
                _migrationsApplied.Add(organisationId);
                var containerName = string.Format(_configuration.ContainerName, organisationId);
                var blobContainerClient = new BlobContainerClient(_configuration.ConnectionString, containerName);

                _logger.LogInformation("Started ensuring blob storage catalog exists for organisation {0}", organisationId);

                await blobContainerClient.CreateIfNotExistsAsync();

                _logger.LogInformation("Finished ensuring blob storage catalog exists for organisation {0}", organisationId);
            }
        }
        finally
        {
            _semaphore.Release();
        }
    }
}