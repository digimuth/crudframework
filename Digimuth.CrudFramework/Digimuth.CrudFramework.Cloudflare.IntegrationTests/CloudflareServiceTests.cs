using System;
using System.Threading.Tasks;
using Digimuth.CrudFramework.Cloudflare.Configuration;
using Digimuth.CrudFramework.Cloudflare.Services;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace Digimuth.CrudFramework.Cloudflare.IntegrationTests;

public class CloudflareServiceTests : IDisposable
{
    private readonly CloudflareService _service;
    private readonly ServiceProvider _serviceProvider;

    public CloudflareServiceTests()
    {
        _serviceProvider = new ServiceCollection()
            .AddTransient<CloudflareService>()
            .AddHttpClient()
            .AddSingleton(new CloudflareConfiguration
            {
                AccountId = "d7f83bdd95426b45e185e84c749cbeb6",
                Token = "eAqisvtdtQWGs5ldjzsrkkxV8pzET2Pe8QJsecW5"
            }).BuildServiceProvider();

        _service = _serviceProvider.GetRequiredService<CloudflareService>();
    }

    [Fact]
    public async Task ShouldGetPagesProjects()
    {
        var result = await _service.GetPagesProjectsAsync();

        Assert.Collection(result, p => Assert.Equal("beefteka", p.Name), p => Assert.Equal("browarkolo", p.Name));
    }

    [Theory]
    [InlineData("digimuth", true)]
    [InlineData("beefteka", true)]
    [InlineData("browarkolo", true)]
    public async Task ShouldGetPagesProjectByName(string projectName, bool expected)
    {
        var result = await _service.GetPagesProjectByNameAsync(projectName);

        Assert.Equal(expected, result.Name == projectName);
    }
    [InlineData("google", false)]

    [Fact]
    public async Task ShouldTriggerProductionDeployment()
    {
        var result = await _service.StartProductionDeploymentAsync("beefteka");

        Assert.Equal("production", result.Environment);
        Assert.Equal("beefteka", result.ProjectName);
    }

    public void Dispose()
    {
        _serviceProvider.Dispose();
    }
}