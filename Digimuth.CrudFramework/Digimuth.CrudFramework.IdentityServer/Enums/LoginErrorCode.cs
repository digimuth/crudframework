﻿namespace Digimuth.CrudFramework.IdentityServer.Enums;

internal enum LoginErrorCode
{
    UnknownError            = 0,
    InvalidCallbackUrl      = 1,
    MissingLoginInfo        = 2,
    UserLockedOut           = 3,
    UserDoesNotExist        = 4,
    InvalidReturnUrl        = 5,
}