using Digimuth.CrudFramework.Common.Configuration;
using IdentityServer4.Validation;

namespace Digimuth.CrudFramework.IdentityServer.Validators;
public class PasswordFlowRequestValidator : ICustomTokenRequestValidator
{
    private readonly AuthenticationConfiguration _authenticationConfiguration;

    public PasswordFlowRequestValidator(AuthenticationConfiguration authenticationConfiguration)
    {
        _authenticationConfiguration = authenticationConfiguration;
    }

    public Task ValidateAsync(CustomTokenRequestValidationContext context)
    {
        if (!_authenticationConfiguration.PasswordFlowEnabled)
        {
            context.Result.IsError = true;
            context.Result.Error = "Password flow not enabled.";
        }

        return Task.CompletedTask;
    }
}