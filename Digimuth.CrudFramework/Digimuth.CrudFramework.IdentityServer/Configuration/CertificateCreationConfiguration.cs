namespace Digimuth.CrudFramework.IdentityServer.Configuration;

public class CertificateCreationConfiguration
{
    public string Path { get; set; }

    public string Name { get; set; }

    public int ValidityInYears { get; set; }
}