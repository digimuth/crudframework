﻿namespace Digimuth.CrudFramework.IdentityServer.Configuration;

public class TokenServiceConfiguration
{
    public int MaxTokenValidationTimeInDays { get; set; }
}