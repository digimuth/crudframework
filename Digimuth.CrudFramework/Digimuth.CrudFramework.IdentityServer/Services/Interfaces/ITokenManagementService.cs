﻿using System.Security.Claims;
using IdentityServer4.Validation;

namespace Digimuth.CrudFramework.IdentityServer.Services.Interfaces;

public interface ITokenManagementService
{
    Task<string> AddTokenAsync(DateTimeOffset expirationDate, IEnumerable<Claim> claims);

    Task<TokenValidationResult> ValidateTokenAsync(string tokenString);

    bool IsExpirationDateCorrect(DateTimeOffset expirationDate);

    bool IsTokenLiftetimeCorrect(DateTimeOffset expirationDate);
}