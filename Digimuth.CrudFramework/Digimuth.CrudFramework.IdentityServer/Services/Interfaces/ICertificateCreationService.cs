using System.Security.Cryptography.X509Certificates;

namespace Digimuth.CrudFramework.IdentityServer.Services.Interfaces;

public interface ICertificateCreationService
{
    X509Certificate2 CreateRsaDigitalSigningCertificate(string dnsName, int validityPeriodInYears);
    Task<X509Certificate2> GetOrCreateSigningCertificate(string path, string dnsName, int validityPeriodInYears);
}