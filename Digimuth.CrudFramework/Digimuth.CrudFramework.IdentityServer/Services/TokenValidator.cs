using IdentityServer4.Validation;
using TokenValidationResult = Digimuth.CrudFramework.Common.Models.TokenValidationResult;

namespace Digimuth.CrudFramework.IdentityServer.Services;

public class TokenValidator : Digimuth.CrudFramework.Common.Services.Interfaces.ITokenValidator
{
    private readonly ITokenValidator _tokenValidator;

    public TokenValidator(ITokenValidator tokenValidator)
    {
        _tokenValidator = tokenValidator;
    }

    public async Task<TokenValidationResult> ValidateIdentityTokenAsync(string token)
    {
        var result = await _tokenValidator.ValidateIdentityTokenAsync(token);

        return new()
        {
            IsError = result.IsError,
            Claims = result.Claims.ToArray()
        };
    }
}