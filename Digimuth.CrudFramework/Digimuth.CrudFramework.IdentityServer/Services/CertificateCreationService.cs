using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using CertificateManager;
using CertificateManager.Models;
using Digimuth.CrudFramework.IdentityServer.Services.Interfaces;

namespace Digimuth.CrudFramework.IdentityServer.Services;

public class CertificateCreationService : ICertificateCreationService
{
    private readonly CreateCertificates _createCertificates;
    private readonly ImportExportCertificate _importExportCertificate;
    private const string PASSWORD = "03993cd0bfab432fbe12100bc4d5f66a";

    public CertificateCreationService(CreateCertificates createCertificates, ImportExportCertificate importExportCertificate)
    {
        _createCertificates = createCertificates;
        _importExportCertificate = importExportCertificate;
    }

    public async Task<X509Certificate2> GetOrCreateSigningCertificate(string path, string dnsName, int validityPeriodInYears)
    {
        async Task CreateSigningCertificate()
        {
            Directory.CreateDirectory(Path.GetDirectoryName(path)!);
            var certificate = CreateRsaDigitalSigningCertificate(dnsName, validityPeriodInYears);
            var bytes = _importExportCertificate.ExportSelfSignedCertificatePfx(PASSWORD, certificate);
            await File.WriteAllBytesAsync(path, bytes);
        }

        if (!File.Exists(path))
        {
            await CreateSigningCertificate();
        }

        var fileContents = await File.ReadAllBytesAsync(path);
        var certificateFromFile = new X509Certificate2(fileContents, PASSWORD);

        if (DateTime.Parse(certificateFromFile.GetExpirationDateString()) < DateTime.UtcNow)
        {
            await CreateSigningCertificate();
        }

        return certificateFromFile;
    }

    public X509Certificate2 CreateRsaDigitalSigningCertificate(string dnsName, int validityPeriodInYears)
    {
        var basicConstraints = new BasicConstraints
        {
            CertificateAuthority = false,
            HasPathLengthConstraint = false,
            PathLengthConstraint = 0,
            Critical = false
        };

        var subjectAlternativeName = new SubjectAlternativeName
        {
            DnsName = new List<string>{ dnsName }
        };

        var x509KeyUsageFlags = X509KeyUsageFlags.DigitalSignature;

        var enhancedKeyUsages = new OidCollection();

        var certificate = _createCertificates.NewRsaSelfSignedCertificate(
            new DistinguishedName { CommonName = dnsName },
            basicConstraints,
            new ValidityPeriod
            {
                ValidFrom = DateTimeOffset.UtcNow,
                ValidTo = DateTimeOffset.UtcNow.AddYears(validityPeriodInYears)
            },
            subjectAlternativeName,
            enhancedKeyUsages,
            x509KeyUsageFlags,
            new RsaConfiguration
            {
                KeySize = 2048,
                HashAlgorithmName = HashAlgorithmName.SHA512
            }
        );

        return certificate;
    }
}