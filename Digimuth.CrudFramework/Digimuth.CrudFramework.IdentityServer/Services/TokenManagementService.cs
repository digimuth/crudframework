﻿using System.Security.Claims;
using Digimuth.CrudFramework.IdentityServer.Configuration;
using Digimuth.CrudFramework.IdentityServer.Services.Interfaces;
using IdentityServer4;
using IdentityServer4.Validation;
using Microsoft.Extensions.DependencyInjection;

namespace Digimuth.CrudFramework.IdentityServer.Services;

public class TokenManagementService : ITokenManagementService
{
    private readonly TokenServiceConfiguration _tokenServiceConfiguration;
    private readonly IdentityServerTools _identityServerTools;
    private readonly IServiceProvider _serviceProvider;

    public TokenManagementService(TokenServiceConfiguration tokenServiceConfiguration,
        IdentityServerTools identityServerTools,
        IServiceProvider serviceProvider)
    {
        _tokenServiceConfiguration = tokenServiceConfiguration;
        _identityServerTools = identityServerTools;
        _serviceProvider = serviceProvider;
    }

    public async Task<string> AddTokenAsync(DateTimeOffset expirationDate, IEnumerable<Claim> claims)
    {
        return await _identityServerTools.IssueJwtAsync(Convert.ToInt32((expirationDate - DateTimeOffset.Now).TotalSeconds), claims);
    }

    public async Task<TokenValidationResult> ValidateTokenAsync(string tokenString)
    {
        using var scope = _serviceProvider.CreateScope();
        var tokenValidatorService = scope.ServiceProvider.GetRequiredService<ITokenValidator>();

        return await tokenValidatorService.ValidateIdentityTokenAsync(tokenString);
    }

    public bool IsExpirationDateCorrect(DateTimeOffset expirationDate)
    {
        return expirationDate >= DateTimeOffset.Now;
    }

    public bool IsTokenLiftetimeCorrect(DateTimeOffset expirationDate)
    {
        return (expirationDate - DateTimeOffset.Now).TotalDays <= _tokenServiceConfiguration.MaxTokenValidationTimeInDays;
    }
}