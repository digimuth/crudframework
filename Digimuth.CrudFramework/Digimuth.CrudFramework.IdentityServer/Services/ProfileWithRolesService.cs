﻿using Digimuth.CrudFramework.Common;
using Digimuth.CrudFramework.Common.Models;
using IdentityServer4.Models;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Identity;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Digimuth.CrudFramework.Web.Services
{
    public class ProfileWithRolesService<TApplicationUser, TApplicationUserRole> : IProfileService
        where TApplicationUser : ApplicationUserBase
        where TApplicationUserRole : Enum
    {
        protected readonly UserManager<TApplicationUser> UserManager;

        public ProfileWithRolesService(UserManager<TApplicationUser> userManager)
        {
            UserManager = userManager;
        }

        public virtual Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            var roleClaims = context.Subject.Claims.Where(c => c.Type == CrudClaimTypes.Role);
            var emailClaim = context.Subject.Claims.FirstOrDefault(c => c.Type == CrudClaimTypes.Email);

            var roleNames = roleClaims.Select(c => Enum.GetName(typeof(TApplicationUserRole), int.Parse(c.Value)));

            context.IssuedClaims.AddRange(roleNames.Select(r => new Claim("applicationUserRoles", r)));
            
            if (emailClaim != null) 
            {
                context.IssuedClaims.Add(emailClaim);
            }

            return Task.CompletedTask;
        }

        public virtual async Task IsActiveAsync(IsActiveContext context)
        {
            var user = await UserManager.GetUserAsync(context.Subject);

            context.IsActive = user is { EmailConfirmed: true };
        }
    }
}
