using IdentityServer4.Models;
using IdentityServer4.Stores;

namespace Digimuth.CrudFramework.IdentityServer.Services;

public class ValidationKeysStore : IValidationKeysStore
{
    private readonly ISigningCredentialStore _signingCredentialStore;

    public ValidationKeysStore(ISigningCredentialStore signingCredentialStore)
    {
        _signingCredentialStore = signingCredentialStore;
    }

    public async Task<IEnumerable<SecurityKeyInfo>> GetValidationKeysAsync()
    {
        var credential = await _signingCredentialStore.GetSigningCredentialsAsync();

        return new[]
        {
            new SecurityKeyInfo
            {
                Key = credential.Key,
                SigningAlgorithm = credential.Algorithm
            }
        };
    }
}