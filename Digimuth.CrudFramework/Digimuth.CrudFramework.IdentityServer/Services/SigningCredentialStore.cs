using Digimuth.CrudFramework.IdentityServer.Configuration;
using Digimuth.CrudFramework.IdentityServer.Services.Interfaces;
using IdentityServer4.Stores;
using Microsoft.IdentityModel.Tokens;

namespace Digimuth.CrudFramework.IdentityServer.Services;

public class SigningCredentialStore : ISigningCredentialStore
{
    private readonly ICertificateCreationService _certificateCreationService;
    private readonly CertificateCreationConfiguration _configuration;
    private SigningCredentials? _signingCredentials;

    public SigningCredentialStore(
        ICertificateCreationService certificateCreationService,
        CertificateCreationConfiguration configuration)
    {
        _certificateCreationService = certificateCreationService;
        _configuration = configuration;
    }

    public async Task<SigningCredentials> GetSigningCredentialsAsync()
    {
        if (_signingCredentials is not null)
        {
            return _signingCredentials;
        }

        var certificate = await _certificateCreationService.GetOrCreateSigningCertificate(
            _configuration.Path,
            _configuration.Name,
            _configuration.ValidityInYears);
        var signingAlgorithm = SecurityAlgorithms.RsaSha512;

        var key = new X509SecurityKey(certificate);
        key.KeyId += signingAlgorithm;

        return _signingCredentials = new SigningCredentials(key, signingAlgorithm);
    }
}