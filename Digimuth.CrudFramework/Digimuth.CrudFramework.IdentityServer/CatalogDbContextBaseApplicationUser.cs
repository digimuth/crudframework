using Digimuth.CrudFramework.Common.Models;
using Digimuth.CrudFramework.Storage.Extensions;
using IdentityServer4.EntityFramework.Entities;
using IdentityServer4.EntityFramework.Extensions;
using IdentityServer4.EntityFramework.Interfaces;
using IdentityServer4.EntityFramework.Options;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace Digimuth.CrudFramework.IdentityServer;

public class CatalogDbContextBase<TUser> : IdentityDbContext<TUser, IdentityRole<long>, long>, IPersistedGrantDbContext
    where TUser : ApplicationUserBase
{
    private static Lazy<SqliteConnection> _inMemorySqliteConnection = new(() =>
    {
        var connection = new SqliteConnection("Filename=:memory:");
        connection.Open();

        return connection;
    });

    private readonly IConfiguration _configuration;
    
    private readonly IOptions<OperationalStoreOptions> _operationalStoreOptions;

    public CatalogDbContextBase(
        DbContextOptions options,
        IConfiguration configuration) : base(options)
    {
        _configuration = configuration;
    }

    public CatalogDbContextBase(DbContextOptions options, IConfiguration configuration, StoreOptions operationalStoreOptions)
        : this(options, configuration)
    {
        _operationalStoreOptions = operationalStoreOptions;
    }

    public DbSet<PersistedGrant> PersistedGrants { get; set; }
    public DbSet<DeviceFlowCodes> DeviceFlowCodes { get; set; }

    public Task<int> SaveChangesAsync()
    {
        return base.SaveChangesAsync();
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        builder.RegisterSlugIndex();
        builder.RestrictCascadeDelete();
        
        var operationalStoreOptions = _operationalStoreOptions ?? this.GetService<StoreOptions>();

        builder.ConfigurePersistedGrantContext(operationalStoreOptions.Value);

        base.OnModelCreating(builder);
    }
    
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        if (!optionsBuilder.IsConfigured)
        {
            var configuration = _configuration ?? this.GetService<IConfiguration>();

            var dbConnectionString = configuration.GetConnectionString("CatalogDatabase");

            if (dbConnectionString == "InMemory")
            {
                optionsBuilder.UseSqlite(_inMemorySqliteConnection.Value);
                optionsBuilder.EnableDetailedErrors();
                optionsBuilder.EnableSensitiveDataLogging();
            }
            else
            {
                optionsBuilder.UseSqlServer(dbConnectionString);
            }
        }

        base.OnConfiguring(optionsBuilder);
    }

    protected Task MapIdNavigationCollectionsToForeignKeysAsync()
    {
        return this.MapSimpleIdsNavigationPropertiesToForeignKeysAsync();
    }

    public Task MigrateAsync()
    {
        if (Database.ProviderName == "Microsoft.EntityFrameworkCore.Sqlite")
        {
            return Database.EnsureCreatedAsync();
        }

        return Database.MigrateAsync();
    }
}

public class StoreOptions : IOptions<OperationalStoreOptions>
{
    public OperationalStoreOptions Value => _options.Value;

    public StoreOptions(IOptions<OperationalStoreOptions> options)
    {
        _options = options;
    }

    private readonly IOptions<OperationalStoreOptions> _options;
}