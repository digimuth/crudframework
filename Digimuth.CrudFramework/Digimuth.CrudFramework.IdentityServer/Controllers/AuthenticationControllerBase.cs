using System.Security.Claims;
using Digimuth.CrudFramework.BL.Dtos.Authentication;
using Digimuth.CrudFramework.Common.Configuration;
using Digimuth.CrudFramework.Common.Models;
using Digimuth.CrudFramework.IdentityServer.Enums;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Digimuth.CrudFramework.IdentityServer.Controllers;

public abstract class AuthenticationControllerBase<TApplicationUser> : Controller
    where TApplicationUser : ApplicationUserBase
{
    private readonly SignInManager<TApplicationUser> _signInManager;
    private readonly UserManager<TApplicationUser> _userManager;
    private readonly ILogger<AuthenticationControllerBase<TApplicationUser>> _logger;
    private readonly IIdentityServerInteractionService _identityServerInteractionService;
    private readonly AuthenticationConfiguration _authenticationConfiguration;

    protected virtual string AccountLockedMessage =>
        "Account locked after too many failed login attempts. Use \"Forgot Password\" mechanism to reset password and unlock account";

    protected virtual string LoginFailedMessage => "Invalid username or password";

    protected virtual bool EnableLockoutOnFailedLogin => false;

    protected AuthenticationControllerBase(
        SignInManager<TApplicationUser> signInManager,
        UserManager<TApplicationUser> userManager,
        ILogger<AuthenticationControllerBase<TApplicationUser>> logger,
        IIdentityServerInteractionService identityServerInteractionService,
        AuthenticationConfiguration authenticationConfiguration)
    {
        _logger = logger;
        _userManager = userManager;
        _signInManager = signInManager;
        _authenticationConfiguration = authenticationConfiguration;
        _identityServerInteractionService = identityServerInteractionService;
    }

    private LocalRedirectResult RedirectToError(LoginErrorCode errorCode)
    {
        return LocalRedirect($"/error?errCode={(int)errorCode}");
    }

    private bool IsUriRelative(string url)
    {
        return Uri.IsWellFormedUriString(url, UriKind.Relative);
    }

    private bool IsDomainTrusted(string domain)
    {
        return _authenticationConfiguration.RedirectUris.Any(d => new Uri(d).Host == domain);
    }

    protected abstract string GetExternalResponseUrl(string returnUrl);

    [HttpGet("login")]
    public virtual async Task<IActionResult> Get([FromQuery] string loginPageUrl = null, string returnUrl = null)
    {
        if (!_identityServerInteractionService.IsValidReturnUrl(returnUrl))
        {
            return RedirectToError(LoginErrorCode.InvalidReturnUrl);
        }

        var context = await _identityServerInteractionService.GetAuthorizationContextAsync(returnUrl);
        var scheme = context.IdP;

        if (string.IsNullOrEmpty((scheme)))
        {
            if (loginPageUrl != null)
            {
                if (IsUriRelative(loginPageUrl) || IsDomainTrusted(new Uri(loginPageUrl).Host))
                {
                    return Redirect(loginPageUrl);
                }

                return Redirect("/login");
            }

            return Redirect("/login");
        }

        var props = new AuthenticationProperties
        {
            RedirectUri = GetExternalResponseUrl(returnUrl),
            Items =
            {
                { "returnUrl", returnUrl },
                { "scheme", scheme },
                { "LoginProvider", scheme }
            }
        };

        return Challenge(props, scheme);
    }

    [HttpGet("loginCallback")]
    public virtual async Task<IActionResult> Callback(string returnUrl = null)
    {
        returnUrl ??= Url.Content("~/");

        if (!_identityServerInteractionService.IsValidReturnUrl(returnUrl))
        {
            _logger.LogWarning("Invalid return url ({0}) provided to login callback", returnUrl);

            return RedirectToError(LoginErrorCode.InvalidCallbackUrl);
        }

        var info = await _signInManager.GetExternalLoginInfoAsync();

        if (info == null)
        {
            _logger.LogWarning("Missing external login info");

            return RedirectToError(LoginErrorCode.MissingLoginInfo);
        }

        var signInResult = await _signInManager.ExternalLoginSignInAsync(info.LoginProvider, info.ProviderKey,
            isPersistent: false, bypassTwoFactor: true);

        if (signInResult.Succeeded)
        {
            _logger.LogInformation("{Name} logged in with {LoginProvider} provider.", info.Principal.Identity.Name,
                info.LoginProvider);

            return LocalRedirect(returnUrl);
        }

        if (signInResult.IsLockedOut)
        {
            // TODO: handle this case??
            return RedirectToError(LoginErrorCode.UserLockedOut);
        }

        var email = info.Principal.FindFirstValue(ClaimTypes.Email);

        var user = await _userManager.FindByEmailAsync(email);

        if (user == null)
        {
            // don't create user - users are allowed only if their accounts were created by admin (by invitation)
            return RedirectToError(LoginErrorCode.UserDoesNotExist);
        }

        var result = await _userManager.AddLoginAsync(user, info);

        if (result.Succeeded)
        {
            _logger.LogInformation("User created an account using {Name} provider.", info.LoginProvider);

            await _signInManager.SignInAsync(user, isPersistent: false, info.LoginProvider);

            return LocalRedirect(returnUrl);
        }

        foreach (var error in result.Errors)
        {
            ModelState.AddModelError(string.Empty, error.Description);
        }

        _logger.LogWarning("Login errors: {0}", string.Join(", ", result.Errors.Select(e => e.Code)));

        return RedirectToError(LoginErrorCode.UnknownError);
    }

    [HttpPost("login")]
    public virtual async Task<IActionResult> Login([FromBody] LoginDto loginDto, string returnUrl = null)
    {
        returnUrl ??= Url.Content("~/");

        if (!ModelState.IsValid)
        {
            return BadRequest(ModelState);
        }

        // This doesn't count login failures towards account lockout
        // To enable password failures to trigger account lockout, set lockoutOnFailure: true
        var result = await _signInManager.PasswordSignInAsync(
            loginDto.Email,
            loginDto.Password,
            loginDto.RememberMe,
            lockoutOnFailure: EnableLockoutOnFailedLogin);

        if (result.Succeeded)
        {
            _logger.LogInformation("User logged in.");
            return Ok(returnUrl);
        }

        if (result.IsLockedOut)
        {
            return BadRequest(AccountLockedMessage);
        }

        await Task.Delay(new Random().Next(70, 120));
        return BadRequest(LoginFailedMessage);
    }

    [HttpGet("logout")]
    public virtual async Task<IActionResult> Logout(string postLogoutUrl = null)
    {
        await _signInManager.SignOutAsync();
        await _identityServerInteractionService.RevokeTokensForCurrentSessionAsync();

        if (postLogoutUrl != null)
        {
            if (IsUriRelative(postLogoutUrl) || IsDomainTrusted(new Uri(postLogoutUrl).Host))
            {
                return Redirect(postLogoutUrl);
            }

            return Redirect("/login");
        }

        return Redirect("/login");
    }
}
