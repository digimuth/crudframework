using Digimuth.CrudFramework.Common;
using Digimuth.CrudFramework.Common.Configuration;
using Digimuth.CrudFramework.Common.Models;
using Digimuth.CrudFramework.Common.Services.Interfaces;
using Digimuth.CrudFramework.IdentityServer.Configuration;
using Digimuth.CrudFramework.IdentityServer.Services;
using Digimuth.CrudFramework.IdentityServer.Services.Interfaces;
using Digimuth.CrudFramework.IdentityServer.Validators;
using IdentityServer4.Configuration;
using IdentityServer4.EntityFramework.Interfaces;
using IdentityServer4.Models;
using IdentityServer4.Stores;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TokenServiceConfiguration = Digimuth.CrudFramework.IdentityServer.Configuration.TokenServiceConfiguration;

namespace Digimuth.CrudFramework.IdentityServer.Extensions;

public static class ServiceCollectionExtensions
{
    public static IIdentityServerBuilder AddIdentityServer<TApplicationUser, TDbContext>(
        this IServiceCollection services,
        Action<IdentityServerOptions> setupAction,
        Client client,
        Action<IdentityOptions>? identityOptionsAction = null)
        where TApplicationUser : ApplicationUserBase
        where TDbContext : DbContext, IPersistedGrantDbContext
    {
        services
            .AddScoped<StoreOptions>()
            .AddTransient<ITokenValidator, TokenValidator>()
            .AddDefaultIdentity<TApplicationUser>(options =>
            {
                options.SignIn.RequireConfirmedEmail = true;
                options.User.AllowedUserNameCharacters =
                    "ąćęłóńżźabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+";
                options.User.RequireUniqueEmail = true;
                options.Password.RequiredLength = 8;
                identityOptionsAction?.Invoke(options);
            })
            .AddRoles<IdentityRole<long>>()
            .AddEntityFrameworkStores<TDbContext>();

        return services
            .AddIdentityServer(setupAction)
            .AddApiAuthorization<TApplicationUser, TDbContext>(options =>
            {
                options.Clients.Add(client);
                var userClaims = options.ApiResources.First().UserClaims;
                userClaims.Add(CrudClaimTypes.Role);
            })
            .AddCustomTokenRequestValidator<PasswordFlowRequestValidator>();
    }

    public static IServiceCollection AddJwtAuthentication(this IServiceCollection services, string validIssuer)
    {
        services
            .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddIdentityServerJwt();

        return services.Configure<JwtBearerOptions>(JwtBearerDefaults.AuthenticationScheme, options =>
        {
            var existingOnTokenValidatedHandler = options.Events.OnTokenValidated;
            options.Events.OnTokenValidated = async context =>
            {
                await existingOnTokenValidatedHandler(context);
                // Your code to add extra configuration that will be executed after the current event implementation.
                options.TokenValidationParameters.ValidIssuers = new[]
                {
                    validIssuer
                };
            };
        });
    }

    public static IServiceCollection AddTokenManagement(
        this IServiceCollection services,
        IConfiguration configuration,
        AuthenticationConfiguration? authConfigurationObject = null,
        TokenServiceConfiguration? tokenConfigurationObject = null)
    {
        authConfigurationObject ??= configuration.GetSection("Authentication").Get<AuthenticationConfiguration>();
        tokenConfigurationObject ??= configuration.GetSection("Token").Get<TokenServiceConfiguration>();

        return services
            .AddSingleton(authConfigurationObject)
            .AddSingleton(tokenConfigurationObject)
            .AddSingleton<TokenServiceConfiguration>()
            .AddSingleton<ITokenManagementService, TokenManagementService>();
    }

    public static IServiceCollection AddIdentityServerCertificateCreationServices(
        this IServiceCollection services,
        IConfiguration configuration,
        CertificateCreationConfiguration? configurationObject = null)
    {
        configurationObject ??= configuration.GetSection("IdentityServerCertificate")
            .Get<CertificateCreationConfiguration>();

        services.Remove(services.First(s => s.ServiceType == typeof(ISigningCredentialStore)));
        services.Remove(services.First(s => s.ServiceType == typeof(IValidationKeysStore)));

        return services
            .AddCertificateManager()
            .AddSingleton(configurationObject)
            .AddSingleton<ICertificateCreationService, CertificateCreationService>()
            .AddSingleton<ISigningCredentialStore, SigningCredentialStore>()
            .AddSingleton<IValidationKeysStore, ValidationKeysStore>();
    }
}