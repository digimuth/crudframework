using System;
using Digimuth.CrudFramework.Common.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Digimuth.CrudFramework.Common.Services;

public class ServiceProviderScopingService : IServiceProviderScopingService
{
    private readonly IServiceProvider _serviceProvider;

    public ServiceProviderScopingService(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }

    public IServiceScope GetServiceProviderScopeWithOrganisationId(long id)
    {
        var scope = _serviceProvider.CreateScope();

        var currentOrganisationService =
            scope.ServiceProvider.GetRequiredService<ICurrentOrganisationService>();

        currentOrganisationService.EnforceOrganisationIdForScope(id);

        return scope;
    }
    public IServiceScope GetServiceProviderScopeWithNoOrganisation()
    {
        var scope = _serviceProvider.CreateScope();

        var currentOrganisationService =
            scope.ServiceProvider.GetRequiredService<ICurrentOrganisationService>();

        currentOrganisationService.EnforceNoOrganisationForScope();

        return scope;
    }
}