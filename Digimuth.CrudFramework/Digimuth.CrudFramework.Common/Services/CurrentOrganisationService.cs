using Digimuth.CrudFramework.Common.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Digimuth.CrudFramework.Common.Services;

public class CurrentOrganisationService : ICurrentOrganisationService
{
    private long? EnforcedOrganisationId { get; set; }

    private const string ORGANISATION_KEY = "Organisation";
    private const string TOKEN_HEADER_NAME = "jwtauthentication";
    private bool _isNoOrganisationIdEnforced = false;

    private readonly IHttpContextAccessor _httpContextAccessor;
    private readonly ILogger<CurrentOrganisationService> _logger;
    private readonly ITokenValidator _tokenValidator;

    public CurrentOrganisationService(
        IHttpContextAccessor httpContextAccessor,
        ILogger<CurrentOrganisationService> logger,
        ITokenValidator tokenValidator)
    {
        _logger = logger;
        _tokenValidator = tokenValidator;
        _httpContextAccessor = httpContextAccessor;
    }

    public long? GetCurrentOrganisationId()
    {
        if (_isNoOrganisationIdEnforced)
        {
            return null;
        }

        return EnforcedOrganisationId ?? GetOrganisationIdFromHttpContext().GetAwaiter().GetResult();   
    }

    public async Task<long?> GetCurrentOrganisationIdAsync()
    {
        if (_isNoOrganisationIdEnforced)
        {
            return null;
        }

        return EnforcedOrganisationId ?? await GetOrganisationIdFromHttpContext();
    }

    public void SetCurrentOrganisationIdInSession(long id)
    {
        _httpContextAccessor.HttpContext.Session.Set(ORGANISATION_KEY, BitConverter.GetBytes(id));
    }

    public void EnforceOrganisationIdForScope(long id)
    {
        EnforcedOrganisationId = id;
        _isNoOrganisationIdEnforced = false;
    }

    public void EnforceNoOrganisationForScope()
    {
        EnforcedOrganisationId = null;
        _isNoOrganisationIdEnforced = true;
    }

    private async Task<long?> GetOrganisationIdFromToken(HttpContext httpContext)
    {
        var token = GetTokenFromHeader(httpContext);

        if (token != null)
        {
            var tokenValidationResult = await _tokenValidator.ValidateIdentityTokenAsync(token);

            if (!tokenValidationResult.IsError)
            {
                return Convert.ToInt64(tokenValidationResult.Claims.FirstOrDefault(c => c.Type == "organisationId").Value);
            }
        }

        return null;
    }

    private async Task<long?> GetOrganisationIdFromHttpContext()
    {
        var httpContext = _httpContextAccessor.HttpContext;

        if (httpContext == null)
        {
            return null;
        }

        return await GetOrganisationIdFromToken(httpContext)
               ?? GetOrganisationIdFromRequestHeaders(httpContext)
               ?? GetOrganisationIdFromSession(httpContext);
    }

    private static long? GetOrganisationIdFromSession(HttpContext httpContext)
    {
        var sessionOrganisationId = httpContext.Session.Get(ORGANISATION_KEY);

        return sessionOrganisationId != null ? BitConverter.ToInt64(sessionOrganisationId) : null;
    }

    private string GetTokenFromHeader(HttpContext httpContext)
    {
        if (!httpContext.Request.Headers.TryGetValue(TOKEN_HEADER_NAME, out var tokenString))
        {
            return null;
        }

        return tokenString;
    }

    private long? GetOrganisationIdFromRequestHeaders(HttpContext httpContext)
    {
        if (!httpContext.Request.Headers.TryGetValue(ORGANISATION_KEY, out var headerOrganisationValue))
        {
            return null;
        }

        if (long.TryParse(headerOrganisationValue, out var headerOrganisationId))
        {
            return headerOrganisationId;
        }

        _logger.LogWarning("Value {0} passed as organisation id in header could not be parsed",
            headerOrganisationValue);

        return null;
    }
}