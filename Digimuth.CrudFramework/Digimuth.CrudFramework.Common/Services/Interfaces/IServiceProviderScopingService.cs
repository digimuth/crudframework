using Microsoft.Extensions.DependencyInjection;

namespace Digimuth.CrudFramework.Common.Services.Interfaces;

public interface IServiceProviderScopingService
{
    IServiceScope GetServiceProviderScopeWithOrganisationId(long id);
    IServiceScope GetServiceProviderScopeWithNoOrganisation();
}