﻿namespace Digimuth.CrudFramework.Common.Services.Interfaces;

public interface ICurrentLanguageService
{
    string? GetCurrentLanguage();
    string GetFallbackLanguage();
}