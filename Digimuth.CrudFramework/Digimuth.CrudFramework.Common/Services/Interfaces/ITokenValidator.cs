using System.Threading.Tasks;
using Digimuth.CrudFramework.Common.Models;

namespace Digimuth.CrudFramework.Common.Services.Interfaces;

public interface ITokenValidator
{
    Task<TokenValidationResult> ValidateIdentityTokenAsync(string token);
}