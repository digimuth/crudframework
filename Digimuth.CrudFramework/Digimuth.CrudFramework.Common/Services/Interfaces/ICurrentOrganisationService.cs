using System.Threading.Tasks;

namespace Digimuth.CrudFramework.Common.Services.Interfaces;

public interface ICurrentOrganisationService
{
    void EnforceOrganisationIdForScope(long id);

    void EnforceNoOrganisationForScope();

    long? GetCurrentOrganisationId();

    Task<long?> GetCurrentOrganisationIdAsync();

    void SetCurrentOrganisationIdInSession(long id);
}