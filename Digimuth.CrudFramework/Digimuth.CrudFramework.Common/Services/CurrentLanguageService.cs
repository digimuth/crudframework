﻿using Digimuth.CrudFramework.Common.Services.Interfaces;
using Microsoft.AspNetCore.Http;

namespace Digimuth.CrudFramework.Common.Services;

public class CurrentLanguageService : ICurrentLanguageService
{
    private readonly IHttpContextAccessor _httpContextAccessor;

    private const string LANGUAGE_HEADER_NAME = "Language";

    public CurrentLanguageService(IHttpContextAccessor httpContextAccessor)
    {
        _httpContextAccessor = httpContextAccessor;
    }

    public string GetCurrentLanguage()
    {
        return GetLanguageFromHttpContext();
    }

    public virtual string GetFallbackLanguage() => "en-GB";

    private string GetLanguageFromHttpContext()
    {
        var httpContext = _httpContextAccessor.HttpContext;

        if (httpContext == null)
        {
            return null;
        }

        return GetLanguageFromRequestHeaders(httpContext);
    }

    private string GetLanguageFromRequestHeaders(HttpContext httpContext)
    {
        if (!httpContext.Request.Headers.TryGetValue(LANGUAGE_HEADER_NAME, out var headerLanguageValue))
        {
            return null;
        }

        return headerLanguageValue;
    }
}