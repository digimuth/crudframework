using Digimuth.CrudFramework.Common.Services;
using Digimuth.CrudFramework.Common.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Digimuth.CrudFramework.Common.Extensions;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddCommonOrganisationScopingServices(this IServiceCollection services)
    {
        return services
            .AddScoped<ICurrentOrganisationService, CurrentOrganisationService>()
            .AddScoped<ICurrentLanguageService, CurrentLanguageService>()
            .AddTransient<IServiceProviderScopingService, ServiceProviderScopingService>();
    }
}