namespace Digimuth.CrudFramework.Common;

public static class CrudClaimTypes
{
    public const string Role = "role";

    public const string Email = "email";
}