using System;

namespace Digimuth.CrudFramework.Common.Configuration;

public class AuthenticationConfiguration
{
    public string InvitationUrlTemplate { get; set; }

    public Enum InvitationEmailType { get; set; }

    public string ResetPasswordUrlTemplate { get; set; }

    public Enum ResetPasswordEmailType { get; set; }

    public bool PasswordFlowEnabled { get; set; }

    public string[]? RedirectUris { get; set; }
}