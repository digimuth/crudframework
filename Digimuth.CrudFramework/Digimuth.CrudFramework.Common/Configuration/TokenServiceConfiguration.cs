﻿namespace Digimuth.CrudFramework.Common.Configuration;

public class TokenServiceConfiguration
{
    public int MaxTokenValidationTimeInDays { get; set; }
}