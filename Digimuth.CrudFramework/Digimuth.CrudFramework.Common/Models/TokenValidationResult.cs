using System.Security.Claims;

namespace Digimuth.CrudFramework.Common.Models;

public class TokenValidationResult
{
    public bool IsError { get; set; }

    public Claim[] Claims { get; set; }
}