using System;
using Microsoft.AspNetCore.Identity;

namespace Digimuth.CrudFramework.Common.Models;

public class ApplicationUserBase : IdentityUser<long>
{
    public long? CreatedById { get; set; }
    public DateTimeOffset CreatedAt { get; set; }

    public long? UpdatedById { get; set; }
    public DateTimeOffset? UpdatedAt { get; set; }

    public DateTimeOffset? InvitationLastSentAt { get; set; }
}