namespace Digimuth.CrudFramework.EmailEngine.Configuration;

public class EmailServiceConfiguration
{
    public string Host { get; set; }
    public int Port { get; set; }

    public string Username { get; set; }
    public string Password { get; set; }

    public string SenderAddress { get; set; }
    public string SenderDisplayName { get; set; }

    public string? SendGridApiKey { get; set; }
    public bool? EnableSsl { get; set; }
}