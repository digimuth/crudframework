﻿using Digimuth.CrudFramework.EmailEngine.Configuration;
using Digimuth.CrudFramework.EmailEngine.Models;
using Digimuth.CrudFramework.EmailEngine.Services.Interfaces;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Logging;
using MimeKit;
using Polly;
using Polly.Retry;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Digimuth.CrudFramework.EmailEngine.Services
{
    public class MailKitEmailService<T> : IMailKitEmailService<T> where T : Enum
    {
        private static IDictionary<T, (Func<IEmailModel, string>, Func<IEmailModel, Task<string>>)> _templateMapping =
            new Dictionary<T, (Func<IEmailModel, string>, Func<IEmailModel, Task<string>>)>();

        internal static void RegisterEmailTemplate(
            T emailType,
            Func<IEmailModel, Task<string>> renderer,
            Func<IEmailModel, string> subjectRenderer)
        {
            _templateMapping[emailType] = (subjectRenderer, renderer);
        }

        internal static void RegisterEmailTemplate(
            T emailType,
            Func<IEmailModel, Task<string>> renderer,
            string subject)
        {
            _templateMapping[emailType] = (_ => subject, renderer);
        }

        private readonly AsyncRetryPolicy _retryPolicy
            = Policy.Handle<Exception>().WaitAndRetryAsync(6, n => TimeSpan.FromSeconds(n * n));

        private string Host { get; set; }
        private int Port { get; set; }

        private string Username { get; set; }
        private string Password { get; set; }
        private bool EnableSsl { get; set; }
        private MailboxAddress Sender { get; set; }

        private readonly ILogger<MailKitEmailService<T>> _logger;

        public MailKitEmailService(EmailServiceConfiguration configuration, ILogger<MailKitEmailService<T>> logger)
        {
            _logger = logger;
            Host = configuration.Host;
            Port = configuration.Port;
            Username = configuration.Username;
            Password = configuration.Password;
            EnableSsl = configuration.EnableSsl ?? false;
            Sender = new MailboxAddress(configuration.SenderDisplayName, configuration.SenderAddress);
        }

        public async Task SendEmail(IEnumerable<MailboxAddress> recipients, T emailType, IEmailModel model, IEnumerable<MimePart> attachments = null)
        {
            foreach (var recipient in recipients)
            {
                var emailMessage = new MimeMessage();
                emailMessage.From.Add(Sender);
                emailMessage.To.Add(recipient);

                var bodyBuilder = new BodyBuilder();

                if (attachments != null)
                {
                    foreach (var attachment in attachments)
                    {
                        bodyBuilder.Attachments.Add(attachment);
                    }
                }

                await Send(await PrepareMailMessage(emailMessage, emailType, model, bodyBuilder));
            }
        }

        public async Task SendEmail(MailboxAddress recipient, T emailType, IEmailModel model, IEnumerable<MimePart> attachments = null)
        {
            var emailMessage = new MimeMessage();
            emailMessage.From.Add(Sender);
            emailMessage.To.Add(recipient);

            var bodyBuilder = new BodyBuilder();

            if (attachments != null)
            {
                foreach (var attachment in attachments)
                {
                    bodyBuilder.Attachments.Add(attachment);
                }
            }

            await Send(await PrepareMailMessage(emailMessage, emailType, model, bodyBuilder));
        }

        public async Task<string> GetRenderedEmailBody(T emailType, IEmailModel model)
        {
            var (_, renderer) = _templateMapping[emailType];
            return await renderer(model);
        }

        private async Task<MimeMessage> PrepareMailMessage(MimeMessage emailMessage, T emailType, IEmailModel model, BodyBuilder bodyBuilder)
        {
            if (!_templateMapping.ContainsKey(emailType))
            {
                throw new ArgumentException($"Template not registered for email type {emailType}");
            }

            var (subject, renderer) = _templateMapping[emailType];
            emailMessage.Subject = subject(model);

            bodyBuilder.HtmlBody = await renderer(model);
            emailMessage.Body = bodyBuilder.ToMessageBody();

            return emailMessage;
        }

        private async Task Send(MimeMessage emailMessage)
        {
            await _retryPolicy.ExecuteAsync(async () =>
            {
                try
                {
                    using var smtpClient = new SmtpClient();
                    await smtpClient.ConnectAsync(Host, Port, EnableSsl ? SecureSocketOptions.SslOnConnect : SecureSocketOptions.StartTls);

                    await smtpClient.AuthenticateAsync(Username, Password);
                    await smtpClient.SendAsync(emailMessage);
                    await smtpClient.DisconnectAsync(true);
                }
                catch (Exception e)
                {
                    _logger.LogError(e, "Error when sending email");
                    throw;
                }
            });
        }
    }
}