using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Threading.Tasks;
using Digimuth.CrudFramework.EmailEngine.Models;

namespace Digimuth.CrudFramework.EmailEngine.Services.Interfaces;

public interface IEmailService<T> where T : Enum
{
    Task SendEmail(MailAddress recipient, T emailType, IEmailModel model, IEnumerable<Attachment> attachments = null);

    Task SendEmail(IEnumerable<MailAddress> recipients, T emailType, IEmailModel model, IEnumerable<Attachment> attachments = null);

    Task<string> GetRenderedEmailBody(T emailType, IEmailModel model);
}