﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Digimuth.CrudFramework.EmailEngine.Models;
using MimeKit;

namespace Digimuth.CrudFramework.EmailEngine.Services.Interfaces
{
    public interface IMailKitEmailService<T> where T : Enum
    {
        Task SendEmail(MailboxAddress recipient, T emailType, IEmailModel model, IEnumerable<MimePart> attachments = null);

        Task SendEmail(IEnumerable<MailboxAddress> recipients, T emailType, IEmailModel model, IEnumerable<MimePart> attachments = null);

        Task<string> GetRenderedEmailBody(T emailType, IEmailModel model);
    }
}