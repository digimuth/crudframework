﻿using Digimuth.CrudFramework.EmailEngine.Configuration;
using Digimuth.CrudFramework.EmailEngine.Extensions;
using Digimuth.CrudFramework.EmailEngine.Models;
using Digimuth.CrudFramework.EmailEngine.Services.Interfaces;
using Microsoft.Extensions.Logging;
using Polly;
using Polly.Retry;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Digimuth.CrudFramework.EmailEngine.Services
{
    public class GridEmailService<T> : IEmailService<T> where T : Enum
    {
        private readonly ILogger<GridEmailService<T>> _logger;

        private SendGridClient SendGridClient { get; set; }
        private EmailAddress Sender { get; set; }

        // if any exception happens retry after 1, 4, 9, 16, 25, 36 seconds hoping that it was just temporary SMTP issue
        private readonly AsyncRetryPolicy _retryPolicy
            = Policy.Handle<Exception>().WaitAndRetryAsync(6, n => TimeSpan.FromSeconds(n * n));

        private static IDictionary<T, (Func<IEmailModel, string>, Func<IEmailModel, Task<string>>)> _templateMapping =
            new Dictionary<T, (Func<IEmailModel, string>, Func<IEmailModel, Task<string>>)>();

        internal static void RegisterEmailTemplate(
            T emailType,
            Func<IEmailModel, Task<string>> renderer,
            Func<IEmailModel, string> subjectRenderer)
        {
            _templateMapping[emailType] = (subjectRenderer, renderer);
        }

        internal static void RegisterEmailTemplate(
            T emailType,
            Func<IEmailModel, Task<string>> renderer,
            string subject)
        {
            _templateMapping[emailType] = (_ => subject, renderer);
        }

        public GridEmailService(EmailServiceConfiguration emailServiceConfiguration, ILogger<GridEmailService<T>> logger)
        {
            _logger = logger;

            Sender = new EmailAddress(emailServiceConfiguration.SenderAddress, emailServiceConfiguration.SenderDisplayName);
            SendGridClient = new SendGridClient(emailServiceConfiguration.SendGridApiKey);
        }

        public async Task SendEmail(MailAddress recipient, T emailType, IEmailModel model, IEnumerable<System.Net.Mail.Attachment> attachments = null)
        {
            if (!_templateMapping.ContainsKey(emailType))
            {
                throw new ArgumentException($"Template not registered for email type {emailType}");
            }

            var (subject, renderer) = _templateMapping[emailType];

            var message = MailHelper.CreateSingleEmail(
                    Sender, new EmailAddress(recipient.Address, recipient.DisplayName),
                    subject(model), string.Empty, await renderer(model));

            if (attachments != null)
            {
                message.AddAttachments(attachments.Select(x => x.ConvertAttachment()));
            }

            Send(message);
        }

        public async Task<string> GetRenderedEmailBody(T emailType, IEmailModel model)
        {
            var (subject, renderer) = _templateMapping[emailType];

            return await renderer(model);
        }

        public async Task SendEmail(IEnumerable<MailAddress> recipients, T emailType, IEmailModel model, IEnumerable<System.Net.Mail.Attachment> attachments = null)
        {
            if (!recipients.Any())
            {
                return;
            }

            if (!_templateMapping.ContainsKey(emailType))
            {
                throw new ArgumentException($"Template not registered for email type {emailType}");
            }

            var (subject, renderer) = _templateMapping[emailType];

            var message = MailHelper.CreateSingleEmailToMultipleRecipients(
                Sender, recipients
                    .Select(r => new EmailAddress(r.Address, r.DisplayName))
                    .ToList(),
                subject(model), string.Empty, await renderer(model));

            if (attachments != null)
            {
                message.AddAttachments(attachments.Select(x => x.ConvertAttachment()));
            }

            Send(message);
        }

        private void Send(SendGridMessage emailMessage)
        {
            _ = _retryPolicy.ExecuteAsync(async () =>
            {
                try
                {
                    var response = await SendGridClient.SendEmailAsync(emailMessage);

                    if (!response.IsSuccessStatusCode)
                    {
                        throw new Exception(
                            $"SendGrid server responded with {response.StatusCode} {await response.Body.ReadAsStringAsync()}");
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Error when sending email");

                    throw;
                }
            });
        }
    }
}
