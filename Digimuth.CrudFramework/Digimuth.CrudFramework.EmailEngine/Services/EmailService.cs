using Digimuth.CrudFramework.EmailEngine.Configuration;
using Digimuth.CrudFramework.EmailEngine.Models;
using Digimuth.CrudFramework.EmailEngine.Services.Interfaces;
using Microsoft.Extensions.Logging;
using Polly;
using Polly.Retry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Digimuth.CrudFramework.EmailEngine.Services;

public class EmailService<T> : IEmailService<T> where T : Enum
{
    private static IDictionary<T, (Func<IEmailModel, string>, Func<IEmailModel, Task<string>>)> _templateMapping =
        new Dictionary<T, (Func<IEmailModel, string>, Func<IEmailModel, Task<string>>)>();

    internal static void RegisterEmailTemplate(
        T emailType,
        Func<IEmailModel, Task<string>> renderer,
        Func<IEmailModel, string> subjectRenderer)
    {
        _templateMapping[emailType] = (subjectRenderer, renderer);
    }

    internal static void RegisterEmailTemplate(
        T emailType,
        Func<IEmailModel, Task<string>> renderer,
        string subject)
    {
        _templateMapping[emailType] = (_ => subject, renderer);
    }

    // if any exception happens retry after 1, 4, 9, 16, 25, 36 seconds hoping that it was just temporary SMTP issue
    private readonly AsyncRetryPolicy _retryPolicy
        = Policy.Handle<Exception>().WaitAndRetryAsync(6, n => TimeSpan.FromSeconds(n * n));

    private string Host { get; set; }
    private int Port { get; set; }

    private string Username { get; set; }
    private string Password { get; set; }
    private bool EnableSsl {  get; set; }
    private MailAddress Sender { get; set; }

    private readonly ILogger<EmailService<T>> _logger;

    public EmailService(EmailServiceConfiguration configuration, ILogger<EmailService<T>> logger)
    {
        _logger = logger;
        Host = configuration.Host;
        Port = configuration.Port;
        Username = configuration.Username;
        Password = configuration.Password;
        EnableSsl = configuration.EnableSsl ?? false;
        Sender = new MailAddress(configuration.SenderAddress, configuration.SenderDisplayName);
    }

    public async Task SendEmail(IEnumerable<MailAddress> recipients, T emailType, IEmailModel model, IEnumerable<Attachment> attachments = null)
    {
        var emailMessage = new MailMessage(Sender, recipients.First()) { IsBodyHtml = true };

        if (attachments != null)
        {
            foreach (var attachment in attachments)
            {
                emailMessage.Attachments.Add(attachment);
            }
        }

        Send(await PrepareMailMessage(emailMessage, emailType, model), recipients);
    }

    public async Task SendEmail(MailAddress recipient, T emailType, IEmailModel model, IEnumerable<Attachment> attachments = null)
    {
        var emailMessage = new MailMessage(Sender, recipient) { IsBodyHtml = true };

        if (attachments != null)
        {
            foreach (var attachment in attachments)
            {
                emailMessage.Attachments.Add(attachment);
            }
        }

        Send(await PrepareMailMessage(emailMessage, emailType, model));
    }

    public async Task<string> GetRenderedEmailBody(T emailType, IEmailModel model)
    {
        var (subject, renderer) = _templateMapping[emailType];

        return await renderer(model);
    }

    private async Task<MailMessage> PrepareMailMessage(MailMessage emailMessage, T emailType, IEmailModel model)
    {
        if (!_templateMapping.ContainsKey(emailType))
        {
            throw new ArgumentException($"Template not registered for email type {emailType}");
        }

        var (subject, renderer) = _templateMapping[emailType];
        emailMessage.Subject = subject(model);

        // Possible try..catch for rendering errors.
        emailMessage.Body = await renderer(model);

        return emailMessage;
    }

    private void Send(MailMessage emailMessage)
    {
        // schedule e-mail sending with retry policy on separate thread and return immediately
        _ = Task.Run(async () =>
        {
            await _retryPolicy.ExecuteAsync(async () =>
            {
                try
                {
                    using var smtpClient = new SmtpClient(Host, Port)
                    {
                        Credentials = new NetworkCredential(Username, Password),
                        EnableSsl = EnableSsl
                    };

                    await smtpClient.SendMailAsync(emailMessage);
                }
                catch (Exception e)
                {
                    _logger.LogError(e, "Error when sending email");

                    throw;
                }

            });
        });
    }

    private void Send(MailMessage emailMessage, IEnumerable<MailAddress> recipients)
    {
        // schedule e-mail sending with retry policy on separate thread and return immediately
        _ = Task.Run(async () =>
        {
            foreach (var recipient in recipients)
            {
                emailMessage.To.Clear();
                emailMessage.To.Add(recipient);

                await _retryPolicy.ExecuteAsync(async () =>
                {
                    try
                    {
                        using var smtpClient = new SmtpClient(Host, Port)
                        {
                            Credentials = new NetworkCredential(Username, Password),
                            EnableSsl = EnableSsl
                        };

                        await smtpClient.SendMailAsync(emailMessage);
                    }
                    catch (Exception e)
                    {
                        _logger.LogError(e, "Error when sending email");

                        throw;
                    }

                });
            }
        });
    }
}