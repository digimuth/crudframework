using System;
using Digimuth.CrudFramework.EmailEngine.Models;

namespace Digimuth.CrudFramework.EmailEngine.Exceptions;

public class InvalidModelException : Exception
{
    public InvalidModelException(Enum emailType, IEmailModel model)
        : base($"'{model.GetType()}' model is not a valid model for '{emailType}'") { }

    public InvalidModelException() { }

    public InvalidModelException(string message)
        : base(message) { }

    public InvalidModelException(string message, Exception innerException)
        : base(message, innerException) { }
}