﻿using System.IO;
using System;

namespace Digimuth.CrudFramework.EmailEngine.Extensions
{
    public static class AttachmentExtensions
    {
        public static SendGrid.Helpers.Mail.Attachment ConvertAttachment(this System.Net.Mail.Attachment attachment)
        {
            using var stream = new MemoryStream();
            attachment.ContentStream.CopyTo(stream);

            return new SendGrid.Helpers.Mail.Attachment
            {
                Content = Convert.ToBase64String(stream.ToArray()),
                Filename = attachment.Name,
                Type = attachment.ContentType.MediaType,
                ContentId = attachment.ContentId,
                Disposition = attachment.ContentDisposition.Inline ? "inline" : "attachment"
            };
        }
    }
}
