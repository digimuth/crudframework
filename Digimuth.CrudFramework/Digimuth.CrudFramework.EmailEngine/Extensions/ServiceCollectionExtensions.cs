using Digimuth.CrudFramework.EmailEngine.Configuration;
using Digimuth.CrudFramework.EmailEngine.Models;
using Digimuth.CrudFramework.EmailEngine.Services;
using Digimuth.CrudFramework.EmailEngine.Services.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;

namespace Digimuth.CrudFramework.EmailEngine.Extensions;
public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddEmailService<TEmailType>(
        this IServiceCollection services,
        IConfiguration configuration,
        EmailServiceConfiguration emailServiceConfiguration = null)
        where TEmailType : Enum
    {
        emailServiceConfiguration ??= configuration.GetSection("EmailServer").Get<EmailServiceConfiguration>();

        return services
            .AddSingleton(emailServiceConfiguration)
            .AddTransient<IEmailService<TEmailType>, EmailService<TEmailType>>();
    }

    public static IServiceCollection AddGridEmailService<TEmailType>(
        this IServiceCollection services,
        IConfiguration configuration,
        EmailServiceConfiguration emailServiceConfiguration = null)
        where TEmailType : Enum
    {
        emailServiceConfiguration ??= configuration.GetSection("EmailServer").Get<EmailServiceConfiguration>();

        return services
            .AddSingleton(emailServiceConfiguration)
            .AddTransient<IEmailService<TEmailType>, GridEmailService<TEmailType>>();
    }

    public static IServiceCollection AddMailKitEmailService<TEmailType>(
        this IServiceCollection services,
        IConfiguration configuration,
        EmailServiceConfiguration emailServiceConfiguration = null)
        where TEmailType : Enum
    {
        emailServiceConfiguration ??= configuration.GetSection("EmailServer").Get<EmailServiceConfiguration>();

        return services
            .AddSingleton(emailServiceConfiguration)
            .AddTransient<IMailKitEmailService<TEmailType>, MailKitEmailService<TEmailType>>();
    }

    public static IServiceCollection RegisterEmailTemplate<TEmailType>(
        this IServiceCollection services,
        TEmailType emailType,
        string subject,
        Func<IEmailModel, Task<string>> renderer)
        where TEmailType : Enum
    {
        EmailService<TEmailType>.RegisterEmailTemplate(emailType, renderer, subject);
        GridEmailService<TEmailType>.RegisterEmailTemplate(emailType, renderer, subject);
        MailKitEmailService<TEmailType>.RegisterEmailTemplate(emailType, renderer, subject);

        return services;
    }

    public static IServiceCollection RegisterEmailTemplate<TEmailType>(
        this IServiceCollection services,
        TEmailType emailType,
        Func<IEmailModel, string> subject,
        Func<IEmailModel, Task<string>> renderer)
        where TEmailType : Enum
    {
        EmailService<TEmailType>.RegisterEmailTemplate(emailType, renderer, subject);
        GridEmailService<TEmailType>.RegisterEmailTemplate(emailType, renderer, subject);
        MailKitEmailService<TEmailType>.RegisterEmailTemplate(emailType, renderer, subject);

        return services;
    }
}
