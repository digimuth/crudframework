﻿namespace Digimuth.CrudFramework.Storage.Models;

public class FileBase : EntityBase
{
    public string FileName { get; set; }
    public string ContentType { get; set; }
    public string BlobName { get; set; }
}