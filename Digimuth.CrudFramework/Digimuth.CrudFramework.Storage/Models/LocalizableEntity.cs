using System;

namespace Digimuth.CrudFramework.Storage.Models;

public class LocalizableEntity<T> : LocalizableEntityBase
{
    public T Value { get; set; }

    #region Equals

    protected bool Equals(LocalizableEntity<T> other)
    {
        return Locale == other.Locale && Value.Equals(other.Value);
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
            
        return obj.GetType() == GetType() && Equals((LocalizableEntity<T>) obj);
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(Locale, Value);
    }

    #endregion
}

public class LocalizableEntityBase
{
    public string Locale { get; set; }
}