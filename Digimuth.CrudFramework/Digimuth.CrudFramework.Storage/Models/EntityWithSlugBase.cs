namespace Digimuth.CrudFramework.Storage.Models;

public class EntityWithSlugBase : EntityBase
{
    public string Slug { get; set; }
}