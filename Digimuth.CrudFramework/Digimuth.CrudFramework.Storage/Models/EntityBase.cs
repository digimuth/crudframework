using System;

namespace Digimuth.CrudFramework.Storage.Models;

public class EntityBase : IEntityBase
{
    public long Id { get; set; }

    public DateTimeOffset CreatedAt { get; set; }

    public long CreatedById { get; set; }

    public DateTimeOffset? UpdatedAt { get; set; }

    public long? UpdatedById { get; set; }
}

public interface IEntityBase
{
    long Id { get; set; }

    DateTimeOffset CreatedAt { get; set; }

    long CreatedById { get; set; }

    DateTimeOffset? UpdatedAt { get; set; }

    long? UpdatedById { get; set; }
}