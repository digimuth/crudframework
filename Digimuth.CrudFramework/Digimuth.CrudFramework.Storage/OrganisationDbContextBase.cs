using Digimuth.CrudFramework.Common.Services.Interfaces;
using Digimuth.CrudFramework.Storage.Extensions;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Digimuth.CrudFramework.Storage;

public class OrganisationDbContextBase : DbContext
{
    public long OrganisationId { get; private set; }

    private readonly IConfiguration _configuration;
    private readonly ICurrentLanguageService _currentLanguageService;

    public OrganisationDbContextBase(
        DbContextOptions options,
        IConfiguration configuration,
        ICurrentOrganisationService currentOrganisationService,
        ICurrentLanguageService currentLanguageService)
        : base(options)
    {
        _configuration = configuration;
        _currentLanguageService = currentLanguageService;

        OrganisationId = currentOrganisationService.GetCurrentOrganisationId()
                         ?? throw new NotSupportedException("Can't access organisation-specific database without organisation specified in user session.");
    }

    private static readonly IDictionary<long?, SqliteConnection> _organisationConnections
        = new Dictionary<long?, SqliteConnection>();

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        var connectionString = _configuration.GetConnectionString("OrganisationDatabase");

        if (!optionsBuilder.IsConfigured && connectionString == "InMemory")
        {
            lock (_organisationConnections)
            {
                if (!_organisationConnections.ContainsKey(OrganisationId))
                {
                    var inMemoryConnection = new SqliteConnection("Filename=:memory:");
                    inMemoryConnection.Open();
                    _organisationConnections[OrganisationId] = inMemoryConnection;
                }
            }

            optionsBuilder.UseSqlite(_organisationConnections[OrganisationId]);
            optionsBuilder.EnableDetailedErrors();
            optionsBuilder.EnableSensitiveDataLogging();
        }
        else if (!optionsBuilder.IsConfigured)
        {
            var dbConnectionString = string.Format(connectionString, OrganisationId);

            optionsBuilder
                .UseSqlServer(dbConnectionString,
                    sqlServerOptions => sqlServerOptions.CommandTimeout(120));
        }

        base.OnConfiguring(optionsBuilder);
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        // TODO figure out and add elastic pool support
        builder.RegisterSlugIndex();
        builder.RestrictCascadeDelete();
        builder.RegisterLocalizableEntities();
            
        base.OnModelCreating(builder);
    }

    protected Task MapIdNavigationCollectionsToForeignKeysAsync()
    {
        return this.MapSimpleIdsNavigationPropertiesToForeignKeysAsync();
    }

    public Task MigrateAsync()
    {
        if (Database.ProviderName == "Microsoft.EntityFrameworkCore.Sqlite")
        {
            return Database.EnsureCreatedAsync();
        }

        return Database.MigrateAsync();
    }
}