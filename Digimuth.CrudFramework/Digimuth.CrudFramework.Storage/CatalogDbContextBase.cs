using System;
using Digimuth.CrudFramework.Storage.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace Digimuth.CrudFramework.Storage;

public class CatalogDbContextBase : DbContext
{
    private static Lazy<SqliteConnection> _inMemorySqliteConnection = new(() =>
    {
        var connection = new SqliteConnection("Filename=:memory:");
        connection.Open();

        return connection;
    });

    private readonly IConfiguration _configuration;

    public CatalogDbContextBase(
        DbContextOptions options,
        IConfiguration configuration)
        : base(options)
    {
        _configuration = configuration;
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        if (!optionsBuilder.IsConfigured)
        {
            var configuration = _configuration ?? this.GetService<IConfiguration>();

            var dbConnectionString = configuration.GetConnectionString("CatalogDatabase");

            if (dbConnectionString == "InMemory")
            {
                optionsBuilder.UseSqlite(_inMemorySqliteConnection.Value);
                optionsBuilder.EnableDetailedErrors();
                optionsBuilder.EnableSensitiveDataLogging();
            }
            else
            {
                optionsBuilder.UseSqlServer(dbConnectionString);
            }
        }

        base.OnConfiguring(optionsBuilder);
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        // TODO figure out and add elastic pool support

        builder.RegisterSlugIndex();
        builder.RestrictCascadeDelete();
        builder.RegisterLocalizableEntities();

        base.OnModelCreating(builder);
    }

    protected Task MapIdNavigationCollectionsToForeignKeysAsync()
    {
        return this.MapSimpleIdsNavigationPropertiesToForeignKeysAsync();
    }

    public Task<int> SaveChangesAsync()
    {
        return base.SaveChangesAsync();
    }

    public Task MigrateAsync()
    {
        if (Database.ProviderName == "Microsoft.EntityFrameworkCore.Sqlite")
        {
            return Database.EnsureCreatedAsync();
        }

        return Database.MigrateAsync();
    }
}