using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Digimuth.CrudFramework.Storage.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Digimuth.CrudFramework.Storage.Extensions;

public static class SeedExtensions
{
    public static void Seed(this DbContext dbContext, IConfiguration configuration)
    {
        foreach (var seedData in configuration.GetSection("SeedData").GetChildren())
        {
            var dbTable = dbContext.GetType().GetProperty(seedData["Table"]);

            if (dbTable == null)
            {
                continue; // skip for now, this entity is probably in different DbContext
            }

            var entityType = dbTable.PropertyType.GetGenericArguments()[0];
            var listType = typeof(List<>).MakeGenericType(entityType);
            var items = seedData.GetSection("Data").Get(listType);
            var addMethod = dbTable.PropertyType.GetMethod("Add");

            foreach (EntityBase entity in (IEnumerable) items)
            {
                var existingEntity = (dbTable.GetValue(dbContext) as IEnumerable)
                    .Cast<EntityBase>()
                    .FirstOrDefault(e => e.Id == entity.Id);

                if (existingEntity is null)
                {
                    entity.Id = 0;
                    addMethod.Invoke(dbTable.GetValue(dbContext), new object[] {entity});
                }
            }

            dbContext.MapSimpleIdsNavigationPropertiesToForeignKeysAsync().GetAwaiter().GetResult();
            dbContext.SaveChanges();
        }
    }
}