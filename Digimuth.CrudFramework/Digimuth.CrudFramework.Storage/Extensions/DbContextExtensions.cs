using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Digimuth.CrudFramework.Storage.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Digimuth.CrudFramework.Storage.Extensions;

public static class DbContextExtensions
{
    private static readonly ConcurrentDictionary<IEntityType, List<(PropertyInfo, PropertyInfo)>> _cache
        = new();

    public static async Task MapSimpleIdsNavigationPropertiesToForeignKeysAsync(this DbContext dbContext)
    {
        var entries = dbContext.ChangeTracker.Entries()
            .Where(e => e.State != EntityState.Unchanged && e.State != EntityState.Detached).ToList();
            
        foreach (var entry in entries)
        {
            var entity = entry.Entity;

            var getSourceAndTargetProperties = _cache.GetOrAdd(entry.Metadata,
                _ => GetSimpleAndFullNavigationPropertiesForEntry(entry));

            foreach (var (source, target) in getSourceAndTargetProperties)
            {
                await MapValuesFromSourcePropertyToTarget(dbContext, source, entity, target);
            }
        }
    }

    private static async Task MapValuesFromSourcePropertyToTarget(DbContext dbContext, PropertyInfo source, object entity, PropertyInfo target)
    {
        if (!(source.GetValue(entity) is ICollection<long> sourceValue))
        {
            return;
        }
            
        // preload navigation property in case of many to many
        // to avoid accidental duplicate in join table
        foreach (var manyToMany in
                 dbContext.Entry(entity).Collections
                     .Where(c => c.Metadata.PropertyInfo == target)
                     .Where(c => c.Metadata.IsCollection && c.Metadata.Inverse?.IsCollection == true))
        {
            await manyToMany.LoadAsync();
        }

        var targetMemberType = target.PropertyType.GetGenericArguments()[0];

        var listType = typeof(List<>);
        var constructedListType = listType.MakeGenericType(targetMemberType);

        var newValue = (IList) Activator.CreateInstance(constructedListType);
            
        foreach (var i in sourceValue)
        {
            object targetObject;

            var trackedTarget = dbContext.ChangeTracker.Entries()
                .FirstOrDefault(e => e.Entity.GetType() == targetMemberType
                                     && e.Property("Id").CurrentValue.Equals(i));

            if (trackedTarget == null) // omit already tracked entities
            {
                targetObject = Activator.CreateInstance(targetMemberType);

                if (targetObject.GetType().IsSubclassOf(typeof(EntityBase)))
                {
                    ((EntityBase)targetObject).Id = i;
                }
                else if (targetObject.GetType().IsSubclassOf(typeof(IdentityUser<long>)))
                {
                    ((IdentityUser<long>)targetObject).Id = i;
                }
                else if (typeof(IEntityBase).IsAssignableFrom(targetObject.GetType()))
                {
                    ((IEntityBase)targetObject).Id = i;
                }
                else
                {
                    throw new InvalidCastException("Unrecognized base class");
                }

                dbContext.Attach(targetObject);
                dbContext.Entry(targetObject).State = EntityState.Unchanged;
            }
            else
            {
                targetObject = trackedTarget.Entity;
            }

            newValue.Add(targetObject);
        }

        target.SetValue(entity, newValue);
    }

    private static List<(PropertyInfo, PropertyInfo)> GetSimpleAndFullNavigationPropertiesForEntry(
        EntityEntry entry)
    {
        var entityType = entry.Entity.GetType();
        var entityProperties = entityType.GetProperties();
                        
        return
            entityProperties.Where(p =>
                    p.PropertyType == typeof(ICollection<long>)
                    && p.Name.EndsWith("Ids")
                    && Attribute.IsDefined(p, typeof(NotMappedAttribute)))
                .Select(p => (p, entityProperties.SingleOrDefault(ep => ep.Name == p.Name[0..^3])))
                .Where(t => t.Item2 != null)
                .ToList();
    }
}