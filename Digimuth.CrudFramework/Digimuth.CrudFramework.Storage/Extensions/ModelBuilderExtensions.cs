using Digimuth.CrudFramework.Storage.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace Digimuth.CrudFramework.Storage.Extensions;

public static class ModelBuilderExtensions
{
    public static void RestrictCascadeDelete(this ModelBuilder builder)
    {
        foreach (var relationship in builder.Model.GetEntityTypes()
                     .SelectMany(e => e.GetForeignKeys().Where(k => !k.IsOwnership)))
        {
            relationship.DeleteBehavior = DeleteBehavior.Restrict;
        }
    }

    public static void RegisterSlugIndex(this ModelBuilder builder)
    {
        var slugTypes = builder.Model.GetEntityTypes().Select(t => t.ClrType)
            .Where(t => t.IsClass && t.IsSubclassOf(typeof(EntityWithSlugBase)));

        foreach (var type in slugTypes)
        {
            builder.Entity(type)
                .HasIndex(nameof(EntityWithSlugBase.Slug))
                .IsUnique(true);
        }
    }

    public static void RegisterLocalizableEntities(this ModelBuilder builder)
    {
        foreach (var navigation in GetLocalizableStringNavigations(builder))
        {
            builder.Entity(navigation.DeclaringEntityType.ClrType)
                .OwnsMany(navigation.TargetEntityType.ClrType, navigation.Name,
                    a =>
                    {
                        a.WithOwner().HasForeignKey("ParentId");
                        a.HasKey("ParentId", nameof(LocalizableString.Locale));
                        a.ToTable(
                            navigation.DeclaringEntityType.GetTableName() + navigation.Name +
                            nameof(LocalizableString) + "s",
                            "strings");
                    });

            navigation.ForeignKey.DeleteBehavior = DeleteBehavior.Cascade;
        }
    }

    private static IEnumerable<IMutableNavigation> GetLocalizableStringNavigations(ModelBuilder builder)
    {
        return builder.Model.GetEntityTypes().SelectMany(e =>
            e.GetNavigations()
                .Where(IsNavigationCollectionOfLocalizableEntity)).ToList();
    }

    private static bool IsNavigationCollectionOfLocalizableEntity(IMutableNavigation n)
    {
        var propertyInfo = n.PropertyInfo;
        var propertyType = propertyInfo.PropertyType;

        // Throw an exception if the property is directly a LocalizableString or a generic LocalizableEntity, as these must be part of a collection.
        if (propertyType == typeof(LocalizableString) || (propertyType.IsGenericType && propertyType.GetGenericTypeDefinition() == typeof(LocalizableEntity<>)))
        {
            throw new InvalidOperationException($"Property '{propertyInfo.Name}' in '{n.DeclaringEntityType.Name}' must be used as part of an ICollection<>.");
        }

        if (!typeof(IEnumerable).IsAssignableFrom(propertyType))
        {
            return false;
        }

        if (propertyType.IsGenericType && propertyType.GetGenericTypeDefinition() == typeof(ICollection<>))
        {
            var genericArgument = propertyType.GetGenericArguments()[0];
            Type baseType = genericArgument;

            //Look up the inheritance chain to check if any base type is a generic LocalizableEntity<>, returning true if found
            while (baseType != null && baseType != typeof(object))
            {
                if (baseType.IsGenericType && baseType.GetGenericTypeDefinition() == typeof(LocalizableEntity<>))
                {
                    return true;
                }
                baseType = baseType.BaseType;
            }
        }

        return false;
    }
}