namespace Digimuth.CrudFramework.Cloudflare.Configuration;

public class CloudflareConfiguration
{
    public string AccountId { get; set; }

    public string Token { get; set; }
}