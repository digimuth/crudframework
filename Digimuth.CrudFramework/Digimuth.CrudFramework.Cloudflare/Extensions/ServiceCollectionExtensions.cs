using Digimuth.CrudFramework.Cloudflare.Configuration;
using Digimuth.CrudFramework.Cloudflare.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Digimuth.CrudFramework.Cloudflare.Extensions;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddCloudflareIntegration(
        this IServiceCollection services,
        CloudflareConfiguration configuration)
    {
        return services
            .AddScoped<ICloudflareService, CloudflareService>()
            .AddSingleton(configuration);
    }
}