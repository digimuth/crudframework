using Digimuth.CrudFramework.Cloudflare.Configuration;
using Digimuth.CrudFramework.Cloudflare.Models;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace Digimuth.CrudFramework.Cloudflare.Services;

public class CloudflareService : ICloudflareService
{
    private readonly CloudflareConfiguration _configuration;
    private readonly HttpClient _client;

    public CloudflareService(CloudflareConfiguration configuration, IHttpClientFactory clientFactory)
    {
        _configuration = configuration;
        _client = clientFactory.CreateClient();
        _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _configuration.Token);
    }

    public async Task<CloudflarePagesProject[]> GetPagesProjectsAsync()
    {
        var url =
            $"https://api.cloudflare.com/client/v4/accounts/{_configuration.AccountId}/pages/projects";

        var result = await _client.GetFromJsonAsync<CloudflareResult<CloudflarePagesProject[]>>(url);

        if (!result.Success)
        {
            throw new Exception("Response did not indicate success");
        }

        return result.Result;
    }

    public async Task<CloudflarePagesProject> GetPagesProjectByNameAsync(string projectName)
    {
        var url =
             $"https://api.cloudflare.com/client/v4/accounts/{_configuration.AccountId}/pages/projects/{projectName}";

        var result = await _client.GetFromJsonAsync<CloudflareResult<CloudflarePagesProject>>(url);

        if (!result.Success)
        {
            throw new Exception("Response did not indicate success");
        }

        return result.Result;
    }

    public async Task<CloudflarePagesDeployment> StartProductionDeploymentAsync(string projectName)
    {
        var url
            = $"https://api.cloudflare.com/client/v4/accounts/{_configuration.AccountId}/pages/projects/{projectName}/deployments";

        var response = await _client.PostAsync(url, null);
        response.EnsureSuccessStatusCode();

        var result = await response.Content.ReadFromJsonAsync<CloudflareResult<CloudflarePagesDeployment>>();

        if (!result.Success)
        {
            throw new Exception("Response did not indicate success");
        }

        return result.Result;
    }
}