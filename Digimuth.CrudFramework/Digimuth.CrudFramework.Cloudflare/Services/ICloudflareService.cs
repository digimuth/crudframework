using System.Threading.Tasks;
using Digimuth.CrudFramework.Cloudflare.Models;

namespace Digimuth.CrudFramework.Cloudflare.Services;

public interface ICloudflareService
{
    Task<CloudflarePagesProject[]> GetPagesProjectsAsync();

    Task<CloudflarePagesProject> GetPagesProjectByNameAsync(string projectName);

    Task<CloudflarePagesDeployment> StartProductionDeploymentAsync(string projectName);
}