namespace Digimuth.CrudFramework.Cloudflare.Models;

internal class CloudflareResult<T>
{
    public T Result { get; set; }

    public bool Success { get; set; }

    public string[] Errors { get; set; }

    public string[] Messages { get; set; }
}