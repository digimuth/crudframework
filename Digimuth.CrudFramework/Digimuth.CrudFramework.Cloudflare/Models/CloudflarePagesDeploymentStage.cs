using System;
using System.Text.Json.Serialization;

namespace Digimuth.CrudFramework.Cloudflare.Models;

public class CloudflarePagesDeploymentStage
{
    public string Name { get; set; }

    [JsonPropertyName("started_on")]
    public DateTimeOffset? StartedOn { get; set; }

    [JsonPropertyName("ended_on")]
    public DateTimeOffset? EndedOn { get; set; }

    public string Status { get; set; }
}