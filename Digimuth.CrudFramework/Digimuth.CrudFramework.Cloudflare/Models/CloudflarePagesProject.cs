using System;
using System.Text.Json.Serialization;

namespace Digimuth.CrudFramework.Cloudflare.Models;

public class CloudflarePagesProject
{
    public Guid Id { get; set; }

    public string Name { get; set; }

    public string Subdomain { get; set; }

    public string[] Domains { get; set; }

    [JsonPropertyName("latest_deployment")]
    public CloudflarePagesDeployment Deployment { get; set; }
}