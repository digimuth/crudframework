using System;
using System.Text.Json.Serialization;

namespace Digimuth.CrudFramework.Cloudflare.Models;

public class CloudflarePagesDeployment
{
    public Guid Id { get; set; }

    [JsonPropertyName("project_id")]
    public Guid ProjectId { get; set; }

    [JsonPropertyName("created_on")]
    public DateTimeOffset CreatedOn { get; set; }

    [JsonPropertyName("modified_on")]
    public DateTimeOffset? ModifiedOn { get; set; }

    [JsonPropertyName("project_name")]
    public string ProjectName { get; set; }

    public string Environment { get; set; }

    [JsonPropertyName("url")]
    public string PreviewUrl { get; set; }

    public CloudflarePagesDeploymentStage[] Stages { get; set; }

    [JsonPropertyName("is_skipped")]
    public bool IsSkipped { get; set; }
}