using System;
using System.IO;
using System.Threading.Tasks;
using Digimuth.CrudFramework.Documents.Models;

namespace Digimuth.CrudFramework.Documents.Services.Interfaces;

public interface IDocumentService<in TEnum> where TEnum : Enum
{
    Task<Stream> GenerateDocumentAsync(TEnum type, IDocumentModel model);
}