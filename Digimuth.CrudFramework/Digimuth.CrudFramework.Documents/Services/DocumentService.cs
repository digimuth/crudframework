using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using ClosedXML.Report;
using Digimuth.CrudFramework.Documents.Models;
using Digimuth.CrudFramework.Documents.Services.Interfaces;
using DocumentFormat.OpenXml.Packaging;
using Text = DocumentFormat.OpenXml.Wordprocessing.Text;

namespace Digimuth.CrudFramework.Documents.Services;

internal class DocumentService<T> : IDocumentService<T> where T : Enum
{
    private static readonly IDictionary<T, (string, Type, Assembly)> _templateMapping =
        new Dictionary<T, (string, Type, Assembly)>();

    internal static void RegisterDocumentTemplate<TModel>(T emailType, string templateResource, Assembly assembly)
        where TModel : IDocumentModel
    {
        if (_templateMapping.ContainsKey(emailType))
        {
            throw new ArgumentException($"Document template for {emailType} is already mapped");
        }

        _templateMapping[emailType] = (templateResource, typeof(TModel), assembly);
    }

    public async Task<Stream> GenerateDocumentAsync(T type, IDocumentModel model)
    {
        if (!_templateMapping.ContainsKey(type))
        {
            throw new ArgumentException($"Template not registered for document type {type}");
        }

        var (templateLocation, modelType, assembly) = _templateMapping[type];

        if (!model.GetType().IsAssignableFrom(modelType))
        {
            throw new ArgumentException($"'{model.GetType()}' model is not a valid model for '{type}'");
        }

        await using var resourceStream = assembly
                                             .GetManifestResourceStream(templateLocation)
                                         ?? throw new Exception($"Template {templateLocation} could not be read");

        var memoryStream = new MemoryStream();
        await resourceStream.CopyToAsync(memoryStream);

        if (templateLocation.EndsWith(".docx"))
        {
            GenerateWordDocument(memoryStream, model);
        }
        else if (templateLocation.EndsWith(".xlsx"))
        {
            GenerateExcelDocument(memoryStream, model);
        }
        else
        {
            throw new Exception("Unrecognized template format");
        }

        memoryStream.Position = 0;

        return memoryStream;
    }

    private static void GenerateExcelDocument(Stream stream, IDocumentModel model)
    {
        using var file = new XLTemplate(stream);

        file.AddVariable(model);

        file.Generate();

        file.SaveAs(stream);
    }

    private static void GenerateWordDocument(Stream stream, IDocumentModel model)
    {
        using var file = WordprocessingDocument.Open(stream, true);

        var document = file.MainDocumentPart.Document;

        foreach (var text in document.Descendants<Text>())
        {
            foreach (var property in model.GetType().GetProperties())
            {
                text.Text = text.Text.Replace("{{" + property.Name + "}}", property.GetValue(model)!.ToString());
            }
        }

        file.Save();
    }
}