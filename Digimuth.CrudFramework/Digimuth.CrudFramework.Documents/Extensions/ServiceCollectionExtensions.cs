using System;
using System.Reflection;
using Digimuth.CrudFramework.Documents.Models;
using Digimuth.CrudFramework.Documents.Services;
using Digimuth.CrudFramework.Documents.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Digimuth.CrudFramework.Documents.Extensions;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddDocumentService<TDocumentType>(
        this IServiceCollection services)
        where TDocumentType : Enum
    {
        return services
            .AddTransient<IDocumentService<TDocumentType>, DocumentService<TDocumentType>>();
    }

    public static IServiceCollection RegisterDocumentTemplate<TDocumentType, TDocumentModel>(
        this IServiceCollection services,
        TDocumentType emailType,
        string resourceLocation,
        Assembly assembly)
        where TDocumentType : Enum
        where TDocumentModel : IDocumentModel
    {
        DocumentService<TDocumentType>.RegisterDocumentTemplate<TDocumentModel>(emailType, resourceLocation, assembly);

        return services;
    }
}