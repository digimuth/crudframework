using System;
using System.Linq;
using System.Reflection;
using AutoMapper;
using AutoMapper.Configuration;
using AutoMapper.Internal;
using Digimuth.CrudFramework.Web.Controllers;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Xunit;

namespace Digimuth.CrudFramework.Tests.Common.Helpers;

public static class AutomapperTestsHelper
{
    public static void ValidateIfAllDtoPropertiesHaveValidMappings(IServiceProvider serviceProvider)
    {
        var mapper = serviceProvider.GetService<IMapper>();
        var configurationExpression = serviceProvider.GetService<IOptions<MapperConfigurationExpression>>();
        var globalConfiguration = mapper.ConfigurationProvider.Internal();

        var validator = new ConfigurationValidator(configurationExpression.Value);

        var typeMaps = globalConfiguration.GetAllTypeMaps();
        var fromDbToDtoMappings = typeMaps.Where(m => m.DestinationType.Name.EndsWith("Dto"));

        // check if all (except for ignored) properties on dtos can be mapped from db entities
        validator.AssertConfigurationExpressionIsValid(globalConfiguration, fromDbToDtoMappings);

        var fromDtoToDbMappings = typeMaps.Where(m => !m.DestinationType.Name.EndsWith("Dto"));

        foreach (var mapping in fromDtoToDbMappings)
        {
            mapping.ConfiguredMemberList = MemberList.Source;
            // check if all dto properties can be successfully mapped to db entities
            // (ignoring the ones on the destination type that are not mapped - mostly on purpose like UpdateAt or similar)
            validator.AssertConfigurationExpressionIsValid(globalConfiguration, new[] { mapping });
        }
    }

    public static void ValidateIfAllDtosUsedInCrudControllersHaveTypeMaps<TWebProjectType>(IServiceProvider serviceProvider)
    {
        var controllers = Assembly.GetAssembly(typeof(TWebProjectType)).GetTypes().Where(t =>
            t.BaseType.IsGenericType
            && t.BaseType.GetGenericTypeDefinition().IsAssignableTo(typeof(CrudControllerBase<,,,>)
                .GetGenericTypeDefinition()));

        var mapper = serviceProvider.GetService<IMapper>();
        var typeMaps = mapper.ConfigurationProvider.Internal().GetAllTypeMaps();

        foreach (var controller in controllers)
        {
            var dtoTypes = controller.BaseType.GetGenericArguments().ToArray();

            var destinationDtos = new[] {dtoTypes[0], dtoTypes[1]};

            foreach (var destinationDtoType in destinationDtos)
            {
                Assert.True(typeMaps.Any(m => m.DestinationType == destinationDtoType),
                    $"{destinationDtoType.Name} is used as destination dto in {controller.Name} but is missing mapping");
            }

            var sourceDtos = new[] {dtoTypes[2], dtoTypes[3]};

            foreach (var sourceDtoType in sourceDtos)
            {
                Assert.True(typeMaps.Any(m => m.SourceType == sourceDtoType),
                    $"{sourceDtoType.Name} is used as source dto in {controller.Name} but is missing mapping");
            }
        }
    }
}