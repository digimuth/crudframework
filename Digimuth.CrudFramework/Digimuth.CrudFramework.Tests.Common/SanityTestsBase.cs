﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Digimuth.CrudFramework.BL.Dtos.ApplicationUser;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace Digimuth.CrudFramework.Tests.Common;

public abstract class SanityTestsBase<TStartup> : IDisposable where TStartup : class
{
    protected JsonSerializerOptions DefaultSerializerOptions = new()
    {
        Converters = {new JsonStringEnumConverter()}
    };

    protected HttpClient Client;
    protected TestServer Server;
    protected IConfiguration Configuration;
    protected WebApplicationFactory<TStartup> WebAppFactory;

    protected SanityTestsBase()
    {
        WebAppFactory = new WebApplicationFactory<TStartup>()
            .WithWebHostBuilder(builder => builder
                .UseEnvironment(Environments.Development)
                .ConfigureAppConfiguration((context, configBuilder) =>
                {
                    configBuilder
                        .AddJsonFile("appsettings.json")
                        .AddJsonFile("appsettings.IntegrationTests.json");

                    Configuration = configBuilder.Build();
                })
                .UseUrls("https://localhost:5001")
            );

        Server = WebAppFactory.Server;

        Client = WebAppFactory.CreateClient(new WebApplicationFactoryClientOptions
        {
            BaseAddress = new Uri("https://localhost:5001") // required for Session cookie to be set (https)
        });
    }

    protected async Task LoginAsAdmin(ApplicationUserRegisterBaseDto user = null)
    {
        user ??= Configuration.GetSection("InitialUsers").Get<List<ApplicationUserRegisterBaseDto>>().First();

        var content = new FormUrlEncodedContent(new Dictionary<string, string>
        {
            ["client_id"] = "Apptimise.Cloudpeek.Web",
            ["grant_type"] = "password",
            ["username"] = user.UserName,
            ["password"] = user.Password
        });
        var response = await Client.PostAsync("/connect/token", content);

        var responseDictionary = await response.Content.ReadFromJsonAsync<Dictionary<string, object>>();

        var accessToken = responseDictionary["access_token"].ToString();

        Client.DefaultRequestHeaders.Add("Authorization", $"Bearer {accessToken}");
    }

    protected virtual void Dispose(bool disposing)
    {
        if (disposing)
        {
            Client?.Dispose();
            Server?.Dispose();
            WebAppFactory?.Dispose();
        }
    }

    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }
}