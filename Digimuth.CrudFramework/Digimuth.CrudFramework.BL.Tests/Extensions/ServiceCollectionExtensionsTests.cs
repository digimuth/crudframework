﻿using NUnit.Framework;
using System;
using Digimuth.CrudFramework.BL.Extensions;

namespace Digimuth.CrudFramework.BLTests.Extensions;

[TestFixture]
public class ServiceCollectionExtensionsTests
{
    [Test]
    [TestCase(typeof(string), false)]
    [TestCase(typeof(int), false)]
    // [TestCase(typeof(IEnumerable<LocalizableStringDto>), true)] TODO: Add support for IEnumerable<LocalizableStringDto>
    // [TestCase(typeof(IEnumerable<LocalizableEntityDto<string>>), true)] TODO: Add support for IEnumerable<LocalizableEntityDto<string>>
    public void IsDestinationTypeLocalizableEntityEnumerableTest(Type type, bool expectedResult)
    {
        var result = type.IsDestinationTypeLocalizableEntityEnumerable();

        Assert.AreEqual(expectedResult, result);
    }
}