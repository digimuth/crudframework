using Digimuth.CrudFramework.BL.Attributes;
using Digimuth.CrudFramework.BL.Dtos;
using NUnit.Framework;

namespace Digimuth.CrudFramework.BLTests.Attributes;

[TestFixture]
public class RichtextAttributeTests
{
    [Test]
    [TestCase("<h1>Hello</h1>This is safe html", true)]
    [TestCase("<script>console.log('hello this is virus')</script>This is unsafe html", false)]
    [TestCase("<a onclick=\"console.log('virus')\">click here please :)</a>This is unsafe html", false)]
    [TestCase("<h2><strong>No witam witam</strong></h2>\n<p><strong>😀</strong></p>\n<p><em>Tutaj tekst</em></p>", true)]
    public void ShouldValidateIncomingHtmlStringWithSanitizer(string input, bool expected)
    {
        var attribute = new RichtextAttribute();

        var result = attribute.IsValid(input);

        Assert.AreEqual(expected, result);
    }

    [Test]
    public void ShouldValidateTrueIncomingLocalizableHtmlWithSanitizer()
    {
        var input = new[]
        {
            new LocalizableStringDto
            {
                Locale = "pl",
                Value =
                    "<h2><strong>No witam witam</strong></h2>\n<p><strong>😀</strong></p>\n<p><em>Tutaj tekst</em></p>"
            },
            new LocalizableStringDto
            {
                Locale = "en",
                Value =
                    "<h2><strong>Welcome</strong></h2>\n<p><strong>😀</strong></p>\n<p><em>This is text</em></p>"
            }
        };
        var attribute = new RichtextAttribute();

        var result = attribute.IsValid(input);

        Assert.AreEqual(true, result);
    }

    [Test]
    public void ShouldValidateFalseIncomingLocalizableHtmlWithSanitizer()
    {
        var input = new[]
        {
            new LocalizableStringDto
            {
                Locale = "pl",
                Value =
                    "<h2><strong>No witam witam</strong></h2>\n<p><strong>😀</strong></p>\n<p><em>Tutaj tekst</em></p>"
            },
            new LocalizableStringDto
            {
                Locale = "en",
                Value =
                    "<h2><strong>Welcome</strong></h2>\n<p><strong>😀</strong></p>\n<p><em><script>console.log('gotcha')</script>This is text</em></p>"
            }
        };
        var attribute = new RichtextAttribute();

        var result = attribute.IsValid(input);

        Assert.AreEqual(false, result);
    }
}