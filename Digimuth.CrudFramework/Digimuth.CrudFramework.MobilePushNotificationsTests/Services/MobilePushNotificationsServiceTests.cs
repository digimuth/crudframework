﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Digimuth.CrudFramework.MobilePushNotifications.Extensions;
using Expo.Server.Models;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;
using Xunit;

namespace Digimuth.CrudFramework.MobilePushNotifications.Services.Tests
{
    public class MobilePushNotificationsServiceTests : IDisposable
    {
        private readonly ServiceProvider _serviceProvider;

        private readonly IMobilePushNotificationsService<Dictionary<string, PushTicketDeliveryStatus>,
            PushTicketRequest, string> _mobilePushNotificationsService;

        public MobilePushNotificationsServiceTests()
        {
            _serviceProvider = new ServiceCollection()
                .AddLogging(builder => builder.AddConsole())
                .AddExpoMobilePushNotifications()
                .BuildServiceProvider();

            _mobilePushNotificationsService = _serviceProvider
                .GetRequiredService<IMobilePushNotificationsService<Dictionary<string, PushTicketDeliveryStatus>,
                    PushTicketRequest, string>>();
        }

        [Fact]
        public async Task ShouldSendNotificationToFewRecipients()
        {
            var recipients = new List<string>
                { "ExponentPushToken[-Ok_6WApJgTWIZm2wZTdaN]", "ExponentPushToken[-Ok_6WApJgTWIZm2wZTdaN]" };

            var result = await _mobilePushNotificationsService
                .SendNotification(recipients, new PushTicketRequest()
                {
                    PushBadgeCount = 7,
                    PushBody = "Test Push - Msg",
                    PushSound = "default"
                });

            Assert.DoesNotContain(result.Values, x => x.DeliveryStatus != "ok");
        }

        [Fact]
        public async Task ShouldSendNotificationToOneRecipient()
        {
            var result = await _mobilePushNotificationsService
                .SendNotification("ExponentPushToken[-Ok_6WApJgTWIZm2wZTdaN]", new PushTicketRequest()
                {
                    PushBadgeCount = 7,
                    PushBody = "Test Push - Msg",
                    PushSound = "default"
                });

            Assert.True(result.First().Value.DeliveryStatus == "ok");
        }

        public void Dispose()
        {
            _serviceProvider.Dispose();
        }
    }
}