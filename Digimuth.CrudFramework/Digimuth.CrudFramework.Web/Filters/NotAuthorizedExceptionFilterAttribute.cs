using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using ExceptionContext = Microsoft.AspNetCore.Mvc.Filters.ExceptionContext;

namespace Digimuth.CrudFramework.Web.Filters;

public class NotAuthorizedExceptionFilterAttribute : ExceptionFilterAttribute
{
    public override void OnException(ExceptionContext context)
    {
        var exception = context.Exception;

        if (exception is NotSupportedException)
        {
            context.Result = new ContentResult
            {
                Content = exception.Message,
                ContentType = "text/plain",
                StatusCode = 420
            };
        }
    }
}