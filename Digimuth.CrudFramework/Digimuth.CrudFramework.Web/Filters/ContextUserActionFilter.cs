using System.Threading.Tasks;
using Digimuth.CrudFramework.BL.Dtos.ApplicationUser;
using Digimuth.CrudFramework.BL.Services.Interfaces;
using Digimuth.CrudFramework.Web.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Digimuth.CrudFramework.Web.Filters;

public class ContextUserActionFilter : IAsyncResourceFilter
{
    private readonly IApplicationUserIdentityService _applicationUserIdentityService;

    public ContextUserActionFilter(IApplicationUserIdentityService applicationUserIdentityService = null)
    {
        _applicationUserIdentityService = applicationUserIdentityService;
    }

    public async Task OnResourceExecutionAsync(ResourceExecutingContext context, ResourceExecutionDelegate next)
    {
        if (_applicationUserIdentityService is null)
        {
            await next();
            return;
        }
        
        var httpContext = context.HttpContext;
        var user = httpContext.User;
        var userId = user.GetUserId();

        if (userId.HasValue)
        {
            var userFromDb = await _applicationUserIdentityService.GetApplicationUserIdentityWithCacheAsync(userId.Value);

            if (userFromDb is null)
            {
                context.Result = new UnauthorizedResult();
                return;
            }

            httpContext.Items[nameof(ApplicationUserIdentityBaseDto)]
                = userFromDb;
        }

        await next();
    }
}