using System.ComponentModel.DataAnnotations;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Digimuth.CrudFramework.Web.Filters;

public class ValidationExceptionFilterAttribute : ExceptionFilterAttribute
{
    public override void OnException(ExceptionContext context)
    {
        var exception = context.Exception;

        if (exception is ValidationException)
        {
            context.Result = new ContentResult
            {
                Content = exception.Message,
                ContentType = "text/plain",
                StatusCode = (int)HttpStatusCode.BadRequest
            };
        }
    }
}