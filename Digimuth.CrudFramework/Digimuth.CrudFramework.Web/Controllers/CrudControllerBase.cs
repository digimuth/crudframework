using System.Threading.Tasks;
using Digimuth.CrudFramework.BL.Dtos.ApplicationUser;
using Digimuth.CrudFramework.BL.Services.Interfaces;
using Digimuth.CrudFramework.Web.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using Morcatko.AspNetCore.JsonMergePatch;

namespace Digimuth.CrudFramework.Web.Controllers;

[Authorize]
[ServiceFilter(typeof(ContextUserActionFilter))]
[NotAuthorizedExceptionFilter]
[ValidationExceptionFilter]
public abstract class CrudControllerBase<TDest, TDestDetails, TInCreate, TInUpdate>
    : ReadonlyCrudControllerBase<TDest, TDestDetails, TInCreate, TInUpdate> where TInUpdate : class
{
    private readonly ICrudService<TDest, TDestDetails, TInCreate, TInUpdate> _crudService;
    private readonly ILogger<CrudControllerBase<TDest, TDestDetails, TInCreate, TInUpdate>> _logger;

    public CrudControllerBase(
        ICrudService<TDest, TDestDetails, TInCreate, TInUpdate> crudService,
        ILogger<CrudControllerBase<TDest, TDestDetails, TInCreate, TInUpdate>> logger) : base(crudService,logger)
    {
        _crudService = crudService;
        _logger = logger;
    }

    [HttpPost]
    [Consumes("application/json")]
    [Produces("application/json")]
    public virtual async Task<ActionResult<TDestDetails>> Post([FromBody] TInCreate value)
    {
        _logger.LogInformation("[POST] UserId ({ContextUserId})", ContextUser?.Id);

        if (!ModelState.IsValid)
        {
            _logger.LogInformation("User ({ContextUserId}) provided invalid model", ContextUser?.Id);

            return BadRequest(ModelState);
        }

        if (!await _crudService.IsAuthorizedForCreateAsync(value))
        {
            _logger.LogWarning("User ({ContextUserId}) was not authorised", ContextUser?.Id);

            return Forbid();
        }

        return await _crudService.CreateAsync(value);
    }

    [HttpPut("{id}")]
    [Consumes("application/json")]
    [Produces("application/json")]
    public virtual async Task<ActionResult<TDestDetails>> Put(long id, [FromBody] TInUpdate value)
    {
        _logger.LogInformation("[PUT] UserId ({ContextUserId}), Id: ({Id})", ContextUser?.Id, id);

        if (!ModelState.IsValid)
        {
            _logger.LogInformation("User ({ContextUserId}) provided invalid model", ContextUser?.Id);

            return BadRequest(ModelState);
        }

        // Note that this is not safe when write skew anomally happens.
        // EF Core will throw DbConcurrencyException if this entry is removed
        // after check and before update happens.
        if (!await _crudService.Exists(id))
        {
            _logger.LogInformation("UserId ({ContextUserId}) tried to modify an entity ({Id}) that doesn't exist", ContextUser?.Id, id);

            return BadRequest($"Entity with Id: {id} does not exist!");
        }

        if (!await _crudService.IsAuthorizedForUpdateAsync(id, value))
        {
            _logger.LogWarning(
                "User ({ContextUserId}) was not authorised for entity ({Id})", ContextUser?.Id, id);

            return Forbid();
        }

        return await _crudService.UpdateAsync(value, id);
    }

    [HttpPatch("{id}")]
    [Consumes(JsonMergePatchDocument.ContentType)]
    [Produces("application/json")]
    public virtual async Task<ActionResult<TDestDetails>> Patch(
        long id,
        [FromBody] JsonMergePatchDocument<TInUpdate> patch)
    {
        _logger.LogInformation("[PATCH] UserId ({ContextUserId}), Id: ({Id})", ContextUser?.Id, id);

        if (!ModelState.IsValid)
        {
            _logger.LogInformation("User ({ContextUserId}) provided invalid model", ContextUser?.Id);
            return BadRequest(ModelState);
        }

        var value = await _crudService.GetUpdateDtoAsync(id);

        // Note that this is not safe when write skew anomally happens.
        // EF Core will throw DbConcurrencyException if this entry is removed
        // after check and before update happens.
        if (value == default)
        {
            _logger.LogInformation("UserId ({ContextUserId}) tried to modify an entity ({Id}) that doesn't exist", ContextUser?.Id, id);

            return BadRequest($"Entity with Id: {id} does not exist!");
        }

        value = patch.ApplyTo(value);

        if (!await _crudService.IsAuthorizedForUpdateAsync(id, value))
        {
            _logger.LogWarning("User ({ContextUserId}) was not authorised for entity ({Id})", ContextUser?.Id, id);

            return Forbid();
        }

        return await _crudService.UpdateAsync(value, id);
    }

    [HttpDelete("{id}")]
    public virtual async Task<ActionResult> Delete(long id)
    {
        _logger.LogInformation("[DELETE] UserId ({ContextUserId}), Id: ({Id})", ContextUser?.Id, id);

        if (!await _crudService.Exists(id))
        {
            _logger.LogInformation("UserId ({ContextUserId}) tried to delete an entity ({Id}) that doesn't exist", ContextUser?.Id, id);

            return BadRequest($"Entity with Id: {id} does not exist!");
        }

        if (!await _crudService.IsAuthorizedForDeleteAsync(id))
        {
            _logger.LogWarning("User ({ContextUserId}) was not authorised for entity ({Id})", ContextUser?.Id, id);

            return Forbid();
        }

        await _crudService.DeleteAsync(id);

        return Ok();
    }

    public override void OnActionExecuting(ActionExecutingContext context)
    {
        ContextUser = context.HttpContext.Items[nameof(ApplicationUserIdentityBaseDto)] as ApplicationUserIdentityBaseDto;
        base.OnActionExecuting(context);
    }
}