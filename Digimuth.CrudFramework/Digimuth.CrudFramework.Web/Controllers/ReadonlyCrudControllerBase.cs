﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Digimuth.CrudFramework.BL.Dtos.ApplicationUser;
using Digimuth.CrudFramework.BL.Services.Interfaces;
using Digimuth.CrudFramework.Web.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Digimuth.CrudFramework.Web.Controllers;

[Authorize]
[ServiceFilter(typeof(ContextUserActionFilter))]
[NotAuthorizedExceptionFilter]
[ValidationExceptionFilter]
public abstract class ReadonlyCrudControllerBase<TDest, TDestDetails, TInCreate, TInUpdate> : Controller where TInUpdate : class
{
    private readonly ICrudService<TDest, TDestDetails, TInCreate, TInUpdate> _crudService;
    private readonly ILogger<ReadonlyCrudControllerBase<TDest, TDestDetails, TInCreate, TInUpdate>> _logger;

    protected ApplicationUserIdentityBaseDto ContextUser;

    public ReadonlyCrudControllerBase(
        ICrudService<TDest, TDestDetails, TInCreate, TInUpdate> crudService,
        ILogger<ReadonlyCrudControllerBase<TDest, TDestDetails, TInCreate, TInUpdate>> logger)
    {
        _crudService = crudService;
        _logger = logger;
    }

    [HttpGet]
    [Produces("application/json")]
    public virtual async Task<ActionResult<List<TDest>>> Get()
    {
        _logger.LogInformation("[GET] UserId ({ContextUserId})", ContextUser?.Id);

        if (!await _crudService.IsAuthorizedForReadAllAsync())
        {
            _logger.LogWarning("User ({ContextUserId}) was not authorised", ContextUser?.Id);

            return Forbid();
        }

        return await _crudService.GetAllAsync();
    }

    [HttpGet("{id}")]
    [Produces("application/json")]
    public virtual async Task<ActionResult<TDestDetails>> Get(long id)
    {
        _logger.LogInformation("[GET (id)] UserId ({ContextUserId}), Id: ({Id})", ContextUser?.Id, id);
        var result = await _crudService.GetDetailsOrDefaultAsync(id);

        if (result == null)
        {
            _logger.LogInformation("Entity ({Id}) was not found", id);
            return NotFound();
        }

        if (!await _crudService.IsAuthorizedForReadAsync(id))
        {
            _logger.LogWarning("User ({ContextUserId}) was not authorised for entity ({Id})", ContextUser?.Id, id);

            return Forbid();
        }

        return result;
    }
}