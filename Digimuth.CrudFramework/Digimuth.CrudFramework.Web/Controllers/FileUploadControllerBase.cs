﻿using Digimuth.CrudFramework.BL.Dtos;
using Digimuth.CrudFramework.BL.Dtos.FileUpload;
using Digimuth.CrudFramework.BL.Services.Interfaces;
using Digimuth.CrudFramework.Web.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Digimuth.CrudFramework.Web.Controllers;

[Authorize]
[ServiceFilter(typeof(ContextUserActionFilter))]
[NotAuthorizedExceptionFilter]
[ValidationExceptionFilter]
public abstract class FileUploadControllerBase<TDest, TDestDetails, TInCreate, TInUpdate>
    : CrudControllerBase<TDest, TDestDetails, TInCreate, TInUpdate>
    where TDest : FileDtoBase
    where TDestDetails : FileDetailsDtoBase
    where TInCreate : FileCreateDtoBase
    where TInUpdate : BaseDto
{
    private readonly IFileService<TDest, TDestDetails, TInCreate, TInUpdate> _fileService;
    private readonly ILogger<FileUploadControllerBase<TDest, TDestDetails, TInCreate, TInUpdate>> _logger;

    public FileUploadControllerBase(
        IFileService<TDest, TDestDetails, TInCreate, TInUpdate> fileService,
        ILogger<FileUploadControllerBase<TDest, TDestDetails, TInCreate, TInUpdate>> logger) : base(fileService, logger)
    {
        _fileService = fileService;
        _logger = logger;
    }

    [HttpGet("{id}/FileStream")]
    public virtual async Task<FileStreamResult> GetFileStream(long id)
    {
        _logger.LogInformation("[GET FILE] UserId ({ContextUserId}), Id: ({Id})", ContextUser?.Id, id);

        var file = await _fileService.GetDetailsOrDefaultAsync(id);

        if (file == null)
        {
            _logger.LogInformation("Entity ({Id}) was not found", id);

            return null;
        }

        if (!await _fileService.IsAuthorizedForReadAsync(id))
        {
            _logger.LogWarning("User ({ContextUserId}) was not authorised for entity ({Id})", ContextUser?.Id, id);

            return null;
        }

        var stream = await _fileService.GetFileStream(id);

        return File(stream, file.ContentType, file.FileName);
    }

    [Consumes("multipart/form-data")]
    public override Task<ActionResult<TDestDetails>> Post([FromForm] TInCreate value)
    {
        return base.Post(value);
    }
}