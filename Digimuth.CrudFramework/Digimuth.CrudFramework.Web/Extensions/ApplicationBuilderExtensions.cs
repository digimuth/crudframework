using Microsoft.AspNetCore.Builder;
using Swashbuckle.AspNetCore.SwaggerUI;

namespace Digimuth.CrudFramework.Web.Extensions;

public static class ApplicationBuilderExtensions
{
    public static IApplicationBuilder UseSwaggerGeneration(
        this IApplicationBuilder app,
        string clientId,
        string apiName,
        bool addUi = true)
    {
        app.UseSwagger();

        if (addUi)
        {
            app.UseSwaggerUI(c =>
            {
                c.DocExpansion(DocExpansion.None);
                c.SwaggerEndpoint("/swagger/v1/swagger.json", apiName);
                c.RoutePrefix = "swagger";
                c.OAuthClientId(clientId);
                c.OAuthUsePkce();
            });
        }

        return app;
    }
}