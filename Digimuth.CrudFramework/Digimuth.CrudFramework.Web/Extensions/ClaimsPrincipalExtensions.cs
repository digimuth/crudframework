using System.Security.Claims;

namespace Digimuth.CrudFramework.Web.Extensions;

public static class ClaimsPrincipalExtensions
{
    public static long? GetUserId(this ClaimsPrincipal user)
    {
        if (long.TryParse(user.FindFirstValue(ClaimTypes.NameIdentifier), out var userId))
        {
            return userId;
        }

        return null;
    }
}