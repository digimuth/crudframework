using System;
using System.Text.Json.Serialization;
using Digimuth.CrudFramework.Web.Filters;
using Digimuth.CrudFramework.Web.OperationFilters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Morcatko.AspNetCore.JsonMergePatch;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Digimuth.CrudFramework.Web.Extensions;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddUrlHelper(this IServiceCollection services)
    {
        return services
            .AddSingleton<IActionContextAccessor, ActionContextAccessor>()
            .AddScoped<IUrlHelper>(factory =>
            {
                var actionContext = factory
                    .GetService<IActionContextAccessor>()
                    .ActionContext;

                return new UrlHelper(actionContext);
            });
    }

    public static IServiceCollection AddSwaggerGeneration(
        this IServiceCollection services,
        OpenApiSecurityScheme securityScheme = null,
        Action<SwaggerGenOptions> optionsAction = null)
    {
        return services.AddSwaggerGen(options =>
        {
            if (securityScheme != null)
            {
                options.AddSecurityDefinition(securityScheme.Name, securityScheme);
            }

            optionsAction?.Invoke(options);

            options.OperationFilter<SecurityRequirementWithReflectedTypeOperationFilter>();
            options.OperationFilter<JsonMergePatchDocumentOperationFilter>();
        });
    }

    public static IServiceCollection AddFileLogging(
        this IServiceCollection services,
        IConfiguration configuration,
        string logFileLocation = null)
    {
        logFileLocation ??= configuration.GetValue<string>("Logging:LogFile");

        return services.AddLogging(log => log.AddFile(logFileLocation, isJson: true));
    }

    public static IMvcBuilder AddJsonMergePatch(this IMvcBuilder services)
    {
        return services
            .AddJsonOptions(options => options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter()))
            .AddSystemTextJsonMergePatch();
    }

    public static IServiceCollection AddSession(this IServiceCollection services, int idleHoursTimeout = 1)
    {
        return services.AddSession(options =>
        {
            options.IdleTimeout = TimeSpan.FromHours(idleHoursTimeout);
            options.Cookie.HttpOnly = true;
            options.Cookie.IsEssential = true;
            options.Cookie.Name = "Session";
            options.Cookie.SecurePolicy = CookieSecurePolicy.Always;
            options.Cookie.SameSite = SameSiteMode.None;
        });
    }

    public static IServiceCollection AddContextUserActionFilter(this IServiceCollection services)
    {
        return services.AddScoped<ContextUserActionFilter>();
    }
}