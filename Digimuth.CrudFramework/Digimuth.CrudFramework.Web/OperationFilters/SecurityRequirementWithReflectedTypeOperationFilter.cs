using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Authorization;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Digimuth.CrudFramework.Web.OperationFilters;

public class SecurityRequirementWithReflectedTypeOperationFilter : IOperationFilter
{
    public void Apply(OpenApiOperation operation, OperationFilterContext context)
    {
        Func<IEnumerable<AuthorizeAttribute>, IEnumerable<string>> policySelector = authAttributes =>
            authAttributes
                .Where(a => !string.IsNullOrEmpty(a.Policy))
                .Select(a => a.Policy);

        if (context.GetControllerAndActionAttributes<AllowAnonymousAttribute>().Any())
        {
            return;
        }

        var actionAttributes = context.GetControllerAndActionAttributes<AuthorizeAttribute>();

        if (!actionAttributes.Any())
        {
            return;
        }

        if (!operation.Responses.ContainsKey("401"))
        {
            operation.Responses.Add("401", new OpenApiResponse { Description = "Unauthorized" });
        }

        if (!operation.Responses.ContainsKey("403"))
        {
            operation.Responses.Add("403", new OpenApiResponse { Description = "Forbidden" });
        }

        var policies = policySelector(actionAttributes) ?? Enumerable.Empty<string>();

        operation.Security.Add(new OpenApiSecurityRequirement
        {
            { new OpenApiSecurityScheme { Reference = new OpenApiReference { Type = ReferenceType.SecurityScheme, Id = "oauth2" } }, policies.ToList() }
        });
    }
}

internal static class OperationFilterContextExtensions
{
    public static IEnumerable<T> GetControllerAndActionAttributes<T>(this OperationFilterContext context) where T : Attribute
    {
        var actionAttributes = context.MethodInfo.GetCustomAttributes<T>();

        var result = new List<T>(actionAttributes);

        var controllerType = context.MethodInfo.ReflectedType;

        do
        {
            result.AddRange(controllerType.GetTypeInfo().GetCustomAttributes<T>());
            controllerType = controllerType.BaseType;
        }
        while (controllerType != null);

        return result;
    }
}