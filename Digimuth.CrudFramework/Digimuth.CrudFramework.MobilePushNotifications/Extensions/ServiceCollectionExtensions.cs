﻿using System.Collections.Generic;
using Digimuth.CrudFramework.MobilePushNotifications.Services;
using Expo.Server.Models;
using Microsoft.Extensions.DependencyInjection;

namespace Digimuth.CrudFramework.MobilePushNotifications.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddMobilePushNotifications
            <TNotificationResult, TNotification, TNotificationRecipient>(this IServiceCollection services)
            where TNotificationResult : class
        {
            return services
                .AddSingleton<IMobilePushNotificationsService<TNotificationResult, TNotification,
                        TNotificationRecipient>,
                    MobilePushNotificationsService<TNotificationResult, TNotification, TNotificationRecipient>>();
        }

        public static IServiceCollection AddExpoMobilePushNotifications(this IServiceCollection services)
        {
            return services
                .AddSingleton<IMobilePushNotificationsService<Dictionary<string, PushTicketDeliveryStatus>,
                    PushTicketRequest, string>, MobilePushNotificationsService<
                    Dictionary<string, PushTicketDeliveryStatus>, PushTicketRequest, string>>();
        }
    }
}