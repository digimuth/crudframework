﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Digimuth.CrudFramework.MobilePushNotifications.Services
{
    public interface IMobilePushNotificationsService<TNotificationResult, in TNotification, TNotificationRecipient>
    {
        Task<TNotificationResult> SendNotification(List<TNotificationRecipient> recipients, TNotification notification);

        Task<TNotificationResult> SendNotification(TNotificationRecipient recipient, TNotification notification);
    }
}
