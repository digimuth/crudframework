﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Expo.Server.Client;
using Expo.Server.Models;
using Microsoft.Extensions.Logging;

namespace Digimuth.CrudFramework.MobilePushNotifications.Services
{
    public class MobilePushNotificationsService<TNotificationResult, TNotification, TNotificationRecipient> 
        : IMobilePushNotificationsService<TNotificationResult, TNotification, TNotificationRecipient> where TNotificationResult : class
    {
        private readonly PushApiClient _pushApiClient = new();
        private readonly ILogger<MobilePushNotificationsService<TNotificationResult, TNotification, TNotificationRecipient>> _logger;

        public MobilePushNotificationsService(
            ILogger<MobilePushNotificationsService<TNotificationResult, TNotification, TNotificationRecipient>> logger)
        {
            _logger = logger;
        }

        public virtual async Task<TNotificationResult> SendNotification(List<TNotificationRecipient> recipients, TNotification notification)
        {
            _logger.LogInformation($"Sending notifications to: {JsonSerializer.Serialize(recipients)}");

            (notification as PushTicketRequest)!.PushTo = recipients as List<string>;

            var result = await _pushApiClient.PushSendAsync(notification as PushTicketRequest);

            if (result.PushTicketErrors != null && result.PushTicketErrors.Any())
            {
                var errorMessage =
                    $@"Error while sending push notifications: {JsonSerializer.Serialize(result.PushTicketErrors)}";

                _logger.LogError(errorMessage);
                throw new Exception(errorMessage);
            }

            var pushResult = await _pushApiClient.PushGetReceiptsAsync(new PushReceiptRequest
            {
                PushTicketIds = result
                    .PushTicketStatuses
                    .Select(x => x.TicketId)
                    .ToList()
            });

            if (pushResult.ErrorInformations != null && pushResult.ErrorInformations.Any())
            {
                var errorMessage =
                    $@"Error while delivering notifications: {JsonSerializer.Serialize(pushResult.ErrorInformations)}";

                _logger.LogError(errorMessage);
                throw new Exception(errorMessage);
            }

            _logger.LogInformation(
                $"Successfully sent push notifications: {JsonSerializer.Serialize(pushResult.PushTicketReceipts)}");

            return pushResult.PushTicketReceipts as TNotificationResult;
        }

        public async Task<TNotificationResult> SendNotification(TNotificationRecipient recipient, TNotification notification)
        {
            return await SendNotification(new List<TNotificationRecipient> { recipient }, notification);
        }
    }
}