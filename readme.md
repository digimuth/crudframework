# Crud Framework

CrudFramework consists of multiple libraries:

### Core libraries
* `Digimuth.CrudFramework.Web` - contains components useful for WebApi projects, containing default controller action implementations, swagger generation, logging, authentication
* `Digimuth.CrudFramework.BL` - contains services helpful in CRUD scenarios - there is CrudService and Repository default implementations, also there are base dtos. It also has some helpful services for application user management
* `Digimuth.CrudFramework.Storage` - contains base db entities and DbContexts for both single-tenant and multi-tenant scenarios
* `Digimuth.CrudFramework.Common` - contains logic that can be useful from anywhere in the app, especially in multi-tenant scenarios (like `CurrentOrganisationService`, `ServiceProviderScopingService`)

### Additional functionality
* `Digimuth.CrudFramework.BlobStorage` - allows for persistent storage of any kind of files. Current implementations are Azure Blob Storage and local filesystem
* `Digimuth.CrudFramework.EmailEngine` - this is an email engine with token replacement (templates are done with Razor syntax), working with the configured SMTP server
* `Digimuth.CrudFramework.JobScheduler` - enables running periodical jobs in the background, configured via CRON expressions
* (TBD) `Digimuth.CrudFramework.Documents` - allows for document generation using token replacement, currently supporting docx and xlsx (TBD) formats

### Templates (part of Digimuth.CrudFramework.Templates package)
* `Digimuth.CrudFramework.Template` - basic project template with CrudFramework compatible structure and core libraries installed
* `Digimuth.CrudFramework.CrudEntityTemplate` - template which speeds up creation of CRUD entities by creating empty entity class, dto, service and controller with default implementations - **deprecated - use DigiTool instead**

All of these libraries are automatically pushed to nuget.digimuth.com on merge to master.
When updating libraries it might take a while for them to be available to update through command line. In order to force refresh nuget cache you can use `dotnet nuget locals http-cache --clear`

## Setting up basic CRUD project
* Add nuget.digimuth.com as nuget package source - you can use NuGet.config file (example in CrudFramework repo) or `dotnet nuget add source https://nuget.digimuth.com/v3/index.json`
* Install templates package `dotnet new --install Digimuth.CrudFramework.Templates`
* Navigate to the directory you want to have your project in and use command: `dotnet new digimuth-crud-framework-project -n [Default.Namespace.Name]`
* Use the nuget package manager to check to make sure that you are using the latest version of the CrudFramework in your generated solution, the template could be out of date. You will need to do this for each of the projects in the solution.
* Make sure to have .env file containing REACT_APP_URL variable (example REACT_APP_URL=https://localhost:5001)
* You should have single-tenant, basic project structure set up with Web, BL, Common and Storage projects

## DigiTool
* `DigiTool` is CLI which simplifies development on CrudFramework projects. Source code: https://bitbucket.org/digimuth/digitool/src/master/
* `DigiTool` basically replaces `Digimuth.CrudFramework.CrudEntityTemplate` and extends it with other functionalities\
* For more detailed information about available commands and options, please refer to the readme file in the source code.


### Features
* Allows for scaffolding CRUD dtos, services, controllers, frontend tables and forms from Entity class.
* Allows for scaffolding file entities when using 'FileBase' as entity base
* Allows generation on every model without corresponding files, with option to ignore some of them.
* Allows generation of empty entity file
* Supports pluralization and provides language options for English and Polish.
* Automatically includes all entity properties in the generated DTOs.
* Offers the option to add EF Core migrations after the scaffolding process is completed.
* Offers the option to generate frontend files as modals instead of separate pages
* Automatically adds configuration for handling files and migrations


### How to install and update
* Make sure you have `nuget.digimuth.com` added as NuGet source
* Run `dotnet tool install --global DigiTool`
* To update DigiTool to newest version use `dotnet tool update --global DigiTool`


### How to add simple new CRUD entity (with Address entity as example, using DigiTool)
* run `digi scaffold Address` and add all required properties into it. It will create entity file in Storage/Models. If creating a file entity, use `FileBase` instead of `EntityBase` as the base.
* (optional if using convention and target entity can be accessed through navigation properties of other entities) register entity in DbContext
* run `digi scaffold Address` somewhere inside solution
* review and add/remove/update all required properties in `AddressDto`, `AddressDetailsDto`, `AddressCreateDto`, `AddressUpdateDto` (used for GetAll, GetDetails, Create, Update actions) in BL/Dtos
* (can be done later) implement authorization rules (via IsAuthorizedFor...() methods overrides) on AddressService
* register `AddressService` in dependency injection - default project template registers it automatically through convention (refer to RegisterAllServices method in ServiceCollectionExtensions)

* make sure that you have dotnet ef global tool installed: `dotnet tool install --global dotnet-ef`

When running the project now you should be able to create, read, update and delete Address entities through REST API, test it using Swagger UI, and have basic table and forms components ready to be used on frontend.

## Multi-tenant architecture
![Database per tenant pattern](https://docs.microsoft.com/en-us/azure/azure-sql/database/media/saas-dbpertenant-wingtip-app-overview/app-architecture.png)

Database-per-Tenant pattern can be optionally used with CrudFramework. Information about organisations and users are stored in Catalog db.

### DbContext
Database access should be split into two DbContexts:

* OrganisationDbContext - contains all organisation-specific data (this connects to the Tenant db). Inherit from `OrganisationDbContextBase`
* CatalogDbContext - connects to the Catalog db. Inherit from `CatalogDbContextBase`

#### Configuration
```json
  "ConnectionStrings": {
    "CatalogDatabase": "Server=.;Database=Perficio_Catalog;User Id=sa;Password=Pa55w0rd;",
    "OrganisationDatabase": "Server=.;Database=Perficio_Organisation_{0};User Id=sa;Password=Pa55w0rd;",
    "ElasticPoolName": ""
  }
```
You have to supply connection strings for both Catalog and per-Organisation databases. Organisation database connection string is formatted with organisation id so leave `{0}` somewhere in the database name.
(TBD) ElasticPoolName is used to specify if these dbs should be a part of some Azure SQL Elastic Pool, leave empty if it's not the case.

#### Access DbContext
Use `AddDbContextFactory<TCatalogDbContext, TOrganisationDbContext>` and specify both dbcontexts for multi-tenant.
You shouldn't access neither of the DbContexts directly by asking for them in ServiceProvider. Use `IDbContextFactory` from `BL` project instead.

* `GetDbContextForEntity<TEntity>()` will get you proper DbContext for accessing the entity type you provided.
* `GetDbContext<T>()` will give you the instance of DbContext of type T (where T : DbContext)

If possible use `IRepository<TSource>` because it will automatically use proper DbContext.

#### Organisation context
Use `ICurrentOrganisationService` for retrieving and setting user's organisation id programmatically.
You can't use OrganisationDbContext (per-organisation db) without specifying the organisation id.

* If the operation is done in scope of `HttpContext` (WebApi request), organisation id is fetched from user's session. In order to set user's current organisation user `current` POST endpoint in OrganisationController (you'll have to implement it yourself).
* If the operation is done outside of web request scope or if you want to override the organisation id only for the specific scope, use `IServiceProviderScopingService`
```csharp
Console.WriteLine(_currentOrganisationService.GetCurrentOrganisationId()); // OUT: 2
var organisationId = 1;

// create service provider scope with organisation id passed as a parameter
using (var scope = _serviceProviderScopingService.GetServiceProviderScopeWithOrganisationId(organisationId))
{
    var scopedCurrentOrganisationService = scope.ServiceProvider.GetRequiredService<ICurrentOrganisationService>();
    Console.WriteLine(scopedCurrentOrganisationService.GetCurrentOrganisationId()); // OUT: 1

    var messageService = scope.ServiceProvider.GetRequiredService<IMessageService>();
    // gets all messages for organisation id = 1
    var messages = await messageService.GetAllAsync(); 
}
```

#### Migrations and seeding
Migrations for Catalog database are applied during application startup. Migrations for organisation dbs are applied the first time they are accessed during the runtime.

To add new migration use:
```
dotnet ef migrations add [migration-name] --project Default.Namespace.Name.Storage[.StorageProject] --startup-project Default.Namespace.Name.Web --context [OrganisationDbContext|CatalogDbContext]
```

Seeding mechanism is invoked after migrations. Seeding mechanism is slightly changed under the hood - it will apply the seeding for specific table only if this table exists in DbContext that is during seeding procedure.

```
 "SeedData": [
    {
      "Table": "OrganisationTypes", // this table is in Catalog db so it will be seeded through CatalogDbContext
      "Data": [
(...)
      ]
    },
    {
      "Table": "RaidCategories", // this table is in Organisation db so it will be seeded through OrganisationDbContext for every organisation db
      "Data": [
(...)
      ]
    },
```

## Using Job Scheduler
* Install `Digimuth.CrudFramework.JobScheduler` package
* Create job by implementing `Digimuth.CrudFramework.JobScheduler.IJob` interface. CronExpression is UTC time based CRON expression from `Cronos` library. ExecuteAsync is the job logic implementation that'll be triggered on time selected by CRON expression.
* Use `services.RegisterJob<TJob>()` on ServiceCollection to register your jobs
* Use `services.AddJobScheduler()` on ServiceCollection to register and run Job Scheduler on application startup

## Using Blob storage
* Install `Digimuth.CrudFramework.BlobStorage` package
* Register one of the implementations by using `AddAzureBlobStorage` or `AddFileSystemBlobStorage`
* From now on you can use `IBlobStorageService` for storing, retrieving and removing any kind of files from persistent storage.

Every file will have GUID generated at the beginning of the name to avoid collisions. 
Blob Storage package requires `ICurrentOrganisationService` to be implemented even if multi-tenant is not used (it can just return null for single tenant scenario).
Blob Storage will store files in separate catalogs for every organisation.

## Using Email Engine
* Install `Digimuth.CrudFramework.EmailEngine` package
* Create enum with email types you're going to use (for example ForgotPassword, Invitation etc.). You can have multiple different enums with separate email service and templates.
* Create email templates - add *.cshtml file (refer to MiniRazor documentation) as embedded resource and create model compatible with that cshtml.
* Use `services.RegisterEmailTemplate` to register email templates. Provide email type (enum value), template embedded resource location, email subject and assembly from which the embedded resource will be loaded.
* Use `services.AddEmailService`
* From now on you can use `IEmailService<TEmailType>` to get the email service for specified email type enum.